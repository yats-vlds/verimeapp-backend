<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('name');
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            // $table->string('password');
            $table->text('phone')->nullable();
            $table->integer('birth')->nullable();
            $table->string('address')->nullable();
            $table->text('firstname')->nullable();
            $table->text('sure_name')->nullable();
            $table->text('id_no')->nullable();
            $table->text('street_address')->nullable();
            $table->text('postal_address')->nullable();
            $table->text('mobile_no')->nullable();
            $table->text('tel_no')->nullable();
            $table->text('status')->nullable();
            $table->text('image')->nullable();
            $table->text('video')->nullable();
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            // $table->text('card_number')->nullable();
            $table->integer('code')->nullable();
            $table->text('documents')->nullable();
            $table->text('document_text')->nullable();
            $table->text('address_1')->nullable();
            $table->text('address_op')->nullable();
            $table->text('city')->nullable();
            $table->text('zip_code')->nullable();
            $table->text('card_number')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
