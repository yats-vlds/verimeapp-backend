-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2021 at 02:16 PM
-- Server version: 5.7.33
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `redboxte_agarwalbhavansweets`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qid` int(11) NOT NULL DEFAULT '0',
  `opt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `qid`, `opt`, `created_at`, `updated_at`) VALUES
(33, 21, 'Bangalore', NULL, NULL),
(26, 15, 'Male', NULL, NULL),
(27, 15, 'Female', NULL, NULL),
(32, 21, 'Bhopal', NULL, NULL),
(31, 21, 'Jaipur', NULL, NULL),
(34, 21, 'Channai', NULL, NULL),
(35, 21, 'Kanpur', NULL, NULL),
(36, 21, 'Delhi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `docs`
--

CREATE TABLE `docs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `passport` text COLLATE utf8mb4_unicode_ci,
  `driving` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `documents` text COLLATE utf8mb4_unicode_ci,
  `police_confirmation` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `documents_image` text COLLATE utf8mb4_unicode_ci,
  `police_confirmation_image` text COLLATE utf8mb4_unicode_ci,
  `type` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document_views`
--

CREATE TABLE `document_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` text COLLATE utf8mb4_unicode_ci,
  `accepter_id` text COLLATE utf8mb4_unicode_ci,
  `document_id` bigint(20) UNSIGNED NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `star` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `do_you_love` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `choose_two_option` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `social_Security_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_07_01_153030_create_docs_table', 1),
(9, '2020_07_08_095337_create_payment_verification_table', 1),
(10, '2020_07_14_072823_add_document_image_and_police_confirmation_image_to_docs', 1),
(11, '2020_07_18_082749_add_step_to_users', 1),
(12, '2020_07_18_101131_add_steps_to_users', 1),
(13, '2020_07_22_103416_add_type_and_code_to_docs', 1),
(14, '2020_07_28_144212_create_document_views_table', 1),
(15, '2020_09_02_104420_add_star_column_in_document_views_table', 1),
(16, '2020_09_08_155642_create_meetings_table', 1),
(17, '2020_09_09_124830_create_starts_table', 1),
(18, '2020_09_10_100737_add_registration_id_to_users_table', 1),
(19, '2020_09_10_123513_create_transections_table', 1),
(20, '2020_09_14_115435_add_business_column_in_users_table', 1),
(21, '2020_09_14_175057_add_token_column_in_users_table', 1),
(22, '2020_09_18_114755_create_police_verifications_table', 1),
(23, '2020_09_22_145505_add_pin_column_in_police_verification_table', 1),
(24, '2020_09_23_153045_create_signs_table', 1),
(25, '2020_09_24_104615_add_payment_verified_column_in_users_table', 1),
(26, '2020_11_03_140919_add_transcript_to_user_table', 1),
(27, '2021_03_06_144354_create_enquiries_table', 1),
(28, '2021_03_27_152538_create_questions_table', 2),
(29, '2021_03_27_152658_create_answers_table', 2),
(30, '2021_03_27_183756_create_surveys_table', 3),
(31, '2021_03_27_183811_create_survey_results_table', 3),
(32, '2021_03_27_185436_add_survey_id_to_survey_results_table', 4),
(33, '2021_03_27_190122_add_answer_to_survey_results_table', 5),
(34, '2021_03_27_190135_add_ans_type_to_survey_results_table', 5),
(35, '2021_03_27_190412_add_ans_type_to_survey_results_table', 6),
(36, '2021_03_28_063607_add_set_id_to_questions_table', 7),
(37, '2021_03_28_063801_create_questionsets_table', 7),
(38, '2021_03_28_064258_add_ans_text_to_survey_results_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('praveen.patidar10@gmail.com', '$2y$10$opzaPxPTMZyryMschcyI5e2UNDbXNuU2vPMnwGOLIKEGXEsp7LEdK', '2021-03-29 13:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `payment_verification`
--

CREATE TABLE `payment_verification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci,
  `last_name` text COLLATE utf8mb4_unicode_ci,
  `address_1` text COLLATE utf8mb4_unicode_ci,
  `address_op` text COLLATE utf8mb4_unicode_ci,
  `city` text COLLATE utf8mb4_unicode_ci,
  `zip_code` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `card_number` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `police_verifications`
--

CREATE TABLE `police_verifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  `document_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ansType` enum('text','textarea','checkbox','radio','date','file') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `setId` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `ansType`, `status`, `created_at`, `updated_at`, `setId`) VALUES
(28, 'Please, name yourself', 'text', 'ACTIVE', NULL, NULL, 7),
(11, 'Your Name', 'text', 'ACTIVE', NULL, NULL, 2),
(20, 'Your Profile Image', 'file', 'ACTIVE', NULL, NULL, 2),
(15, 'Your Gender ?', 'radio', 'ACTIVE', NULL, NULL, 2),
(19, 'Your  Email', 'text', 'ACTIVE', NULL, NULL, 2),
(18, 'Your  DOB', 'date', 'ACTIVE', NULL, NULL, 2),
(21, 'Your preferred Cites?', 'checkbox', 'ACTIVE', NULL, NULL, 2),
(22, 'About you ?', 'textarea', 'ACTIVE', NULL, NULL, 2),
(27, 'Please, name yourself', 'text', 'ACTIVE', NULL, NULL, 7),
(33, 'Social Security Number', 'text', 'ACTIVE', NULL, NULL, 9),
(32, 'Please, name yourself', 'text', 'ACTIVE', NULL, NULL, 9),
(34, 'Upoad your application letter', 'file', 'ACTIVE', NULL, NULL, 9);

-- --------------------------------------------------------

--
-- Table structure for table `questionsets`
--

CREATE TABLE `questionsets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `businessId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questionsets`
--

INSERT INTO `questionsets` (`id`, `businessId`, `title`, `status`, `created_at`, `updated_at`) VALUES
(11, 12, 'First set', 'INACTIVE', '2021-04-02 16:47:17', '2021-04-02 16:47:17'),
(2, 15, 'Sample Test To check resposes', 'ACTIVE', '2021-03-28 17:25:48', '2021-03-31 10:03:39'),
(4, 12, 'test', 'INACTIVE', '2021-03-31 11:32:26', '2021-03-31 10:03:39'),
(5, 12, 'Insurance Application', 'INACTIVE', '2021-03-31 13:15:44', '2021-03-31 10:03:39'),
(6, 14, 'Medical Test', 'INACTIVE', '2021-03-31 15:22:38', '2021-03-31 10:03:39'),
(7, 14, 'ASKA Insurance Aplication', 'ACTIVE', '2021-03-31 15:49:42', '2021-03-31 10:21:05'),
(9, 14, 'Driving Licence Application', 'INACTIVE', '2021-04-02 10:57:45', '2021-04-02 10:57:45');

-- --------------------------------------------------------

--
-- Table structure for table `signs`
--

CREATE TABLE `signs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documents` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `starts`
--

CREATE TABLE `starts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `room_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `starts`
--

INSERT INTO `starts` (`id`, `participant_id`, `room_no`, `created_at`, `updated_at`) VALUES
(1, 1, '4512894956577f5b528c8e3543c9dee5', '2021-03-28 02:20:06', '2021-03-28 02:20:06');

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(11) NOT NULL,
  `formId` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `userId`, `formId`, `status`, `created_at`, `updated_at`) VALUES
(6, 1, 1, NULL, '2021-03-28 07:28:21', '2021-03-28 07:28:21'),
(5, 1, 1, NULL, '2021-03-28 07:27:25', '2021-03-28 07:27:25'),
(3, 1, 1, NULL, '2021-03-27 19:53:48', '2021-03-27 19:53:48'),
(4, 1, 1, NULL, '2021-03-27 19:54:04', '2021-03-27 19:54:04'),
(7, 1, 1, NULL, '2021-03-28 07:31:27', '2021-03-28 07:31:27'),
(8, 1, 1, NULL, '2021-03-28 07:31:50', '2021-03-28 07:31:50'),
(9, 1, 1, NULL, '2021-03-28 07:32:22', '2021-03-28 07:32:22'),
(10, 1, 1, NULL, '2021-03-28 07:33:32', '2021-03-28 07:33:32'),
(11, 1, 1, NULL, '2021-03-28 07:35:12', '2021-03-28 07:35:12'),
(12, 1, 1, NULL, '2021-03-28 07:36:03', '2021-03-28 07:36:03'),
(79, 1, 14, NULL, '2021-04-06 12:48:29', '2021-04-06 12:48:29'),
(78, 1, 2, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(77, 1, 14, NULL, '2021-04-06 11:20:21', '2021-04-06 11:20:21'),
(76, 1, 14, NULL, '2021-04-06 11:14:20', '2021-04-06 11:14:20'),
(75, 1, 2, NULL, '2021-04-06 11:13:37', '2021-04-06 11:13:37'),
(51, 1, 1, NULL, '2021-04-01 06:46:57', '2021-04-01 06:46:57'),
(52, 1, 1, NULL, '2021-04-01 06:47:15', '2021-04-01 06:47:15'),
(53, 1, 1, NULL, '2021-04-01 06:47:39', '2021-04-01 06:47:39'),
(54, 1, 1, NULL, '2021-04-01 06:50:20', '2021-04-01 06:50:20'),
(55, 1, 1, NULL, '2021-04-01 06:50:59', '2021-04-01 06:50:59'),
(56, 1, 1, NULL, '2021-04-01 06:55:22', '2021-04-01 06:55:22'),
(57, 1, 1, NULL, '2021-04-01 06:58:54', '2021-04-01 06:58:54'),
(58, 1, 1, NULL, '2021-04-01 07:04:17', '2021-04-01 07:04:17'),
(59, 1, 1, NULL, '2021-04-01 07:04:39', '2021-04-01 07:04:39'),
(60, 1, 1, NULL, '2021-04-01 07:05:00', '2021-04-01 07:05:00'),
(61, 1, 1, NULL, '2021-04-01 07:06:29', '2021-04-01 07:06:29'),
(62, 1, 1, NULL, '2021-04-01 07:07:08', '2021-04-01 07:07:08'),
(63, 1, 1, NULL, '2021-04-01 07:14:08', '2021-04-01 07:14:08'),
(64, 1, 1, NULL, '2021-04-01 07:14:28', '2021-04-01 07:14:28'),
(65, 1, 1, NULL, '2021-04-01 07:14:59', '2021-04-01 07:14:59'),
(66, 1, 1, NULL, '2021-04-01 07:15:56', '2021-04-01 07:15:56'),
(69, 1, 14, NULL, '2021-04-03 15:08:12', '2021-04-03 15:08:12'),
(68, 1, 8, NULL, '2021-04-01 07:22:24', '2021-04-01 07:22:24'),
(70, 1, 14, NULL, '2021-04-03 15:10:44', '2021-04-03 15:10:44'),
(74, 1, 2, NULL, '2021-04-06 11:07:40', '2021-04-06 11:07:40'),
(72, 1, 14, NULL, '2021-04-05 08:04:43', '2021-04-05 08:04:43'),
(73, 1, 14, NULL, '2021-04-06 06:19:58', '2021-04-06 06:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `survey_results`
--

CREATE TABLE `survey_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `surveyId` int(11) NOT NULL DEFAULT '0',
  `qId` int(11) NOT NULL,
  `ansType` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answerId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ansText` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_results`
--

INSERT INTO `survey_results` (`id`, `surveyId`, `qId`, `ansType`, `answerId`, `answer`, `ansText`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'radio', '1', NULL, '', NULL, '2021-03-27 19:53:48', '2021-03-27 19:53:48'),
(2, 3, 9, 'checkbox', '20,21', NULL, '', NULL, '2021-03-27 19:53:48', '2021-03-27 19:53:48'),
(3, 3, 5, 'text', NULL, 'Praveen Patidar', '', NULL, '2021-03-27 19:53:48', '2021-03-27 19:53:48'),
(4, 3, 8, 'date', NULL, '08-08-1998', '', NULL, '2021-03-27 19:53:48', '2021-03-27 19:53:48'),
(5, 4, 1, 'radio', '1', NULL, '', NULL, '2021-03-27 19:54:04', '2021-03-27 19:54:04'),
(6, 4, 9, 'checkbox', '20,21', NULL, '', NULL, '2021-03-27 19:54:04', '2021-03-27 19:54:04'),
(7, 4, 5, 'text', NULL, 'Praveen Patidar', '', NULL, '2021-03-27 19:54:04', '2021-03-27 19:54:04'),
(8, 4, 8, 'date', NULL, '08-08-1998', '', NULL, '2021-03-27 19:54:04', '2021-03-27 19:54:04'),
(9, 6, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:28:21', '2021-03-28 07:28:21'),
(10, 6, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:28:21', '2021-03-28 07:28:21'),
(11, 7, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:31:27', '2021-03-28 07:31:27'),
(12, 7, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:31:27', '2021-03-28 07:31:27'),
(13, 8, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:31:50', '2021-03-28 07:31:50'),
(14, 8, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:31:50', '2021-03-28 07:31:50'),
(15, 9, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:32:22', '2021-03-28 07:32:22'),
(16, 9, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:32:22', '2021-03-28 07:32:22'),
(17, 10, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:33:32', '2021-03-28 07:33:32'),
(18, 10, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:33:32', '2021-03-28 07:33:32'),
(19, 10, 5, 'file', NULL, NULL, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAH6pJREFUeJztnXtgVNW59p93zyTkNoOigDdKVFoVb4FkwiVoj9VaWz32U1FQ256jrbW1PUetLcwEtWnVJFDx0p62VltvtQooVM+xar1Xi0gmAa9YK5pwEUhALDMJScjMfr4/MK1iwqyZ2WvvmWT9/iRrv++TMM+svdd+17sAg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYBka8FjDUCVRetX/SV1Ru2Xa5CMpFOJ6UcoDjRVAOSOBTF5FdBLoh6AHQDUi3ED0AYwTaIPIObbxHkbVdvbvewWs3drn/mw0PjEEcpCw092gLUkFIhYAVEFQCsq/uvAQ7APwdwAqx5fnYzhEvYE1dp+68wwFjkAwJVEf2s22e6rMwnZRKAY6HoMRrXR+jmcCzAF6Md4543hgmM4xB0iBQHTkB5FcBnCoix3qtJy3Ip23IMsvfszi24ubtXsvJF4xBUhCsmlNti3WBiMwU4GCv9TgC8QyBxbB3/THesnCb13JyGWOQgZh2ZXEgUfQ1AS6DoMJrOVohn7Zh3dEZrV/itZRcxBjkYwSnXTmKiaKICC8dcHVpCENyEyG/QW/fbZ2v3djhtZ5cwRgEQNmkyGjxISyC7+TYg7brkNglgocI/E+8qWGF13q8ZlgbJFB51f6QwggsflcgxV7ryTVILCfsSzuj89/0WotXDEuDBKoj+4GMQHCZMUZqCN6b7LMjO1cv2OS1FrcZXgaZWFcWLO2dC/BKiJS6mLmV4GtCeZWC1bCxNUnptpjoTthWt68g2dNlW91omb8DAFA5d2QJfCVJWCV+9JX4YJUkwZE+S44EeARFjgJ5lIgc4NYvQKBHwIWxnkTDcHpzP2wMEgzNvRxiXQtglM48BN4H8ATBZrHl9ThHrEZL3U4tySrnjgxYOFpgVVFYCSAkkKO05PoIkltEODfWNP9enXlyhSFvkKJJ88YX+O17RPB5HfFJfCDCZ21bnrF8iWdjK3/2jo48ylTWlZRZvSEBQhCcKcAJOtKQeNS25FtdK+vbdcTPFYa0QQLVkf8Q4OcAgo4GJt4FeH8S1rKuaP0rjsZ2mJJJcw7yF1izSDlXBNOcjc4Pkza+19Xc+EAmV5cd98Mx1oiC2RCOsomkgM/Eo/NfclZjdgxNg0ypCwbZ81tAznUqJIF2gPdLUhbHWhpWOhXXTYpD88b5JXmBEJdB5DNOxSWxjAl8p3N1w1bVawLVkTOEWLznsjqB38WbGr7llLZsGXIGCVTOnQFLFovIQU7EI3mfDevurmj9M07EyxXKQrXnCfjfIqhxJiI/hM0LY83zH081MlA196tiWQ8PGgl4ON7UcJYzurJjSBmkLBS+xhL5qROxSN4tSdwQW9W41ol4uUpJVXiST/ADEfmaMxH5g1hT482D/TRQHT5LIMtSRgFOjDc1vOiMpswZEgYpnVI71iIfEOCkbGOR/G2fzet6Wuavd0JbvlBUOfczBZbME5FvZxuLxF3xaMPFe/57WXX4XAuiVPNFGzfFmxuuylZLtuS9QYJT5kyl7ftfEYzOJs5wNcaeFFWEywsKcY1APvUBTweSz8d7cDZeb/wQAMqqamdbFtUf5sknYtHGL2ejwQny2iDpfCMNBsFXRezvxFYueNkpXUOBolD4sALBjwXyjYyDEO+SydNty1/pA/+Q5rXPxKINp2Sc2yHy1iCBUCQsgoaMA5A7bOLqzubG/3FQ1pCjrLL2JMviPRCMyzBEDBkssxN8Nt7UeHKGOR3D8lpAJgRCkZ9lYw6S99kJ+awxR2o6W+qfi8WSR5O4K8MQGb6Dkpz48vZ7LSBdPjLHDzO7mh8mbWtWV3PDU86qGuK8vSAeBy4uDYWXWCJ3CzDWa0lukVczSDbmIPlSX6/v2K7memOODOmKNj4hvp6JIFz4G5L6c6QmbwwSrA7XZWIOkiTRGI++e2L3qze8r0PbcCK24ubtseiI00jcqDeTucVSJhAKXwXIj9O9juQ2Eud2Njc8r0PX8KXOjkfxo9Lq2tUW+DsBirxWpIucn0ECocglIpL2txWJvyUse1Jnc6Mxhya6murvTyY4g8Rm56ObW6yUlIZqTxbB7eleR2BlvIfTu1cu2KhDl+Ff7FzV2ELgNq916CJnb7GCk8MTIExZs/MpyKfjH3adgbW/6NUgy7AHZVW137OEP3E+snkGGZzKuSPhw+NIcw19dw1QY1YlEgZ1gtXhKwHepCe6ucUalKDPWgzIhHSuIXjzQAVyBj3sNodoMgdgZpBBCIYi1wH4UjrXkLw7Hm38gSZJhj0oC0W+C0CjOXKHnDJIoDpyBoCr07mG5P/Fo40XaZJk2IOSUDhkCX6lP5O5xfoEJZPmHCTE79O6iGiKf9jl2LZaQ2p8gu+7k8ncYn0CX4G1FMA+quMJrIn38DSzWuUuAkx3J5OZQf5JsDpcJ5CpquNJbkn0Wqf2b8YxuIo7X6rEDlfypMBzg5RNnnMUIfNUx39UW3W+qavyBhKrXckDWeVGnlR4bhDL57tb0vlWEmkw5SPeYYv1GxfSbBd7V068nfd5mTxYPfe/IKLcA4nkS/Fo44U6NRn2Tt/7L75bePAJBwpQpSuHTXy5s3nBW7rip4N3Bqmo22eEz/4jALXu6sQ/+myekti8PCfuTYczu97/66MjDq5pBRGgoEzgUCNw4tfJpHyrq6WhyZF4DuDZUlogFL5RRJTbuiQhp3U11f9ZpyYveOmQ4Cj6i79GkXMFmAEAIFaTfMif6Lt36vvbTcGlh3hikI96MK0VkQKlC8g/xKKNDjU2857mw/Yd2ZMsnAkLs0GcJCKDzuQkVwJYVAh7UXXb1i0uyjTAI4MEQ5EHIJitOLwTvp7x+X508atjx5Z2juBZ8MlsAKcCil8O/ZA2RP4K2osk0bN4+sZYXv898gXXDVIaqq3wCZWXCm1bvt/ZXP9LnZp08c4EjOjYNfrf4fPNJnG6iDM770gmBXhGiMVWvO+hqdu3x5yIa/g0rhskUB1eofpSkMQb8WjDsbo1OUkzUNB36NjTksTs3edzSJnmlLsI/tmysai0B48c394+bE5/cgNXDVJWHf6CBVHukp5IsGrnqsYWnZqcgIDvpUPHnAzKbAJniYhyyYzDQroh+BNse9GYgo5HP7sWpgwnS1w1SCAUeUQEZyoNJn4dizZcpllSxhCQFeUHnEhgFgUzBdn1BnYagnEBHpEkFveub3/iJCDhtaZ8xDWDFB8/72B/YXKDSOoqTRK7IDgo3tTwgRva0mFF+dgpSWA2gPOcOoNENyQ+hHCZL4lFU9e3PydA0mtN+YJrBglUh28SyJWKw38Ra2r4b62C0mB5+egKwJoNyCwIyr3Wkx3sAOQhJpKLajZs/asAOVE1m6u4Y5CJdYXBst6tUNhjTmKXjcT4rujPPF3zXz5uzOHwWV8nMFsER3ipRSMbASwRYPH01i058/Y6l3DFIIHqyDcF+K3SYA+fPV48aN/PSGHhhRDMFshxXmjwCpLvAVjsFy6a2trxmtd6cgVXDBIMhVdBZFKqcQQSNhPj3Jw9mspHH9BHuQCWzAKk2q28OQ3xFoDFSNr31WzoeNdrOV6i3SAllbWT/T4qLdWSvD0ebbxUtybgo4dtwXUC+aIb+fIVAs2WbddPX9fxR6+1eIH23WE+i+erjk2q3oZlyfLxY66yRW7MiU3POY4AVbSsZcvLD/h1TdsWT259A5VX7Q9f4cUClti2bAZkeWdz/Rtu5Nb+GQmGIusVTydqjTU1HKZbz4ufGT3D8vk8Pz01LyHPr2lrX+RmytLq2gt8tG+H7FlSzwdiO+xL8faCuM78WncUBqfMmap6dJcN/k6nln4sny9nlo/zDbrW0WQ3paHwKT7wD582BwDI+cGR1nO6NWg1CGmdrTo2KfY9OrX8E3KyK3mGIAIJ0cV3ZxZwzd5HSGWgOqy8IzVDDVo5Q2UQyRfc68Qu+7qTZ0hS2HRwYJQ7qc71QVCTapQQWZ/rvje0GaSksvZAgRylNFjSbBhnGPIUTzn0QMHgG8n+iUgIFXXaikO1GcQSKN9exZN8UJcOQ37SvXLBRgI9KmPL/D3ajovWZhARW1E0W9Ay3zRiMHwKAV9QGic4VZcGjc8gcqLKKALP6tNgyGdo42mVcQKcokuDFoMUhcKHiWA/lbFiU/tSnSE/SVo+tc+GyGG6nkO0GKQQVGpwTCARY/FfdGgw5D87mwpWAehUGVta0KPc2zkdtBjEhhVSGkg2oaVupw4NhqFAnU2o3WZZQMpi2EzQYhARNbECMc8fhr1CsFlxaKWO/JqKFVml8sKVRFRP/uHJiEPGoejgQwAAO1au8FiNM1jgKyqfJRHRUiHhuEFGVF59uCCp1G9XbK5xOv9wZMw55+HAi76N0olHf+LfOx5ajA0/X4jejRs8UpY9ffS/ViB2ynEky/FvdX48X+docwrHb7FGSFJpeyoJO7aqcVhvxnGCCQtuwYSf3fopcwDAmJmzcPyjT6N04jEeKHOG7ugNGwCk7CIpIhKI96Z1MrIKjhvEhhyqOHQNTMOArJiw4BaMmTlrr2P8wSCOvn8pRhyiVFSdkxB4VWWciK362VPGcYOoihRhTpz/kK8cdPG3U5qjH38wiEOv+almRTphq8ooG3K405mdX8USGa80jmIMkiEjp05H+dU/SeuaUV88LW9nEQHWq41TvntRRscy71iVQUnybxpyD3n8wZE44ra7Mrq2KE8NQojSVggRHux0bg0G4f5KwwTmhNoMOPr+pfAHU7YXG5B8fVi3FQ1CkTFO59Yxg6jVYFGtlNnwLyYsuGXA1SpVuta40ufAcUSotk5N5/sjO/+QDjUXW5Rup3MPZcbMnKX8UD4YXWvedEiNuyR7LNXtEEq39+ng2THQSZ9tZhBFSiceg/Krs1uF6li6BIlYfm676baTSvV6InkwgxBU6hxu2TQGUcAfHIkjb7sz4+cOAEjGY2i77loHVblMd5dnBa06ZpBdKoP6kgXGIAoccdudWS/PvnH+OXk7ewAA1v7Cs4OAnH8GoZpBfIVmBknFuMt/iJFTlbbWDErb9T/O24fzT0B6crSc48WKFOkRYKTTcfMBf3AkxpxzHvb94mkonXgMejduQNeaN7B12RLsePmltGKNOvXLGHe58jHyA9KxdAk23Xl7VjFyBhFPypIcN4iA3SrlyUlaJU7n9pIxM2eh/OqffuJZwT/xaJROPBpjZs7CjpdfwtvfuVjpVqd04jGYsOCWrPR0vfVmfj93fBrdh6EOiIaHdCgt3/pFrSQ+HxgzcxYmLLhlrw/SI6dOx/GPPpXyZZ0/ODJlrFQk4zH87dKL8vu54+NMqcv8j5ElGp5B1N5v2B7NIBSk3lyQBiMOGae8BDvikHE4+v6le32uKL/6J1m9DASAd350hbY9ILt8lqN/PxWKe/oCbufsx7MZxGLCE4MI4OjD3phzZqX1bd9ffj7QS790KnQHY8OtC7H9ycezirE3Tli/w/USIb/fVjIIgXanc2sod1dbbRDCmx65TL35Jh2CGa4yTVhwyydmnkwqdPdk+1NPYMOtN2YVY28QVOow4jRJi6oziOMGcX5POrFVpf83IR6VlvJDJxuUj5wyLeNrD7r4EviDQWz4+cKMK3T76X1/I9b+6IqsYqRGHP1yUc4KK6iyt06YBzMIRd5XGScijpcmKyHi6C1C7/vZNaUfM3MWKl9oyouHciE9MohaASyFm53OraFY0Vb7xAi8MYjt7H9yLryEa73uWrd0eGIQS+yJSgMpHY7ndjqgTUtpBgFwiNO5VaBgm5PxOpYucTJc2my66w50PLTYlVyEs3+7NBIfqTgy92+xLCbVdn8B5U7nVsrrcKuh7U8+7lkPKvdfBtKTenmK2jkzefEMkqBfdQH+UBwxx/X1bb/fbnI65tuXXoSut9z97CTjMbx5/jmu5rSAla4m/BefUxlEC21OJ3bcIDtb6jerFpYFRvoqnM6fiinvbfs7QEefZhOxHXjz/HNcnUm8qNC1O+30CsocYHcjQhSpjI37elTblCqjZcMURVYpjbN5vI78KfPC+Zanu01ytivPJN5U6PKdGdu2aT1yeSAKfUm1Y/yId7HiZsd3qeppXg28ojRO4IlBBHD8NquftT+6HG3X/1hXeC8rdD25vRJC7aQAodKXcrromUFsqhkEUqUjfyqsJLQe2rPpztuxds4VSMZjjsb1skJXbL1/s8Gg8AtK46h215IuWgyStHyvKQ0UVODYsOslJ1PXtz/rdMnJnnQ8tBhvnH+OYybxskKXZLJYepe5nnjCf40AROlgHFtktQ4JWgyys6NA+Qa5rFjf+XKDIYBNQPvDQteaN/DK6ac4ssKls0I3FSJ4YlLbP/7hdt7SUYF/E8VyqK5k8mUdGvR0NWmr6yEVTyil+wYBAB/gytu13o0bsl7h0l2hmwrafMCLvBbtkxSHrtN1UrK2tj+EPKMyTiDazrjeG9PatjxPcosbubJZ4dJdoZsKEj1jCzoe8iK3AIrPH3hSlwZtBrGspJpoweEllbUH6tKx99xyn5vp0l3hcqdCNxVc9tm1cL+ryJS6IEQUV7DwmC4Z2gwSW7ngZSieUOrz0fGDT1QoZHIhFNsUOYXqCldObJsl6WPiei9SB5I9M1XGEUzGkyPybwYBABLPq4yzk7brD4AAUN22dQth3+F23v4VrsGeSzqWLkHLCdXeVwqLLJu27gNvjqkQfF1x4As6T0rWdIhnP1wKyBl7HQFu7GqZ/7peHYNTSF7fJ7gEQKGbebvWvIE3zz9798Gbh4xD6cRj0LXmDXSteTM3mi2QtJi4xovUJZPmHATg80qDbWpdvdA6g8SjjXeD3OvhJ2LLpTo1pKK6besWEnd6lb934wbsePklbLrzdux4+aXcMAcAAg97NXv4/NZ/iIjStk8KtT1/AC40r+6Dbwb56TJpgt0kLo41N2j9BVWwEn3X06POfTmLJCPe5Va8vSLXd0bnay2j1m6Q7ugNG+LRxmMIXkLwXpLP2+StyaR1eDzakN1GbIeYvvGD9y0wuzaGQwpeM6N129teZC6ZHK4UqO3/AHCPVjFwsnvBEGD5oQf8BcCJXuvwEpKv1rS1Vwqg1KXfaQLVkdsEULrt7pPkuO6VC7JrCpACz84HyU3kG6BaX6+hCMGEhcRsr8xRUll7IIiLVMYS/JNucwDGIJ+gpnXzOgrmeK3DM8jrprd94Nnhqn6L80QUVxPJ32iWA8DcYg3I8kMPeBCA0ouqoQLBp2e0tn/Rq/wllbUH+iy2qRiE5JZ4tOhgoE57G1QzgwyA9G35Bohhc447gfX+HX3ubnDfA8uy56rOHiK4zQ1zAMYgAzJ9I7qRtP8dhGdHf7kFgV5J8PSp27c7u7srDcqO++EYEfX3YYk+96ofjEEGoWZDx7sC+0KvdehGYF9Ss6Hd05oWa0TBtaqNGUjevnP1gk26NfVjDLIXprd1PAwb3/Nahy4I+9qa1o7fe6mhrKr2GIKXqYwlkOizeYNuTR/HGCQFNeu2/ArAQq91OA3B381o7bjOYxliWfbdqmUlIO/raZm/19IlpzEGUaCmdcsPATzotQ6nIPFETWv7JV7rKAtFvgNIpcpYErYk4ersARiDKFPTuuU8kI94rSNbCD7Vhy1nicp5AhoJVEf2s4B69St4f2xV41p9igbGGCQNpre1n4O8nkn4aF9r+1dOakMuHMF9MwT7qAz8aPbI7nShDDEGSQMBkh/NJJ6Vx2cKiXumt7afeRKQ8FpLoCpypkB1QxQA8E4vZg8gx96kB6rmHAGxLoHI91SX/VJB4g2b8oOu5vqnnIjXz1/Lx9aLiHcl4WnB+prW9nleqwCA4ilzDimwrTcgMlJlPInNcds+SlfXklTkzAxSUlk7WcRqEZGrnDIHAIjgGJ/FJ4Oh8C+digkAM9raa2HjjFzeR0KyywLPzBVzAIDf9i1SNQcAQHCZV+YAcsUgE+vK/BYfhkipthwil5VW117gZMiadVv+VGAlJ5PwZO/E3iDZaiXsimmt7f/ntZZ+gqHIdSKoUR1P4KF4U8PDOjWlIicMUlbScwEE2g/1tKj2Qiodpry37e/oTIRAunPMkwIkl6Azefz0jVs9uW8fiLLK2pMguDqNS2K2yPe1CVIkJwxiWTjWjTzpfHulw4xt2+I1be2zafNbnu4nIbqF9qUz2tpneXFUwWCUTJpzkFj8QzrXkLy8a2W94ydGpUtOGISQAteSldc59nyzJzPWtf9O0DeZhKPHvKnB1wV9k6e3dXhyNsKgVNTt4/NbT4pAuTkgwT/Fo41365SlSk4YxEWa0Van9R3A9LYP/lbUtqVCyIgrswmxE0R4V2v7ZC83Ow3ItCuLAwW9j4nI0aqXkNwS705nCVgvw8ogNulKk9sqoG96W3ujbfMYkNq6/pF4HCITa9q2zM+F9xt7EkgULRLBtLQuEjkPrzc6epZ9NmhuHJc7kPxtZ7TR1QfpE9a3vwfgS8vHjzmHlnWzwKGFCKIN4BUz2tpztvQlEIrcKYIz07nGJn7cGW14UZemTBgmMwgfiEcbPSvOq1nXsbSo1zqSZGO2sWjjul3YclRNLpujKrJQRK35Qj8kX+iMNvxUl6ZMGfIzCIHH4k3ven5PW7Vp004AkRc/M/YOy8IdEFFq7f8v+ChgfX/Gus3rtAh0hDorGOq5F4K0NpqR+MC2rPN0qcqGoW0Q8ol4tPF0r2V8nI9uu05efuiYU0Gpg8he79FJPgegdkZbu5YTlByjvK4oMLr3QcjeezEPBIFzcmFJdyCGrEEIvhzfWnSW1zoGo6a140kAT/51/AFfgYWfCPCJA01JvuyDRKa1tSt1yPeUyrkjg77ep7HH75AKgklA/l9ntOEvmpRlzZA1CChlxaP7Rne3wZuD/RSZsW7LYwAeayoffUCC1hcgLEOCz9bk0FvwvVE8Zc4hflueBnBEOteRpE18vau54VFN0hwhJ6p502k3mQ4kPgDsM+PR+S85Hduwu3xELHuJiOyf7rU2+N3OpsbbdOhykiG9iiWC/QDruWAoPOS7k7jLub5AdeQGsfh0JuYAWJcP5gCGuEEAQASFELmvLBS52WstQ4GSytoDA9WHPy9ArUj6nx+buCXW1OjJ7sBMGPIG6ccSXBGsjkRLK+e6Uhg5FCmtDJ/q99lvCmRGJtfb5LWd0YYrndalk6H7kD4wVZbPWhUIhRfG/b0/wYqbh20n93QomxQZLQVYmN422X9BkoBc2hltdP08yGwZNjNIPwL4RWRuMFn0Vmll+FSv9eQ2dVZZVfj7lp/vZGwOIEFaF8SjDXlnDiCPDULgMZv250luyzDEeJ9P/hwMhRcVVYTLHRU3BAhWRqYEQ70tliW/SGuL7B6Ija92NtcvclKbm+SEQQRMq0fT7vKRhtM7o/NfSNrWcQCaM08uswoLpTVQHb4Jx4b3zTjOEKE4NG9cIBS+Az68DEFFpnFIbCVwYi6cQZkNOWEQAMplBgQfiTc1/LN8ZGdL/eZYU0OI4L3ZCBDIlcFivBsMzb08mzj5SlFFuDwQitxZIPZ6EflWVsGIZ2xLjo035VZlbibkxIvCstDcEy2xUpYbkPxjPNp49mA/D1aHrwTkJgckrQPshbHO4ruwpq7TgXg5S3ByeAL9uEYg33AiHsFwvKlxvhOxcoGcMAgABEPhJoiEBvs5yaXxaGPKU5/KQpHPW4J7AIzPXhXjtOWOPtq3ut00WTdl1eEvWJBvAnCm0wu5HracF2tpWOlIvBwhZwxSVBEuLyiQx0Vw5AA/vj/W1KD+NvyIOYFA0LpRRL7tnEI+aFN+mcuFdakIVkam0MdZIGaJyEHOReaDsSQv8bJ/lS5yxiD9BKsiV0DwFQKFEKxPJuWWnS31qzKJVVoZPtWycJeTHwaCG0FZJDYeyodvy9JQbYUP9tkQnA/IBCdjE2gncVlntGGZk3FziZwziONUzh0ZsOQWEflP54OzjZAlAB6ONzWscD5++gSqI5+jjZMswRcgOAXAKB15CN4b31V0OV6p+4eO+LnC0DfIR5SGak+2wF+J4HPakhCvAFhBYHWSbN7Z3LhaWy4AqKwrCVjdFRQ5TiBVAE4T4GCtOcn1tuCizqbGZ7XmyRGGjUH6CVRH5gCsE0ixG/lIvg6Rt4V8K0m8BbHe6orWv7LXi46YEyjdx1+S7LWL/ZZdbIu/2CcshoXRQkyi4Dghj4PIYW78Dv2QuDHu77l2OJXoDDuDALs7/fn91k0QmeW1llyHZK9A7kzYct3OlvrNXutxm2FpkH4+2vBzvYhM91pLLmKTt9q2NX84GqOfYW2Qfkqq51X5YM8FeJZAfF7r8RICPSRupyX1udpIwU2MQT5G0aR54/1++wcW+E2tRzHkICTfBnCH+Hvviq24ebvXenIFY5CBqJw7skx8XxOLXxdgitdydELg96T9287o/Be81pKLGIOkoCgUPswPXCjAhSKSVueOnIVYBbHvju0q/v1Qf4+RLcYgaVAyOVzp88lMAU7LphTcA7aDfIqCp9knj3SubtjqtaB8wRgkQ4LTrhxlJ0ecLJAvCXAKHCmOdAaC3SCihDxj2fhzPpTE5CrGIA4RqIwcKYIQLR4rlMkEKna3HdLLbjPIKwSiIlhlJ+1VXS3zX9edd7hgDKKR4uPnHVxQkDiOsCZRMFbAUQLsR8g+IhwNyCikqJUi2AFgsxCbKNgslE02uFlENtkJea9rVf1r7vw2BoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGD7O/wcz9MgE8U3nuQAAA61pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0n77u/JyBpZD0nVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkJz8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0nYWRvYmU6bnM6bWV0YS8nPgo8cmRmOlJERiB4bWxuczpyZGY9J2h0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMnPgoKIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PScnCiAgeG1sbnM6QXR0cmliPSdodHRwOi8vbnMuYXR0cmlidXRpb24uY29tL2Fkcy8xLjAvJz4KICA8QXR0cmliOkFkcz4KICAgPHJkZjpTZXE+CiAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9J1Jlc291cmNlJz4KICAgICA8QXR0cmliOkNyZWF0ZWQ+MjAyMS0wMy0wOTwvQXR0cmliOkNyZWF0ZWQ+CiAgICAgPEF0dHJpYjpFeHRJZD44ZmZiNzg2OC01ZTI1LTQwMGUtODUzMy1kZGM0MTk5NTNiYjU8L0F0dHJpYjpFeHRJZD4KICAgICA8QXR0cmliOkZiSWQ+NTI1MjY1OTE0MTc5NTgwPC9BdHRyaWI6RmJJZD4KICAgICA8QXR0cmliOlRvdWNoVHlwZT4yPC9BdHRyaWI6VG91Y2hUeXBlPgogICAgPC9yZGY6bGk+CiAgIDwvcmRmOlNlcT4KICA8L0F0dHJpYjpBZHM+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnBkZj0naHR0cDovL25zLmFkb2JlLmNvbS9wZGYvMS4zLyc+CiAgPHBkZjpBdXRob3I+U3VwZXIgRmluc2VydiBQcml2YXRlIExpbWl0ZWQ8L3BkZjpBdXRob3I+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnhtcD0naHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyc+CiAgPHhtcDpDcmVhdG9yVG9vbD5DYW52YTwveG1wOkNyZWF0b3JUb29sPgogPC9yZGY6RGVzY3JpcHRpb24+CjwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9J3InPz4ci/WZAAAAAElFTkSuQmCC', NULL, '2021-03-28 07:33:32', '2021-03-28 07:33:32'),
(20, 10, 8, 'date', NULL, '08-08-1998', NULL, NULL, '2021-03-28 07:33:32', '2021-03-28 07:33:32'),
(21, 11, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:35:12', '2021-03-28 07:35:12'),
(22, 11, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:35:12', '2021-03-28 07:35:12'),
(23, 11, 5, 'file', NULL, '/home/redboxte/public_html/testing/public/assets/files/606031b05dbae.png', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAH6pJREFUeJztnXtgVNW59p93zyTkNoOigDdKVFoVb4FkwiVoj9VaWz32U1FQ256jrbW1PUetLcwEtWnVJFDx0p62VltvtQooVM+xar1Xi0gmAa9YK5pwEUhALDMJScjMfr4/MK1iwqyZ2WvvmWT9/iRrv++TMM+svdd+17sAg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYBka8FjDUCVRetX/SV1Ru2Xa5CMpFOJ6UcoDjRVAOSOBTF5FdBLoh6AHQDUi3ED0AYwTaIPIObbxHkbVdvbvewWs3drn/mw0PjEEcpCw092gLUkFIhYAVEFQCsq/uvAQ7APwdwAqx5fnYzhEvYE1dp+68wwFjkAwJVEf2s22e6rMwnZRKAY6HoMRrXR+jmcCzAF6Md4543hgmM4xB0iBQHTkB5FcBnCoix3qtJy3Ip23IMsvfszi24ubtXsvJF4xBUhCsmlNti3WBiMwU4GCv9TgC8QyBxbB3/THesnCb13JyGWOQgZh2ZXEgUfQ1AS6DoMJrOVohn7Zh3dEZrV/itZRcxBjkYwSnXTmKiaKICC8dcHVpCENyEyG/QW/fbZ2v3djhtZ5cwRgEQNmkyGjxISyC7+TYg7brkNglgocI/E+8qWGF13q8ZlgbJFB51f6QwggsflcgxV7ryTVILCfsSzuj89/0WotXDEuDBKoj+4GMQHCZMUZqCN6b7LMjO1cv2OS1FrcZXgaZWFcWLO2dC/BKiJS6mLmV4GtCeZWC1bCxNUnptpjoTthWt68g2dNlW91omb8DAFA5d2QJfCVJWCV+9JX4YJUkwZE+S44EeARFjgJ5lIgc4NYvQKBHwIWxnkTDcHpzP2wMEgzNvRxiXQtglM48BN4H8ATBZrHl9ThHrEZL3U4tySrnjgxYOFpgVVFYCSAkkKO05PoIkltEODfWNP9enXlyhSFvkKJJ88YX+O17RPB5HfFJfCDCZ21bnrF8iWdjK3/2jo48ylTWlZRZvSEBQhCcKcAJOtKQeNS25FtdK+vbdcTPFYa0QQLVkf8Q4OcAgo4GJt4FeH8S1rKuaP0rjsZ2mJJJcw7yF1izSDlXBNOcjc4Pkza+19Xc+EAmV5cd98Mx1oiC2RCOsomkgM/Eo/NfclZjdgxNg0ypCwbZ81tAznUqJIF2gPdLUhbHWhpWOhXXTYpD88b5JXmBEJdB5DNOxSWxjAl8p3N1w1bVawLVkTOEWLznsjqB38WbGr7llLZsGXIGCVTOnQFLFovIQU7EI3mfDevurmj9M07EyxXKQrXnCfjfIqhxJiI/hM0LY83zH081MlA196tiWQ8PGgl4ON7UcJYzurJjSBmkLBS+xhL5qROxSN4tSdwQW9W41ol4uUpJVXiST/ADEfmaMxH5g1hT482D/TRQHT5LIMtSRgFOjDc1vOiMpswZEgYpnVI71iIfEOCkbGOR/G2fzet6Wuavd0JbvlBUOfczBZbME5FvZxuLxF3xaMPFe/57WXX4XAuiVPNFGzfFmxuuylZLtuS9QYJT5kyl7ftfEYzOJs5wNcaeFFWEywsKcY1APvUBTweSz8d7cDZeb/wQAMqqamdbFtUf5sknYtHGL2ejwQny2iDpfCMNBsFXRezvxFYueNkpXUOBolD4sALBjwXyjYyDEO+SydNty1/pA/+Q5rXPxKINp2Sc2yHy1iCBUCQsgoaMA5A7bOLqzubG/3FQ1pCjrLL2JMviPRCMyzBEDBkssxN8Nt7UeHKGOR3D8lpAJgRCkZ9lYw6S99kJ+awxR2o6W+qfi8WSR5O4K8MQGb6Dkpz48vZ7LSBdPjLHDzO7mh8mbWtWV3PDU86qGuK8vSAeBy4uDYWXWCJ3CzDWa0lukVczSDbmIPlSX6/v2K7memOODOmKNj4hvp6JIFz4G5L6c6QmbwwSrA7XZWIOkiTRGI++e2L3qze8r0PbcCK24ubtseiI00jcqDeTucVSJhAKXwXIj9O9juQ2Eud2Njc8r0PX8KXOjkfxo9Lq2tUW+DsBirxWpIucn0ECocglIpL2txWJvyUse1Jnc6Mxhya6murvTyY4g8Rm56ObW6yUlIZqTxbB7eleR2BlvIfTu1cu2KhDl+Ff7FzV2ELgNq916CJnb7GCk8MTIExZs/MpyKfjH3adgbW/6NUgy7AHZVW137OEP3E+snkGGZzKuSPhw+NIcw19dw1QY1YlEgZ1gtXhKwHepCe6ucUalKDPWgzIhHSuIXjzQAVyBj3sNodoMgdgZpBBCIYi1wH4UjrXkLw7Hm38gSZJhj0oC0W+C0CjOXKHnDJIoDpyBoCr07mG5P/Fo40XaZJk2IOSUDhkCX6lP5O5xfoEJZPmHCTE79O6iGiKf9jl2LZaQ2p8gu+7k8ncYn0CX4G1FMA+quMJrIn38DSzWuUuAkx3J5OZQf5JsDpcJ5CpquNJbkn0Wqf2b8YxuIo7X6rEDlfypMBzg5RNnnMUIfNUx39UW3W+qavyBhKrXckDWeVGnlR4bhDL57tb0vlWEmkw5SPeYYv1GxfSbBd7V068nfd5mTxYPfe/IKLcA4nkS/Fo44U6NRn2Tt/7L75bePAJBwpQpSuHTXy5s3nBW7rip4N3Bqmo22eEz/4jALXu6sQ/+myekti8PCfuTYczu97/66MjDq5pBRGgoEzgUCNw4tfJpHyrq6WhyZF4DuDZUlogFL5RRJTbuiQhp3U11f9ZpyYveOmQ4Cj6i79GkXMFmAEAIFaTfMif6Lt36vvbTcGlh3hikI96MK0VkQKlC8g/xKKNDjU2857mw/Yd2ZMsnAkLs0GcJCKDzuQkVwJYVAh7UXXb1i0uyjTAI4MEQ5EHIJitOLwTvp7x+X508atjx5Z2juBZ8MlsAKcCil8O/ZA2RP4K2osk0bN4+sZYXv898gXXDVIaqq3wCZWXCm1bvt/ZXP9LnZp08c4EjOjYNfrf4fPNJnG6iDM770gmBXhGiMVWvO+hqdu3x5yIa/g0rhskUB1eofpSkMQb8WjDsbo1OUkzUNB36NjTksTs3edzSJnmlLsI/tmysai0B48c394+bE5/cgNXDVJWHf6CBVHukp5IsGrnqsYWnZqcgIDvpUPHnAzKbAJniYhyyYzDQroh+BNse9GYgo5HP7sWpgwnS1w1SCAUeUQEZyoNJn4dizZcpllSxhCQFeUHnEhgFgUzBdn1BnYagnEBHpEkFveub3/iJCDhtaZ8xDWDFB8/72B/YXKDSOoqTRK7IDgo3tTwgRva0mFF+dgpSWA2gPOcOoNENyQ+hHCZL4lFU9e3PydA0mtN+YJrBglUh28SyJWKw38Ra2r4b62C0mB5+egKwJoNyCwIyr3Wkx3sAOQhJpKLajZs/asAOVE1m6u4Y5CJdYXBst6tUNhjTmKXjcT4rujPPF3zXz5uzOHwWV8nMFsER3ipRSMbASwRYPH01i058/Y6l3DFIIHqyDcF+K3SYA+fPV48aN/PSGHhhRDMFshxXmjwCpLvAVjsFy6a2trxmtd6cgVXDBIMhVdBZFKqcQQSNhPj3Jw9mspHH9BHuQCWzAKk2q28OQ3xFoDFSNr31WzoeNdrOV6i3SAllbWT/T4qLdWSvD0ebbxUtybgo4dtwXUC+aIb+fIVAs2WbddPX9fxR6+1eIH23WE+i+erjk2q3oZlyfLxY66yRW7MiU3POY4AVbSsZcvLD/h1TdsWT259A5VX7Q9f4cUClti2bAZkeWdz/Rtu5Nb+GQmGIusVTydqjTU1HKZbz4ufGT3D8vk8Pz01LyHPr2lrX+RmytLq2gt8tG+H7FlSzwdiO+xL8faCuM78WncUBqfMmap6dJcN/k6nln4sny9nlo/zDbrW0WQ3paHwKT7wD582BwDI+cGR1nO6NWg1CGmdrTo2KfY9OrX8E3KyK3mGIAIJ0cV3ZxZwzd5HSGWgOqy8IzVDDVo5Q2UQyRfc68Qu+7qTZ0hS2HRwYJQ7qc71QVCTapQQWZ/rvje0GaSksvZAgRylNFjSbBhnGPIUTzn0QMHgG8n+iUgIFXXaikO1GcQSKN9exZN8UJcOQ37SvXLBRgI9KmPL/D3ajovWZhARW1E0W9Ay3zRiMHwKAV9QGic4VZcGjc8gcqLKKALP6tNgyGdo42mVcQKcokuDFoMUhcKHiWA/lbFiU/tSnSE/SVo+tc+GyGG6nkO0GKQQVGpwTCARY/FfdGgw5D87mwpWAehUGVta0KPc2zkdtBjEhhVSGkg2oaVupw4NhqFAnU2o3WZZQMpi2EzQYhARNbECMc8fhr1CsFlxaKWO/JqKFVml8sKVRFRP/uHJiEPGoejgQwAAO1au8FiNM1jgKyqfJRHRUiHhuEFGVF59uCCp1G9XbK5xOv9wZMw55+HAi76N0olHf+LfOx5ajA0/X4jejRs8UpY9ffS/ViB2ynEky/FvdX48X+docwrHb7FGSFJpeyoJO7aqcVhvxnGCCQtuwYSf3fopcwDAmJmzcPyjT6N04jEeKHOG7ugNGwCk7CIpIhKI96Z1MrIKjhvEhhyqOHQNTMOArJiw4BaMmTlrr2P8wSCOvn8pRhyiVFSdkxB4VWWciK362VPGcYOoihRhTpz/kK8cdPG3U5qjH38wiEOv+almRTphq8ooG3K405mdX8USGa80jmIMkiEjp05H+dU/SeuaUV88LW9nEQHWq41TvntRRscy71iVQUnybxpyD3n8wZE44ra7Mrq2KE8NQojSVggRHux0bg0G4f5KwwTmhNoMOPr+pfAHU7YXG5B8fVi3FQ1CkTFO59Yxg6jVYFGtlNnwLyYsuGXA1SpVuta40ufAcUSotk5N5/sjO/+QDjUXW5Rup3MPZcbMnKX8UD4YXWvedEiNuyR7LNXtEEq39+ng2THQSZ9tZhBFSiceg/Krs1uF6li6BIlYfm676baTSvV6InkwgxBU6hxu2TQGUcAfHIkjb7sz4+cOAEjGY2i77loHVblMd5dnBa06ZpBdKoP6kgXGIAoccdudWS/PvnH+OXk7ewAA1v7Cs4OAnH8GoZpBfIVmBknFuMt/iJFTlbbWDErb9T/O24fzT0B6crSc48WKFOkRYKTTcfMBf3AkxpxzHvb94mkonXgMejduQNeaN7B12RLsePmltGKNOvXLGHe58jHyA9KxdAk23Xl7VjFyBhFPypIcN4iA3SrlyUlaJU7n9pIxM2eh/OqffuJZwT/xaJROPBpjZs7CjpdfwtvfuVjpVqd04jGYsOCWrPR0vfVmfj93fBrdh6EOiIaHdCgt3/pFrSQ+HxgzcxYmLLhlrw/SI6dOx/GPPpXyZZ0/ODJlrFQk4zH87dKL8vu54+NMqcv8j5ElGp5B1N5v2B7NIBSk3lyQBiMOGae8BDvikHE4+v6le32uKL/6J1m9DASAd350hbY9ILt8lqN/PxWKe/oCbufsx7MZxGLCE4MI4OjD3phzZqX1bd9ffj7QS790KnQHY8OtC7H9ycezirE3Tli/w/USIb/fVjIIgXanc2sod1dbbRDCmx65TL35Jh2CGa4yTVhwyydmnkwqdPdk+1NPYMOtN2YVY28QVOow4jRJi6oziOMGcX5POrFVpf83IR6VlvJDJxuUj5wyLeNrD7r4EviDQWz4+cKMK3T76X1/I9b+6IqsYqRGHP1yUc4KK6iyt06YBzMIRd5XGScijpcmKyHi6C1C7/vZNaUfM3MWKl9oyouHciE9MohaASyFm53OraFY0Vb7xAi8MYjt7H9yLryEa73uWrd0eGIQS+yJSgMpHY7ndjqgTUtpBgFwiNO5VaBgm5PxOpYucTJc2my66w50PLTYlVyEs3+7NBIfqTgy92+xLCbVdn8B5U7nVsrrcKuh7U8+7lkPKvdfBtKTenmK2jkzefEMkqBfdQH+UBwxx/X1bb/fbnI65tuXXoSut9z97CTjMbx5/jmu5rSAla4m/BefUxlEC21OJ3bcIDtb6jerFpYFRvoqnM6fiinvbfs7QEefZhOxHXjz/HNcnUm8qNC1O+30CsocYHcjQhSpjI37elTblCqjZcMURVYpjbN5vI78KfPC+Zanu01ytivPJN5U6PKdGdu2aT1yeSAKfUm1Y/yId7HiZsd3qeppXg28ojRO4IlBBHD8NquftT+6HG3X/1hXeC8rdD25vRJC7aQAodKXcrromUFsqhkEUqUjfyqsJLQe2rPpztuxds4VSMZjjsb1skJXbL1/s8Gg8AtK46h215IuWgyStHyvKQ0UVODYsOslJ1PXtz/rdMnJnnQ8tBhvnH+OYybxskKXZLJYepe5nnjCf40AROlgHFtktQ4JWgyys6NA+Qa5rFjf+XKDIYBNQPvDQteaN/DK6ac4ssKls0I3FSJ4YlLbP/7hdt7SUYF/E8VyqK5k8mUdGvR0NWmr6yEVTyil+wYBAB/gytu13o0bsl7h0l2hmwrafMCLvBbtkxSHrtN1UrK2tj+EPKMyTiDazrjeG9PatjxPcosbubJZ4dJdoZsKEj1jCzoe8iK3AIrPH3hSlwZtBrGspJpoweEllbUH6tKx99xyn5vp0l3hcqdCNxVc9tm1cL+ryJS6IEQUV7DwmC4Z2gwSW7ngZSieUOrz0fGDT1QoZHIhFNsUOYXqCldObJsl6WPiei9SB5I9M1XGEUzGkyPybwYBABLPq4yzk7brD4AAUN22dQth3+F23v4VrsGeSzqWLkHLCdXeVwqLLJu27gNvjqkQfF1x4As6T0rWdIhnP1wKyBl7HQFu7GqZ/7peHYNTSF7fJ7gEQKGbebvWvIE3zz9798Gbh4xD6cRj0LXmDXSteTM3mi2QtJi4xovUJZPmHATg80qDbWpdvdA6g8SjjXeD3OvhJ2LLpTo1pKK6besWEnd6lb934wbsePklbLrzdux4+aXcMAcAAg97NXv4/NZ/iIjStk8KtT1/AC40r+6Dbwb56TJpgt0kLo41N2j9BVWwEn3X06POfTmLJCPe5Va8vSLXd0bnay2j1m6Q7ugNG+LRxmMIXkLwXpLP2+StyaR1eDzakN1GbIeYvvGD9y0wuzaGQwpeM6N129teZC6ZHK4UqO3/AHCPVjFwsnvBEGD5oQf8BcCJXuvwEpKv1rS1Vwqg1KXfaQLVkdsEULrt7pPkuO6VC7JrCpACz84HyU3kG6BaX6+hCMGEhcRsr8xRUll7IIiLVMYS/JNucwDGIJ+gpnXzOgrmeK3DM8jrprd94Nnhqn6L80QUVxPJ32iWA8DcYg3I8kMPeBCA0ouqoQLBp2e0tn/Rq/wllbUH+iy2qRiE5JZ4tOhgoE57G1QzgwyA9G35Bohhc447gfX+HX3ubnDfA8uy56rOHiK4zQ1zAMYgAzJ9I7qRtP8dhGdHf7kFgV5J8PSp27c7u7srDcqO++EYEfX3YYk+96ofjEEGoWZDx7sC+0KvdehGYF9Ss6Hd05oWa0TBtaqNGUjevnP1gk26NfVjDLIXprd1PAwb3/Nahy4I+9qa1o7fe6mhrKr2GIKXqYwlkOizeYNuTR/HGCQFNeu2/ArAQq91OA3B381o7bjOYxliWfbdqmUlIO/raZm/19IlpzEGUaCmdcsPATzotQ6nIPFETWv7JV7rKAtFvgNIpcpYErYk4ersARiDKFPTuuU8kI94rSNbCD7Vhy1nicp5AhoJVEf2s4B69St4f2xV41p9igbGGCQNpre1n4O8nkn4aF9r+1dOakMuHMF9MwT7qAz8aPbI7nShDDEGSQMBkh/NJJ6Vx2cKiXumt7afeRKQ8FpLoCpypkB1QxQA8E4vZg8gx96kB6rmHAGxLoHI91SX/VJB4g2b8oOu5vqnnIjXz1/Lx9aLiHcl4WnB+prW9nleqwCA4ilzDimwrTcgMlJlPInNcds+SlfXklTkzAxSUlk7WcRqEZGrnDIHAIjgGJ/FJ4Oh8C+digkAM9raa2HjjFzeR0KyywLPzBVzAIDf9i1SNQcAQHCZV+YAcsUgE+vK/BYfhkipthwil5VW117gZMiadVv+VGAlJ5PwZO/E3iDZaiXsimmt7f/ntZZ+gqHIdSKoUR1P4KF4U8PDOjWlIicMUlbScwEE2g/1tKj2Qiodpry37e/oTIRAunPMkwIkl6Azefz0jVs9uW8fiLLK2pMguDqNS2K2yPe1CVIkJwxiWTjWjTzpfHulw4xt2+I1be2zafNbnu4nIbqF9qUz2tpneXFUwWCUTJpzkFj8QzrXkLy8a2W94ydGpUtOGISQAteSldc59nyzJzPWtf9O0DeZhKPHvKnB1wV9k6e3dXhyNsKgVNTt4/NbT4pAuTkgwT/Fo41365SlSk4YxEWa0Van9R3A9LYP/lbUtqVCyIgrswmxE0R4V2v7ZC83Ow3ItCuLAwW9j4nI0aqXkNwS705nCVgvw8ogNulKk9sqoG96W3ujbfMYkNq6/pF4HCITa9q2zM+F9xt7EkgULRLBtLQuEjkPrzc6epZ9NmhuHJc7kPxtZ7TR1QfpE9a3vwfgS8vHjzmHlnWzwKGFCKIN4BUz2tpztvQlEIrcKYIz07nGJn7cGW14UZemTBgmMwgfiEcbPSvOq1nXsbSo1zqSZGO2sWjjul3YclRNLpujKrJQRK35Qj8kX+iMNvxUl6ZMGfIzCIHH4k3ven5PW7Vp004AkRc/M/YOy8IdEFFq7f8v+ChgfX/Gus3rtAh0hDorGOq5F4K0NpqR+MC2rPN0qcqGoW0Q8ol4tPF0r2V8nI9uu05efuiYU0Gpg8he79FJPgegdkZbu5YTlByjvK4oMLr3QcjeezEPBIFzcmFJdyCGrEEIvhzfWnSW1zoGo6a140kAT/51/AFfgYWfCPCJA01JvuyDRKa1tSt1yPeUyrkjg77ep7HH75AKgklA/l9ntOEvmpRlzZA1CChlxaP7Rne3wZuD/RSZsW7LYwAeayoffUCC1hcgLEOCz9bk0FvwvVE8Zc4hflueBnBEOteRpE18vau54VFN0hwhJ6p502k3mQ4kPgDsM+PR+S85Hduwu3xELHuJiOyf7rU2+N3OpsbbdOhykiG9iiWC/QDruWAoPOS7k7jLub5AdeQGsfh0JuYAWJcP5gCGuEEAQASFELmvLBS52WstQ4GSytoDA9WHPy9ArUj6nx+buCXW1OjJ7sBMGPIG6ccSXBGsjkRLK+e6Uhg5FCmtDJ/q99lvCmRGJtfb5LWd0YYrndalk6H7kD4wVZbPWhUIhRfG/b0/wYqbh20n93QomxQZLQVYmN422X9BkoBc2hltdP08yGwZNjNIPwL4RWRuMFn0Vmll+FSv9eQ2dVZZVfj7lp/vZGwOIEFaF8SjDXlnDiCPDULgMZv250luyzDEeJ9P/hwMhRcVVYTLHRU3BAhWRqYEQ70tliW/SGuL7B6Ija92NtcvclKbm+SEQQRMq0fT7vKRhtM7o/NfSNrWcQCaM08uswoLpTVQHb4Jx4b3zTjOEKE4NG9cIBS+Az68DEFFpnFIbCVwYi6cQZkNOWEQAMplBgQfiTc1/LN8ZGdL/eZYU0OI4L3ZCBDIlcFivBsMzb08mzj5SlFFuDwQitxZIPZ6EflWVsGIZ2xLjo035VZlbibkxIvCstDcEy2xUpYbkPxjPNp49mA/D1aHrwTkJgckrQPshbHO4ruwpq7TgXg5S3ByeAL9uEYg33AiHsFwvKlxvhOxcoGcMAgABEPhJoiEBvs5yaXxaGPKU5/KQpHPW4J7AIzPXhXjtOWOPtq3ut00WTdl1eEvWJBvAnCm0wu5HracF2tpWOlIvBwhZwxSVBEuLyiQx0Vw5AA/vj/W1KD+NvyIOYFA0LpRRL7tnEI+aFN+mcuFdakIVkam0MdZIGaJyEHOReaDsSQv8bJ/lS5yxiD9BKsiV0DwFQKFEKxPJuWWnS31qzKJVVoZPtWycJeTHwaCG0FZJDYeyodvy9JQbYUP9tkQnA/IBCdjE2gncVlntGGZk3FziZwziONUzh0ZsOQWEflP54OzjZAlAB6ONzWscD5++gSqI5+jjZMswRcgOAXAKB15CN4b31V0OV6p+4eO+LnC0DfIR5SGak+2wF+J4HPakhCvAFhBYHWSbN7Z3LhaWy4AqKwrCVjdFRQ5TiBVAE4T4GCtOcn1tuCizqbGZ7XmyRGGjUH6CVRH5gCsE0ixG/lIvg6Rt4V8K0m8BbHe6orWv7LXi46YEyjdx1+S7LWL/ZZdbIu/2CcshoXRQkyi4Dghj4PIYW78Dv2QuDHu77l2OJXoDDuDALs7/fn91k0QmeW1llyHZK9A7kzYct3OlvrNXutxm2FpkH4+2vBzvYhM91pLLmKTt9q2NX84GqOfYW2Qfkqq51X5YM8FeJZAfF7r8RICPSRupyX1udpIwU2MQT5G0aR54/1++wcW+E2tRzHkICTfBnCH+Hvviq24ebvXenIFY5CBqJw7skx8XxOLXxdgitdydELg96T9287o/Be81pKLGIOkoCgUPswPXCjAhSKSVueOnIVYBbHvju0q/v1Qf4+RLcYgaVAyOVzp88lMAU7LphTcA7aDfIqCp9knj3SubtjqtaB8wRgkQ4LTrhxlJ0ecLJAvCXAKHCmOdAaC3SCihDxj2fhzPpTE5CrGIA4RqIwcKYIQLR4rlMkEKna3HdLLbjPIKwSiIlhlJ+1VXS3zX9edd7hgDKKR4uPnHVxQkDiOsCZRMFbAUQLsR8g+IhwNyCikqJUi2AFgsxCbKNgslE02uFlENtkJea9rVf1r7vw2BoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGD7O/wcz9MgE8U3nuQAAA61pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0n77u/JyBpZD0nVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkJz8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0nYWRvYmU6bnM6bWV0YS8nPgo8cmRmOlJERiB4bWxuczpyZGY9J2h0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMnPgoKIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PScnCiAgeG1sbnM6QXR0cmliPSdodHRwOi8vbnMuYXR0cmlidXRpb24uY29tL2Fkcy8xLjAvJz4KICA8QXR0cmliOkFkcz4KICAgPHJkZjpTZXE+CiAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9J1Jlc291cmNlJz4KICAgICA8QXR0cmliOkNyZWF0ZWQ+MjAyMS0wMy0wOTwvQXR0cmliOkNyZWF0ZWQ+CiAgICAgPEF0dHJpYjpFeHRJZD44ZmZiNzg2OC01ZTI1LTQwMGUtODUzMy1kZGM0MTk5NTNiYjU8L0F0dHJpYjpFeHRJZD4KICAgICA8QXR0cmliOkZiSWQ+NTI1MjY1OTE0MTc5NTgwPC9BdHRyaWI6RmJJZD4KICAgICA8QXR0cmliOlRvdWNoVHlwZT4yPC9BdHRyaWI6VG91Y2hUeXBlPgogICAgPC9yZGY6bGk+CiAgIDwvcmRmOlNlcT4KICA8L0F0dHJpYjpBZHM+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnBkZj0naHR0cDovL25zLmFkb2JlLmNvbS9wZGYvMS4zLyc+CiAgPHBkZjpBdXRob3I+U3VwZXIgRmluc2VydiBQcml2YXRlIExpbWl0ZWQ8L3BkZjpBdXRob3I+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnhtcD0naHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyc+CiAgPHhtcDpDcmVhdG9yVG9vbD5DYW52YTwveG1wOkNyZWF0b3JUb29sPgogPC9yZGY6RGVzY3JpcHRpb24+CjwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9J3InPz4ci/WZAAAAAElFTkSuQmCC', NULL, '2021-03-28 07:35:12', '2021-03-28 07:35:12'),
(24, 11, 8, 'date', NULL, '08-08-1998', NULL, NULL, '2021-03-28 07:35:12', '2021-03-28 07:35:12'),
(25, 12, 1, 'radio', '1', NULL, NULL, NULL, '2021-03-28 07:36:03', '2021-03-28 07:36:03'),
(26, 12, 9, 'checkbox', '20,21', NULL, NULL, NULL, '2021-03-28 07:36:03', '2021-03-28 07:36:03'),
(27, 12, 5, 'file', NULL, '606031e36ef91.png', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAH6pJREFUeJztnXtgVNW59p93zyTkNoOigDdKVFoVb4FkwiVoj9VaWz32U1FQ256jrbW1PUetLcwEtWnVJFDx0p62VltvtQooVM+xar1Xi0gmAa9YK5pwEUhALDMJScjMfr4/MK1iwqyZ2WvvmWT9/iRrv++TMM+svdd+17sAg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYBka8FjDUCVRetX/SV1Ru2Xa5CMpFOJ6UcoDjRVAOSOBTF5FdBLoh6AHQDUi3ED0AYwTaIPIObbxHkbVdvbvewWs3drn/mw0PjEEcpCw092gLUkFIhYAVEFQCsq/uvAQ7APwdwAqx5fnYzhEvYE1dp+68wwFjkAwJVEf2s22e6rMwnZRKAY6HoMRrXR+jmcCzAF6Md4543hgmM4xB0iBQHTkB5FcBnCoix3qtJy3Ip23IMsvfszi24ubtXsvJF4xBUhCsmlNti3WBiMwU4GCv9TgC8QyBxbB3/THesnCb13JyGWOQgZh2ZXEgUfQ1AS6DoMJrOVohn7Zh3dEZrV/itZRcxBjkYwSnXTmKiaKICC8dcHVpCENyEyG/QW/fbZ2v3djhtZ5cwRgEQNmkyGjxISyC7+TYg7brkNglgocI/E+8qWGF13q8ZlgbJFB51f6QwggsflcgxV7ryTVILCfsSzuj89/0WotXDEuDBKoj+4GMQHCZMUZqCN6b7LMjO1cv2OS1FrcZXgaZWFcWLO2dC/BKiJS6mLmV4GtCeZWC1bCxNUnptpjoTthWt68g2dNlW91omb8DAFA5d2QJfCVJWCV+9JX4YJUkwZE+S44EeARFjgJ5lIgc4NYvQKBHwIWxnkTDcHpzP2wMEgzNvRxiXQtglM48BN4H8ATBZrHl9ThHrEZL3U4tySrnjgxYOFpgVVFYCSAkkKO05PoIkltEODfWNP9enXlyhSFvkKJJ88YX+O17RPB5HfFJfCDCZ21bnrF8iWdjK3/2jo48ylTWlZRZvSEBQhCcKcAJOtKQeNS25FtdK+vbdcTPFYa0QQLVkf8Q4OcAgo4GJt4FeH8S1rKuaP0rjsZ2mJJJcw7yF1izSDlXBNOcjc4Pkza+19Xc+EAmV5cd98Mx1oiC2RCOsomkgM/Eo/NfclZjdgxNg0ypCwbZ81tAznUqJIF2gPdLUhbHWhpWOhXXTYpD88b5JXmBEJdB5DNOxSWxjAl8p3N1w1bVawLVkTOEWLznsjqB38WbGr7llLZsGXIGCVTOnQFLFovIQU7EI3mfDevurmj9M07EyxXKQrXnCfjfIqhxJiI/hM0LY83zH081MlA196tiWQ8PGgl4ON7UcJYzurJjSBmkLBS+xhL5qROxSN4tSdwQW9W41ol4uUpJVXiST/ADEfmaMxH5g1hT482D/TRQHT5LIMtSRgFOjDc1vOiMpswZEgYpnVI71iIfEOCkbGOR/G2fzet6Wuavd0JbvlBUOfczBZbME5FvZxuLxF3xaMPFe/57WXX4XAuiVPNFGzfFmxuuylZLtuS9QYJT5kyl7ftfEYzOJs5wNcaeFFWEywsKcY1APvUBTweSz8d7cDZeb/wQAMqqamdbFtUf5sknYtHGL2ejwQny2iDpfCMNBsFXRezvxFYueNkpXUOBolD4sALBjwXyjYyDEO+SydNty1/pA/+Q5rXPxKINp2Sc2yHy1iCBUCQsgoaMA5A7bOLqzubG/3FQ1pCjrLL2JMviPRCMyzBEDBkssxN8Nt7UeHKGOR3D8lpAJgRCkZ9lYw6S99kJ+awxR2o6W+qfi8WSR5O4K8MQGb6Dkpz48vZ7LSBdPjLHDzO7mh8mbWtWV3PDU86qGuK8vSAeBy4uDYWXWCJ3CzDWa0lukVczSDbmIPlSX6/v2K7memOODOmKNj4hvp6JIFz4G5L6c6QmbwwSrA7XZWIOkiTRGI++e2L3qze8r0PbcCK24ubtseiI00jcqDeTucVSJhAKXwXIj9O9juQ2Eud2Njc8r0PX8KXOjkfxo9Lq2tUW+DsBirxWpIucn0ECocglIpL2txWJvyUse1Jnc6Mxhya6murvTyY4g8Rm56ObW6yUlIZqTxbB7eleR2BlvIfTu1cu2KhDl+Ff7FzV2ELgNq916CJnb7GCk8MTIExZs/MpyKfjH3adgbW/6NUgy7AHZVW137OEP3E+snkGGZzKuSPhw+NIcw19dw1QY1YlEgZ1gtXhKwHepCe6ucUalKDPWgzIhHSuIXjzQAVyBj3sNodoMgdgZpBBCIYi1wH4UjrXkLw7Hm38gSZJhj0oC0W+C0CjOXKHnDJIoDpyBoCr07mG5P/Fo40XaZJk2IOSUDhkCX6lP5O5xfoEJZPmHCTE79O6iGiKf9jl2LZaQ2p8gu+7k8ncYn0CX4G1FMA+quMJrIn38DSzWuUuAkx3J5OZQf5JsDpcJ5CpquNJbkn0Wqf2b8YxuIo7X6rEDlfypMBzg5RNnnMUIfNUx39UW3W+qavyBhKrXckDWeVGnlR4bhDL57tb0vlWEmkw5SPeYYv1GxfSbBd7V068nfd5mTxYPfe/IKLcA4nkS/Fo44U6NRn2Tt/7L75bePAJBwpQpSuHTXy5s3nBW7rip4N3Bqmo22eEz/4jALXu6sQ/+myekti8PCfuTYczu97/66MjDq5pBRGgoEzgUCNw4tfJpHyrq6WhyZF4DuDZUlogFL5RRJTbuiQhp3U11f9ZpyYveOmQ4Cj6i79GkXMFmAEAIFaTfMif6Lt36vvbTcGlh3hikI96MK0VkQKlC8g/xKKNDjU2857mw/Yd2ZMsnAkLs0GcJCKDzuQkVwJYVAh7UXXb1i0uyjTAI4MEQ5EHIJitOLwTvp7x+X508atjx5Z2juBZ8MlsAKcCil8O/ZA2RP4K2osk0bN4+sZYXv898gXXDVIaqq3wCZWXCm1bvt/ZXP9LnZp08c4EjOjYNfrf4fPNJnG6iDM770gmBXhGiMVWvO+hqdu3x5yIa/g0rhskUB1eofpSkMQb8WjDsbo1OUkzUNB36NjTksTs3edzSJnmlLsI/tmysai0B48c394+bE5/cgNXDVJWHf6CBVHukp5IsGrnqsYWnZqcgIDvpUPHnAzKbAJniYhyyYzDQroh+BNse9GYgo5HP7sWpgwnS1w1SCAUeUQEZyoNJn4dizZcpllSxhCQFeUHnEhgFgUzBdn1BnYagnEBHpEkFveub3/iJCDhtaZ8xDWDFB8/72B/YXKDSOoqTRK7IDgo3tTwgRva0mFF+dgpSWA2gPOcOoNENyQ+hHCZL4lFU9e3PydA0mtN+YJrBglUh28SyJWKw38Ra2r4b62C0mB5+egKwJoNyCwIyr3Wkx3sAOQhJpKLajZs/asAOVE1m6u4Y5CJdYXBst6tUNhjTmKXjcT4rujPPF3zXz5uzOHwWV8nMFsER3ipRSMbASwRYPH01i058/Y6l3DFIIHqyDcF+K3SYA+fPV48aN/PSGHhhRDMFshxXmjwCpLvAVjsFy6a2trxmtd6cgVXDBIMhVdBZFKqcQQSNhPj3Jw9mspHH9BHuQCWzAKk2q28OQ3xFoDFSNr31WzoeNdrOV6i3SAllbWT/T4qLdWSvD0ebbxUtybgo4dtwXUC+aIb+fIVAs2WbddPX9fxR6+1eIH23WE+i+erjk2q3oZlyfLxY66yRW7MiU3POY4AVbSsZcvLD/h1TdsWT259A5VX7Q9f4cUClti2bAZkeWdz/Rtu5Nb+GQmGIusVTydqjTU1HKZbz4ufGT3D8vk8Pz01LyHPr2lrX+RmytLq2gt8tG+H7FlSzwdiO+xL8faCuM78WncUBqfMmap6dJcN/k6nln4sny9nlo/zDbrW0WQ3paHwKT7wD582BwDI+cGR1nO6NWg1CGmdrTo2KfY9OrX8E3KyK3mGIAIJ0cV3ZxZwzd5HSGWgOqy8IzVDDVo5Q2UQyRfc68Qu+7qTZ0hS2HRwYJQ7qc71QVCTapQQWZ/rvje0GaSksvZAgRylNFjSbBhnGPIUTzn0QMHgG8n+iUgIFXXaikO1GcQSKN9exZN8UJcOQ37SvXLBRgI9KmPL/D3ajovWZhARW1E0W9Ay3zRiMHwKAV9QGic4VZcGjc8gcqLKKALP6tNgyGdo42mVcQKcokuDFoMUhcKHiWA/lbFiU/tSnSE/SVo+tc+GyGG6nkO0GKQQVGpwTCARY/FfdGgw5D87mwpWAehUGVta0KPc2zkdtBjEhhVSGkg2oaVupw4NhqFAnU2o3WZZQMpi2EzQYhARNbECMc8fhr1CsFlxaKWO/JqKFVml8sKVRFRP/uHJiEPGoejgQwAAO1au8FiNM1jgKyqfJRHRUiHhuEFGVF59uCCp1G9XbK5xOv9wZMw55+HAi76N0olHf+LfOx5ajA0/X4jejRs8UpY9ffS/ViB2ynEky/FvdX48X+docwrHb7FGSFJpeyoJO7aqcVhvxnGCCQtuwYSf3fopcwDAmJmzcPyjT6N04jEeKHOG7ugNGwCk7CIpIhKI96Z1MrIKjhvEhhyqOHQNTMOArJiw4BaMmTlrr2P8wSCOvn8pRhyiVFSdkxB4VWWciK362VPGcYOoihRhTpz/kK8cdPG3U5qjH38wiEOv+almRTphq8ooG3K405mdX8USGa80jmIMkiEjp05H+dU/SeuaUV88LW9nEQHWq41TvntRRscy71iVQUnybxpyD3n8wZE44ra7Mrq2KE8NQojSVggRHux0bg0G4f5KwwTmhNoMOPr+pfAHU7YXG5B8fVi3FQ1CkTFO59Yxg6jVYFGtlNnwLyYsuGXA1SpVuta40ufAcUSotk5N5/sjO/+QDjUXW5Rup3MPZcbMnKX8UD4YXWvedEiNuyR7LNXtEEq39+ng2THQSZ9tZhBFSiceg/Krs1uF6li6BIlYfm676baTSvV6InkwgxBU6hxu2TQGUcAfHIkjb7sz4+cOAEjGY2i77loHVblMd5dnBa06ZpBdKoP6kgXGIAoccdudWS/PvnH+OXk7ewAA1v7Cs4OAnH8GoZpBfIVmBknFuMt/iJFTlbbWDErb9T/O24fzT0B6crSc48WKFOkRYKTTcfMBf3AkxpxzHvb94mkonXgMejduQNeaN7B12RLsePmltGKNOvXLGHe58jHyA9KxdAk23Xl7VjFyBhFPypIcN4iA3SrlyUlaJU7n9pIxM2eh/OqffuJZwT/xaJROPBpjZs7CjpdfwtvfuVjpVqd04jGYsOCWrPR0vfVmfj93fBrdh6EOiIaHdCgt3/pFrSQ+HxgzcxYmLLhlrw/SI6dOx/GPPpXyZZ0/ODJlrFQk4zH87dKL8vu54+NMqcv8j5ElGp5B1N5v2B7NIBSk3lyQBiMOGae8BDvikHE4+v6le32uKL/6J1m9DASAd350hbY9ILt8lqN/PxWKe/oCbufsx7MZxGLCE4MI4OjD3phzZqX1bd9ffj7QS790KnQHY8OtC7H9ycezirE3Tli/w/USIb/fVjIIgXanc2sod1dbbRDCmx65TL35Jh2CGa4yTVhwyydmnkwqdPdk+1NPYMOtN2YVY28QVOow4jRJi6oziOMGcX5POrFVpf83IR6VlvJDJxuUj5wyLeNrD7r4EviDQWz4+cKMK3T76X1/I9b+6IqsYqRGHP1yUc4KK6iyt06YBzMIRd5XGScijpcmKyHi6C1C7/vZNaUfM3MWKl9oyouHciE9MohaASyFm53OraFY0Vb7xAi8MYjt7H9yLryEa73uWrd0eGIQS+yJSgMpHY7ndjqgTUtpBgFwiNO5VaBgm5PxOpYucTJc2my66w50PLTYlVyEs3+7NBIfqTgy92+xLCbVdn8B5U7nVsrrcKuh7U8+7lkPKvdfBtKTenmK2jkzefEMkqBfdQH+UBwxx/X1bb/fbnI65tuXXoSut9z97CTjMbx5/jmu5rSAla4m/BefUxlEC21OJ3bcIDtb6jerFpYFRvoqnM6fiinvbfs7QEefZhOxHXjz/HNcnUm8qNC1O+30CsocYHcjQhSpjI37elTblCqjZcMURVYpjbN5vI78KfPC+Zanu01ytivPJN5U6PKdGdu2aT1yeSAKfUm1Y/yId7HiZsd3qeppXg28ojRO4IlBBHD8NquftT+6HG3X/1hXeC8rdD25vRJC7aQAodKXcrromUFsqhkEUqUjfyqsJLQe2rPpztuxds4VSMZjjsb1skJXbL1/s8Gg8AtK46h215IuWgyStHyvKQ0UVODYsOslJ1PXtz/rdMnJnnQ8tBhvnH+OYybxskKXZLJYepe5nnjCf40AROlgHFtktQ4JWgyys6NA+Qa5rFjf+XKDIYBNQPvDQteaN/DK6ac4ssKls0I3FSJ4YlLbP/7hdt7SUYF/E8VyqK5k8mUdGvR0NWmr6yEVTyil+wYBAB/gytu13o0bsl7h0l2hmwrafMCLvBbtkxSHrtN1UrK2tj+EPKMyTiDazrjeG9PatjxPcosbubJZ4dJdoZsKEj1jCzoe8iK3AIrPH3hSlwZtBrGspJpoweEllbUH6tKx99xyn5vp0l3hcqdCNxVc9tm1cL+ryJS6IEQUV7DwmC4Z2gwSW7ngZSieUOrz0fGDT1QoZHIhFNsUOYXqCldObJsl6WPiei9SB5I9M1XGEUzGkyPybwYBABLPq4yzk7brD4AAUN22dQth3+F23v4VrsGeSzqWLkHLCdXeVwqLLJu27gNvjqkQfF1x4As6T0rWdIhnP1wKyBl7HQFu7GqZ/7peHYNTSF7fJ7gEQKGbebvWvIE3zz9798Gbh4xD6cRj0LXmDXSteTM3mi2QtJi4xovUJZPmHATg80qDbWpdvdA6g8SjjXeD3OvhJ2LLpTo1pKK6besWEnd6lb934wbsePklbLrzdux4+aXcMAcAAg97NXv4/NZ/iIjStk8KtT1/AC40r+6Dbwb56TJpgt0kLo41N2j9BVWwEn3X06POfTmLJCPe5Va8vSLXd0bnay2j1m6Q7ugNG+LRxmMIXkLwXpLP2+StyaR1eDzakN1GbIeYvvGD9y0wuzaGQwpeM6N129teZC6ZHK4UqO3/AHCPVjFwsnvBEGD5oQf8BcCJXuvwEpKv1rS1Vwqg1KXfaQLVkdsEULrt7pPkuO6VC7JrCpACz84HyU3kG6BaX6+hCMGEhcRsr8xRUll7IIiLVMYS/JNucwDGIJ+gpnXzOgrmeK3DM8jrprd94Nnhqn6L80QUVxPJ32iWA8DcYg3I8kMPeBCA0ouqoQLBp2e0tn/Rq/wllbUH+iy2qRiE5JZ4tOhgoE57G1QzgwyA9G35Bohhc447gfX+HX3ubnDfA8uy56rOHiK4zQ1zAMYgAzJ9I7qRtP8dhGdHf7kFgV5J8PSp27c7u7srDcqO++EYEfX3YYk+96ofjEEGoWZDx7sC+0KvdehGYF9Ss6Hd05oWa0TBtaqNGUjevnP1gk26NfVjDLIXprd1PAwb3/Nahy4I+9qa1o7fe6mhrKr2GIKXqYwlkOizeYNuTR/HGCQFNeu2/ArAQq91OA3B381o7bjOYxliWfbdqmUlIO/raZm/19IlpzEGUaCmdcsPATzotQ6nIPFETWv7JV7rKAtFvgNIpcpYErYk4ersARiDKFPTuuU8kI94rSNbCD7Vhy1nicp5AhoJVEf2s4B69St4f2xV41p9igbGGCQNpre1n4O8nkn4aF9r+1dOakMuHMF9MwT7qAz8aPbI7nShDDEGSQMBkh/NJJ6Vx2cKiXumt7afeRKQ8FpLoCpypkB1QxQA8E4vZg8gx96kB6rmHAGxLoHI91SX/VJB4g2b8oOu5vqnnIjXz1/Lx9aLiHcl4WnB+prW9nleqwCA4ilzDimwrTcgMlJlPInNcds+SlfXklTkzAxSUlk7WcRqEZGrnDIHAIjgGJ/FJ4Oh8C+digkAM9raa2HjjFzeR0KyywLPzBVzAIDf9i1SNQcAQHCZV+YAcsUgE+vK/BYfhkipthwil5VW117gZMiadVv+VGAlJ5PwZO/E3iDZaiXsimmt7f/ntZZ+gqHIdSKoUR1P4KF4U8PDOjWlIicMUlbScwEE2g/1tKj2Qiodpry37e/oTIRAunPMkwIkl6Azefz0jVs9uW8fiLLK2pMguDqNS2K2yPe1CVIkJwxiWTjWjTzpfHulw4xt2+I1be2zafNbnu4nIbqF9qUz2tpneXFUwWCUTJpzkFj8QzrXkLy8a2W94ydGpUtOGISQAteSldc59nyzJzPWtf9O0DeZhKPHvKnB1wV9k6e3dXhyNsKgVNTt4/NbT4pAuTkgwT/Fo41365SlSk4YxEWa0Van9R3A9LYP/lbUtqVCyIgrswmxE0R4V2v7ZC83Ow3ItCuLAwW9j4nI0aqXkNwS705nCVgvw8ogNulKk9sqoG96W3ujbfMYkNq6/pF4HCITa9q2zM+F9xt7EkgULRLBtLQuEjkPrzc6epZ9NmhuHJc7kPxtZ7TR1QfpE9a3vwfgS8vHjzmHlnWzwKGFCKIN4BUz2tpztvQlEIrcKYIz07nGJn7cGW14UZemTBgmMwgfiEcbPSvOq1nXsbSo1zqSZGO2sWjjul3YclRNLpujKrJQRK35Qj8kX+iMNvxUl6ZMGfIzCIHH4k3ven5PW7Vp004AkRc/M/YOy8IdEFFq7f8v+ChgfX/Gus3rtAh0hDorGOq5F4K0NpqR+MC2rPN0qcqGoW0Q8ol4tPF0r2V8nI9uu05efuiYU0Gpg8he79FJPgegdkZbu5YTlByjvK4oMLr3QcjeezEPBIFzcmFJdyCGrEEIvhzfWnSW1zoGo6a140kAT/51/AFfgYWfCPCJA01JvuyDRKa1tSt1yPeUyrkjg77ep7HH75AKgklA/l9ntOEvmpRlzZA1CChlxaP7Rne3wZuD/RSZsW7LYwAeayoffUCC1hcgLEOCz9bk0FvwvVE8Zc4hflueBnBEOteRpE18vau54VFN0hwhJ6p502k3mQ4kPgDsM+PR+S85Hduwu3xELHuJiOyf7rU2+N3OpsbbdOhykiG9iiWC/QDruWAoPOS7k7jLub5AdeQGsfh0JuYAWJcP5gCGuEEAQASFELmvLBS52WstQ4GSytoDA9WHPy9ArUj6nx+buCXW1OjJ7sBMGPIG6ccSXBGsjkRLK+e6Uhg5FCmtDJ/q99lvCmRGJtfb5LWd0YYrndalk6H7kD4wVZbPWhUIhRfG/b0/wYqbh20n93QomxQZLQVYmN422X9BkoBc2hltdP08yGwZNjNIPwL4RWRuMFn0Vmll+FSv9eQ2dVZZVfj7lp/vZGwOIEFaF8SjDXlnDiCPDULgMZv250luyzDEeJ9P/hwMhRcVVYTLHRU3BAhWRqYEQ70tliW/SGuL7B6Ija92NtcvclKbm+SEQQRMq0fT7vKRhtM7o/NfSNrWcQCaM08uswoLpTVQHb4Jx4b3zTjOEKE4NG9cIBS+Az68DEFFpnFIbCVwYi6cQZkNOWEQAMplBgQfiTc1/LN8ZGdL/eZYU0OI4L3ZCBDIlcFivBsMzb08mzj5SlFFuDwQitxZIPZ6EflWVsGIZ2xLjo035VZlbibkxIvCstDcEy2xUpYbkPxjPNp49mA/D1aHrwTkJgckrQPshbHO4ruwpq7TgXg5S3ByeAL9uEYg33AiHsFwvKlxvhOxcoGcMAgABEPhJoiEBvs5yaXxaGPKU5/KQpHPW4J7AIzPXhXjtOWOPtq3ut00WTdl1eEvWJBvAnCm0wu5HracF2tpWOlIvBwhZwxSVBEuLyiQx0Vw5AA/vj/W1KD+NvyIOYFA0LpRRL7tnEI+aFN+mcuFdakIVkam0MdZIGaJyEHOReaDsSQv8bJ/lS5yxiD9BKsiV0DwFQKFEKxPJuWWnS31qzKJVVoZPtWycJeTHwaCG0FZJDYeyodvy9JQbYUP9tkQnA/IBCdjE2gncVlntGGZk3FziZwziONUzh0ZsOQWEflP54OzjZAlAB6ONzWscD5++gSqI5+jjZMswRcgOAXAKB15CN4b31V0OV6p+4eO+LnC0DfIR5SGak+2wF+J4HPakhCvAFhBYHWSbN7Z3LhaWy4AqKwrCVjdFRQ5TiBVAE4T4GCtOcn1tuCizqbGZ7XmyRGGjUH6CVRH5gCsE0ixG/lIvg6Rt4V8K0m8BbHe6orWv7LXi46YEyjdx1+S7LWL/ZZdbIu/2CcshoXRQkyi4Dghj4PIYW78Dv2QuDHu77l2OJXoDDuDALs7/fn91k0QmeW1llyHZK9A7kzYct3OlvrNXutxm2FpkH4+2vBzvYhM91pLLmKTt9q2NX84GqOfYW2Qfkqq51X5YM8FeJZAfF7r8RICPSRupyX1udpIwU2MQT5G0aR54/1++wcW+E2tRzHkICTfBnCH+Hvviq24ebvXenIFY5CBqJw7skx8XxOLXxdgitdydELg96T9287o/Be81pKLGIOkoCgUPswPXCjAhSKSVueOnIVYBbHvju0q/v1Qf4+RLcYgaVAyOVzp88lMAU7LphTcA7aDfIqCp9knj3SubtjqtaB8wRgkQ4LTrhxlJ0ecLJAvCXAKHCmOdAaC3SCihDxj2fhzPpTE5CrGIA4RqIwcKYIQLR4rlMkEKna3HdLLbjPIKwSiIlhlJ+1VXS3zX9edd7hgDKKR4uPnHVxQkDiOsCZRMFbAUQLsR8g+IhwNyCikqJUi2AFgsxCbKNgslE02uFlENtkJea9rVf1r7vw2BoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGD7O/wcz9MgE8U3nuQAAA61pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0n77u/JyBpZD0nVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkJz8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0nYWRvYmU6bnM6bWV0YS8nPgo8cmRmOlJERiB4bWxuczpyZGY9J2h0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMnPgoKIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PScnCiAgeG1sbnM6QXR0cmliPSdodHRwOi8vbnMuYXR0cmlidXRpb24uY29tL2Fkcy8xLjAvJz4KICA8QXR0cmliOkFkcz4KICAgPHJkZjpTZXE+CiAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9J1Jlc291cmNlJz4KICAgICA8QXR0cmliOkNyZWF0ZWQ+MjAyMS0wMy0wOTwvQXR0cmliOkNyZWF0ZWQ+CiAgICAgPEF0dHJpYjpFeHRJZD44ZmZiNzg2OC01ZTI1LTQwMGUtODUzMy1kZGM0MTk5NTNiYjU8L0F0dHJpYjpFeHRJZD4KICAgICA8QXR0cmliOkZiSWQ+NTI1MjY1OTE0MTc5NTgwPC9BdHRyaWI6RmJJZD4KICAgICA8QXR0cmliOlRvdWNoVHlwZT4yPC9BdHRyaWI6VG91Y2hUeXBlPgogICAgPC9yZGY6bGk+CiAgIDwvcmRmOlNlcT4KICA8L0F0dHJpYjpBZHM+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnBkZj0naHR0cDovL25zLmFkb2JlLmNvbS9wZGYvMS4zLyc+CiAgPHBkZjpBdXRob3I+U3VwZXIgRmluc2VydiBQcml2YXRlIExpbWl0ZWQ8L3BkZjpBdXRob3I+CiA8L3JkZjpEZXNjcmlwdGlvbj4KCiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0nJwogIHhtbG5zOnhtcD0naHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyc+CiAgPHhtcDpDcmVhdG9yVG9vbD5DYW52YTwveG1wOkNyZWF0b3JUb29sPgogPC9yZGY6RGVzY3JpcHRpb24+CjwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9J3InPz4ci/WZAAAAAElFTkSuQmCC', NULL, '2021-03-28 07:36:03', '2021-03-28 07:36:03'),
(28, 12, 8, 'date', NULL, '08-08-1998', NULL, NULL, '2021-03-28 07:36:03', '2021-03-28 07:36:03'),
(91, 79, 31, 'date', NULL, '10-07-1994', NULL, NULL, '2021-04-06 12:48:29', '2021-04-06 12:48:29'),
(90, 79, 30, 'text', NULL, 'Vikas Kumar', NULL, NULL, '2021-04-06 12:48:29', '2021-04-06 12:48:29'),
(89, 79, 29, 'file', NULL, '606c589d5e7a5.png', '', NULL, '2021-04-06 12:48:29', '2021-04-06 12:48:29'),
(88, 78, 22, 'textarea', NULL, 'Ff', NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(87, 78, 21, 'checkbox', '31,34', NULL, NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(86, 78, 18, 'date', NULL, 'Apr 06 2021', NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(85, 78, 19, 'text', NULL, 'Ss', NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(84, 78, 15, 'radio', '26', NULL, NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(83, 78, 20, 'file', NULL, '606c4a25042dc.jpg', '', NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(82, 78, 11, 'text', NULL, 'Ff', NULL, NULL, '2021-04-06 11:46:45', '2021-04-06 11:46:45'),
(81, 75, 11, 'text', NULL, 'Tt', NULL, NULL, '2021-04-06 11:13:37', '2021-04-06 11:13:37'),
(67, 68, 29, 'file', NULL, '606574b07ba0e.png', '', NULL, '2021-04-01 07:22:24', '2021-04-01 07:22:24'),
(68, 68, 30, 'text', NULL, 'Vikas Kumar', NULL, NULL, '2021-04-01 07:22:24', '2021-04-01 07:22:24'),
(69, 68, 31, 'date', NULL, '10-07-1994', NULL, NULL, '2021-04-01 07:22:24', '2021-04-01 07:22:24'),
(70, 70, 29, 'gg', NULL, '/tmp/phpi4Juir', NULL, NULL, '2021-04-03 15:10:44', '2021-04-03 15:10:44'),
(71, 70, 30, 'text', NULL, 'Vikas Kumar', NULL, NULL, '2021-04-03 15:10:44', '2021-04-03 15:10:44'),
(72, 70, 31, '31/11/2020', NULL, '10-07-1994', NULL, NULL, '2021-04-03 15:10:44', '2021-04-03 15:10:44'),
(80, 74, 11, 'text', NULL, 'Ff', NULL, NULL, '2021-04-06 11:07:40', '2021-04-06 11:07:40'),
(74, 72, 29, 'gg', NULL, '/tmp/phpBT5TRK', NULL, NULL, '2021-04-05 08:04:43', '2021-04-05 08:04:43'),
(75, 72, 30, 'text', NULL, 'Vikas Kumar', NULL, NULL, '2021-04-05 08:04:43', '2021-04-05 08:04:43'),
(76, 72, 31, '31/11/2020', NULL, '10-07-1994', NULL, NULL, '2021-04-05 08:04:43', '2021-04-05 08:04:43'),
(77, 73, 29, 'file', NULL, '606bfd8e71f3c.png', '', NULL, '2021-04-06 06:19:58', '2021-04-06 06:19:58'),
(78, 73, 30, 'text', NULL, 'Vikas Kumar', NULL, NULL, '2021-04-06 06:19:58', '2021-04-06 06:19:58'),
(79, 73, 31, 'date', NULL, '10-07-1994', NULL, NULL, '2021-04-06 06:19:58', '2021-04-06 06:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `transections`
--

CREATE TABLE `transections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `birth` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` text COLLATE utf8mb4_unicode_ci,
  `sure_name` text COLLATE utf8mb4_unicode_ci,
  `id_no` text COLLATE utf8mb4_unicode_ci,
  `street_address` text COLLATE utf8mb4_unicode_ci,
  `postal_address` text COLLATE utf8mb4_unicode_ci,
  `mobile_no` text COLLATE utf8mb4_unicode_ci,
  `tel_no` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `video` text COLLATE utf8mb4_unicode_ci,
  `first_name` text COLLATE utf8mb4_unicode_ci,
  `last_name` text COLLATE utf8mb4_unicode_ci,
  `code` int(11) DEFAULT NULL,
  `documents` text COLLATE utf8mb4_unicode_ci,
  `document_text` text COLLATE utf8mb4_unicode_ci,
  `address_1` text COLLATE utf8mb4_unicode_ci,
  `address_op` text COLLATE utf8mb4_unicode_ci,
  `city` text COLLATE utf8mb4_unicode_ci,
  `zip_code` text COLLATE utf8mb4_unicode_ci,
  `card_number` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `step` text COLLATE utf8mb4_unicode_ci,
  `steps` text COLLATE utf8mb4_unicode_ci,
  `registration_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business` tinyint(1) DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_verified` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transcript` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `email_verified_at`, `phone`, `birth`, `address`, `firstname`, `sure_name`, `id_no`, `street_address`, `postal_address`, `mobile_no`, `tel_no`, `status`, `password`, `image`, `video`, `first_name`, `last_name`, `code`, `documents`, `document_text`, `address_1`, `address_op`, `city`, `zip_code`, `card_number`, `remember_token`, `created_at`, `updated_at`, `step`, `steps`, `registration_id`, `business`, `token`, `payment_verified`, `transcript`, `document_image`) VALUES
(1, 'praveen.patidar10@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-27 01:12:34', '2021-03-27 01:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 00:07:14', '2021-03-28 00:07:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 02:21:11', '2021-03-28 02:21:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 11:45:05', '2021-03-28 11:45:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-29 08:59:40', '2021-03-29 08:59:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-29 13:18:45', '2021-03-29 13:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-30 09:51:53', '2021-03-30 09:51:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 03:13:56', '2021-03-31 03:13:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 06:05:05', '2021-03-31 06:05:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 06:29:02', '2021-03-31 06:29:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 07:45:07', '2021-03-31 07:45:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 08:36:03', '2021-03-31 08:36:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 10:18:59', '2021-03-31 10:18:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$K2jePAsR6hrZwbgmaN45.OoBS.4ZqM8gNuulfEC4.25Ae4PAtuoeS', '60657addf0bf114.png', NULL, 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 10:27:24', '2021-04-02 06:24:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'davis@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$CLca/FH2EoJyUeb1wWKqbevmeUgIWXXhdNxy5XIm139YTINF38Pfu', '606c5ac20c46c15.png', NULL, 'davis', 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-05 02:34:31', '2021-04-06 07:27:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `docs_user_id_foreign` (`user_id`);

--
-- Indexes for table `document_views`
--
ALTER TABLE `document_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_views_user_id_foreign` (`user_id`),
  ADD KEY `document_views_document_id_foreign` (`document_id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_verification`
--
ALTER TABLE `payment_verification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_verification_user_id_foreign` (`user_id`);

--
-- Indexes for table `police_verifications`
--
ALTER TABLE `police_verifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `police_verifications_user_id_foreign` (`user_id`),
  ADD KEY `police_verifications_document_id_foreign` (`document_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionsets`
--
ALTER TABLE `questionsets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signs`
--
ALTER TABLE `signs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `signs_user_id_foreign` (`user_id`),
  ADD KEY `signs_sender_id_foreign` (`sender_id`);

--
-- Indexes for table `starts`
--
ALTER TABLE `starts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_results`
--
ALTER TABLE `survey_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transections`
--
ALTER TABLE `transections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transections_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `docs`
--
ALTER TABLE `docs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_views`
--
ALTER TABLE `document_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_verification`
--
ALTER TABLE `payment_verification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `police_verifications`
--
ALTER TABLE `police_verifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `questionsets`
--
ALTER TABLE `questionsets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `signs`
--
ALTER TABLE `signs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `starts`
--
ALTER TABLE `starts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `survey_results`
--
ALTER TABLE `survey_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `transections`
--
ALTER TABLE `transections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
