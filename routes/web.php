<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('main', 'VerificationController@main')->name('main');
Route::get('savenumber', 'VerificationController@saveNumber')->name('savenumber');
// Route::get('login', function () {
//     return view('welcome'); 
// });

Route::get('login', 'UserController@login')->name('login');
Route::get('start-meeting', 'MeetingController@startMeeting')->name('startmeeting');
Route::get('start', 'StartController@startBtn');

Route::get('savedata', 'StartController@saveData')->name('savedata');
Route::get('transection', 'TransactionController@transection');
//payment verified page
Route::any('payment/verified', 'TransactionController@paymentVerified')->name('paymentVerified');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');



// Frontend routes
// Route::get('index', 'VerificationController@mainPage')->name('index');
// Route::get('verification', 'VerificationController@verification')->name('verification'); // before

Route::get('verification', 'VerificationController@getOtpVerification')->name('verification');
Route::post('verification', 'VerificationController@postOtpVerification')->name('verification');

//get
Route::group(['middleware' => ['auth:web']], function () {
    
     Route::any('create/questions/set', 'QuestionsetController@CreateQuestionsSet');
     Route::post('save/questions/set', 'QuestionsetController@saveQuestionSet');
     Route::get('create-questions/{id}', 'QuestionsetController@CreateQuestionsForSet');
     Route::get('get-answer-template/{ansType}', 'QuestionsetController@getAnswerTemplate');
     Route::post('save/new-questions', 'QuestionsetController@saveQuestions');
     Route::get('delete-question/{QueId}', 'QuestionsetController@RemoveQuestion');
     Route::get('delete-questionset/{setId}', 'QuestionsetController@RemoveQuestionset');
     Route::get('mark-active-questionset/{status}/{setId}', 'QuestionsetController@updateQuestionsetStatus');
     
     
     
     Route::get('surveys', 'SurveysController@AllSurveys');
     Route::get('surveys-responses/{id}', 'SurveysController@surveysResponses');
     Route::get('surveys/export-sheet/{id}', 'SurveysController@exportCsv');
     
     
     
      Route::any('profile', 'ProfileController@index');
      Route::post('profile/upload-image', 'ProfileController@uploadImage');
      Route::post('profile/update-info', 'ProfileController@updateProfileInfo');
      Route::post('profile/change-password', 'ProfileController@changePassword');
      

    // Route::get('verification/{id}', 'VerificationController@verificationFatchData')->name('verificationdata');
   // Route::any('dashboard', 'VerificationController@dashboardMainPage')->name('dashboard');
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('requestpage', 'VerificationController@requestPage')->name('requestpage');
    Route::get('documentslist', 'VerificationController@DocumentsList')->name('documentslist');
    Route::post('documentslist', 'VerificationController@acceptRejectDocumentsList')->name('documentslist');
    Route::get('signpage', 'VerificationController@signPage')->name('signpage'); // send sign request page

    Route::get('showRequestedData', 'VerificationController@showRequestedData')->name('showRequestedData');
    Route::get('founddocumentslist', 'VerificationController@FoundDocumentsList')->name('founddocumentslist');

    //Police verification all routes
    Route::get('police/verification', 'VerificationController@policeVerification')->name('policeverification');
    Route::get('police/verification/{id}', 'VerificationController@policeVerificationUser')->name('policeverificationuser');
    Route::post('police/verification/{id}', 'VerificationController@generatePin')->name('policeverificationuser');
    Route::post('police/generate/pin', 'VerificationController@generatePin')->name('generatepin');
    Route::get('police/generate/pin/view', 'VerificationController@generatePinView')->name('generatepinview');

    //sign document
    route::get('sign/document', 'SignController@signDocument')->name('signDocuments');
    route::post('sign/document', 'SignController@signDocumentUpload')->name('signDocumentsUpload');

    //App user request page
    Route::get('sendrequestpage', 'SignController@sendRequestPage')->name('sendrequestpage');


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
