<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//----Enquiry--------------------------------------------------
// Route::post('storeFirstName', 'EnquiryController@storeFirstName');
// Route::post('storeLastName', 'EnquiryController@storeLastName');

Route::post('storeDoYouLove', 'EnquiryController@storeDoYouLove');
Route::post('storeAddress', 'EnquiryController@storeAddress');
Route::post('storeChooseTwoOption', 'EnquiryController@storeChooseTwoOption');
Route::post('storeYourDob', 'EnquiryController@storeYourDob');
Route::post('storeSocialSecurityNumber', 'EnquiryController@storeSocialSecurityNumber');
Route::post('storePhoto', 'EnquiryController@storePhoto');

//Route::post('storePhoneNumber', 'EnquiryController@storePhoneNumber');



Route::post('register', 'API\UserController@register');
Route::post('user/{id}/verification', 'API\UserController@otpVerification');
//Route::post('login', 'API\UserController@login');


//-----------------------------Questions----------------------------//

Route::get('get/questions-sets', 'API\QuestionsController@getQuestionsSets');
Route::get('questions/get/{setId?}', 'API\QuestionsController@getQuestionSet');
Route::post('questions/save/response', 'API\QuestionsController@saveResponse');


//-----------------------------Questions---------------------------//

Route::group(['middleware' => 'auth:api'], function(){

  Route::post('user/document', 'API\UserController@documents');
  Route::post('user/video', 'API\UserController@upload_video');
  Route::post('user/image', 'API\UserController@upload_image');
  Route::post('user/information', 'API\UserController@personal_info');
  Route::post('user/paymentVerification', 'API\UserController@payment_verification');
  Route::get('user/paymentUrl', 'API\UserController@generatePaymentUrl');

    
  Route::get('user', 'API\UserController@get_user');


  Route::get('documents', 'API\DocsController@get_docs');
  Route::post('docs', 'API\DocsController@docs');

  Route::post('user/registrationid', 'API\UserController@registration_id'); //clear

  Route::get('country', 'API\countryController@country');
  Route::post('user/{id}/phone', 'API\UserController@registerPhone');


  Route::get('user/{id}/video', 'API\UserController@get_video');
  // Route::get('selected/documents/list', 'API\DocsController@selectedDocumentsList');

  // Police Verification Routes
  // Route::post('police/docs', 'PoliceVerificationController@policeDocs');
  // Route::post('police/docs/list', 'PoliceVerificationController@policeDocsList');
  Route::post('police/selected/docs/send', 'PoliceVerificationController@policeSelectedDocsSend');
  Route::post('police/selected/docs/send/update', 'PoliceVerificationController@policeSelectedDocsSendUpdate');

  Route::post('docs/{id}/rescan', 'API\DocsController@rescanDocuments');
  Route::post('user/{id}/documents/{document_id}/request', 'API\UserController@DocumentsViewRequest'); // clear
  Route::post('user/documents/{id}/accept', 'API\UserController@DocumentsViewAccept'); // clear
  Route::post('user/documents/{id}/reject', 'API\UserController@DocumentsViewReject'); // status update: request to reject
  Route::post('user/{id}/documents/{document_id}/favorite','API\UserController@DocumentsFavoriteAccept'); // clear
  Route::post('documents/{document_id}/rejected', 'API\UserController@DocumentsViewRejected'); // Reject data from database as well
  Route::get('documents/view', 'API\UserController@DocumentsView');
  Route::get('documents/request/list', 'API\UserController@DocumentsRequestList'); // clear
  Route::get('documents/accept/list', 'API\UserController@DocumentsAcceptList'); // clear
  Route::get('documents/reject/list', 'API\UserController@DocumentsRejectList'); // clear

  //verify pin API
  Route::post('document/pin', "API\UserController@verifyPin");
  //Payment verified

});
// Transection API
Route::post('user/savepayment', 'TransactionController@savepayment');
