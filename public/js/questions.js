$(function(){
    'use strict'

    $('body').on('click', '.delete-Question', function (e) {
        var Id = $(this).attr('data-id');
        var _this  = $(this);

        $.confirm({
            icon:'fa fa-trash',
            title: 'Confirm!',
            backgroundDismiss: false,
            type:'red',
            content:"Sure You want to Delete Question?",
            buttons: {
                confirm: function () {
                    $.get(base_url+"/delete-question/"+Id,function(result){
                        var st = $.trim(result.status);
                        if(st=="success"){
                            $('#tr-'+Id).remove();
                            toastr.success(result.message);
                        }else{
                            toastr.error(result.message);
                        }
                    },'json')
                },
                cancel: function () {

                }
            }
        });
    });


    $('body').on('click', '.Qset-status', function (e) {
        var Id = $(this).attr('data-id');
        var status = $('#span-status-'+Id).attr('data-status');
        //var str = "hello world";
        var newStatus = (status=="INACTIVE")?'ACTIVE':'INACTIVE';
        var str = newStatus.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var msg = (status=="INACTIVE")?'Active':'Inactive';
        //if(status=="INACTIVE"){
        var _this  = $(this);

        $.confirm({
            icon:'fa fa-trash',
            title: 'Confirm!',
            backgroundDismiss: false,
            type:'red',
            content:"<h6 >Sure You want to "+msg+" this Question Set?</h6>",
            buttons: {
                confirm: function () {
                    $.get(base_url+"/mark-active-questionset/"+newStatus+"/"+Id,function(result){
                        var st = $.trim(result.status);
                        if(st=="success"){
                            toastr.success(result.message);
                            location.reload();
                        }else{
                            toastr.error(result.message);
                        }
                    },'json')
                },
                cancel: function () {

                }
            }
        });
        //   }else{
        //       $.alert("Question set is already Active.");
        //   }
    });


    $('body').on('click', '.delete-QuestionSet', function (e) {
        var Id = $(this).attr('data-id');
        var status = $('#span-status-'+Id).attr('data-status');
        if(status=="INACTIVE"){
            var _this  = $(this);

            $.confirm({
                icon:'fa fa-trash',
                title: 'Confirm!',
                backgroundDismiss: false,
                type:'red',
                content:"<h6 class='text-danger'>Sure You want to Delete Question Set?</h6><p><span style='font-size:12px;'>On your confirmation all questions will be deleted for this set.</span>",
                buttons: {
                    confirm: function () {
                        $.get(base_url+"/delete-questionset/"+Id,function(result){
                            var st = $.trim(result.status);
                            if(st=="success"){
                                $('#tr-'+Id).remove();
                                toastr.success(result.message);
                            }else{
                                toastr.error(result.message);
                            }
                        },'json')
                    },
                    cancel: function () {

                    }
                }
            });
        }else{
            $.alert("You can not delete Active Question set.");
        }
    });

    $(document).on('change', '#ansType', function (e) {
        var val =  $(this).val();
        $('#BoxAddmore').remove();
        if(val!==""){
            $.get(base_url+'/get-answer-template/'+val,function(resp){
                $('#answerTempBox').html(resp);
                if(val=="checkbox" || val=="radio"){
                    $( ".answerTempBoxCardBody" ).after('<div class="card-footer" id="BoxAddmore"><button id="btnAddMoreOption" type="button" class="btn btn-primary " style="float: right;background-color: #9C27B0;border-color: #9C27B0;"><i class="fas fa-plus"></i> Add more options</button></div>');
                }else{
                    $('#BoxAddmore').remove();
                }
            });
        }else{
            $('#answerTempBox').empty("");
            $('#BoxAddmore').remove();
        }
    })

    $('body').on('click', '#btnAddMoreOption', function (e) {
        var inputType = $('#ansType').val();
        var remove  = base_url+'/assets/img/remove-icon.png';
        $('#answerTempBox').append('<div class="col-md-4 mb-2 elembox-'+inputType+'"><div class="input-group  " >'
            +'<div class="input-group-prepend">'
            +'<span class="input-group-text span-count"></span>'
            +'</div>'
            +'<input type="text" class="form-control input-opt inputBox-'+inputType+'" placeholder="Enter Option 1" >'
            +'<div class="input-group-append">'
            +'<span class="input-group-text text-danger btn-trash" style="padding: 5px;cursor: pointer;"><img style="width:20px;" src="'+remove+'"/></span>'
            +'</div>'
            +'</div></div>');
        var _index =2;
        $(".elembox-"+inputType).each(function(){
            $(this).attr('id',"elembox-"+inputType+_index);
            _index++;
        })

        var _index =2;
        $(".span-count").each(function(){
            $(this).text(_index);
            _index++;
        })

        var _index =2;
        $(".inputBox-"+inputType).each(function(){
            $(this).attr('id',inputType+"-"+_index);
            $(this).attr('name',inputType+"-"+_index);
            $(this).attr('placeholder',"Enter Option "+_index);
            _index++;
        })

        var _index =2;
        $(".btn-trash").each(function(){
            $(this).attr('data-id',_index);
            $(this).attr('id',"elem-"+inputType+_index);
            _index++;
        });
        var cnt = $(".elembox-"+inputType).length+1
        $('#counter').val(cnt);

    })

    $('body').on('click', '.btn-trash', function (e) {
        var inputType = $('#ansType').val();
        var n = $(this).attr('data-id');
        $("#elembox-"+inputType+n).remove();
        var _index =2;
        $(".elembox-"+inputType).each(function(){
            $(this).attr('id',"elembox-"+inputType+_index);
            _index++;
        })

        var _index =2;
        $(".span-count").each(function(){
            $(this).text(_index);
            _index++;
        })

        var _index =2;
        $(".inputBox-"+inputType).each(function(){
            $(this).attr('id',inputType+"-"+_index);
            $(this).attr('name',inputType+"-"+_index);
            $(this).attr('placeholder',"Enter Option "+_index);
            _index++;
        })

        var _index =2;
        $(".btn-trash").each(function(){
            $(this).attr('data-id',_index);
            $(this).attr('id',"elem-"+inputType+_index);
            _index++;
        })
        var cnt = $(".elembox-"+inputType).length+1
        $('#counter').val(cnt);
    });




    $('body').on('submit', '#formQuestionSet', function (e) {
        e.preventDefault();

        $('#question').rules("add", {
            required: true,
            messages: {
                required: "This field is required.",
            }
        });

        $('#ansType').rules("add", {
            required: true,
            messages: {
                required: "Required.",
            }
        });



        $('.input-opt').each(function() {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: 'This field is required!'
                }
            });
        });

        if($('#formQuestionSet').valid()){
            var formData = $('#formQuestionSet').serialize();
            $.post(base_url+'/save/new-questions',formData,function(result){
                if($.trim(result.status)=='success'){
                    toastr.success(result.message);
                    $('#formQuestionSet')[0].reset();
                    $('#answerTempBox').empty("");
                    $('#BoxAddmore').remove();
                    $('#ansType').val('').trigger('change');
                }else{
                    toastr.error(result.message);
                }
            },'json')
        }else{
            toastr.error("Please fill all required field");
        }
    })
});
$(document).ready(function() {
    $('#formQuestionSet').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            }else if(element.hasClass('select2') && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            }else{
                error.insertAfter(element);
            }
        }
    });

});
