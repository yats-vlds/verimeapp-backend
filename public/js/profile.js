
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
 $('body').on('change', '#imageUpload', function (e) { 
    $('#fileActionElem').removeClass('fileActionElem-hidden');
    readURL(this);
});
$('body').on('click', '#uploadProfile', function (e) { 
    e.preventDefault();
     var form = $('#avtarUploadForm')[0]; // You need to use standard javascript object here
                  var formData = new FormData(form);
                  $.ajax({
                     url:base_url+'/profile/upload-image',
                     type:"post",
                     data:formData,
                     processData:false,
                     contentType:false,
                     dataType:'json',
                     cache:false,
                     async:false,
                      success: function(data){
                         console.log(data)
                         var st = $.trim(data.status);
                         if(st=="success"){
                              toastr.success(data.message);
                              $('#fileActionElem').addClass('fileActionElem-hidden');
                        }else{
                            toastr.error(data.message);
                        }
                       
                    }
                 });
})
$('body').on('click', '#uploadCancel', function (e) { 
 
    var bg = $('#oldImage').val();
      $('#imagePreview').css('background-image', 'url('+bg+')');
      $('#fileActionElem').addClass('fileActionElem-hidden');
})