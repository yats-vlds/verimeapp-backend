<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiries extends Model
{
    
protected $fillable = [
        'do_you_love','choose_two_option','dob','social_Security_number','photo','address',
    ];


public function SetChooseTwoOptionAttribute($value)
{
	$this->attributes['choose_two_option'] = json_encode($value);
}

public function GetChooseTwoOptionAttribute($value)
{
	return json_decode($value);
}

}
