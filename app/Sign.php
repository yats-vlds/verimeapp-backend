<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
    protected $fillable = [
        'name',
        'documents',
        'user_id',
        'sender_id',
    ];
}
