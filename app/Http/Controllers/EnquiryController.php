<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiries;
use App\User;
use Validator;

class EnquiryController extends Controller
{
    
//---------------------------------------------------------------
     public function storeDoYouLove(Request $request)
     {
     	$validator = Validator::make($request->all(), [ 
     	    'do_you_love' => 'required',  
     	]);
     	
     	if ($validator->fails()) { 
     	    return response()->json(['error'=>$validator->errors()], 401);            
     	}

        $user = Enquiries::create(['do_you_love'=>$request->do_you_love]);
       return response()->json(['id' => $user->id], 201);

     }
//--------------------------------------------------------------
         
//---------------------------------------------------------------
          public function storeChooseTwoOption(Request $request)
          {
          	$validator = Validator::make($request->all(), [ 
          	    'choose_two_option' => 'required',  
          	    'id' => 'required',  
          	]);
          	
          	if ($validator->fails()) { 
          	    return response()->json(['error'=>$validator->errors()], 401);            
          	}

             $user = Enquiries::find($request->id);
             $user->update(['choose_two_option'=>$request->choose_two_option]);
            return response()->json(['id' => $user->id], 201);

          }
 //--------------------------------------------------------------

     public function storeYourDob(Request $request)
     {
     	$validator = Validator::make($request->all(), [ 
     	    'dob' => 'required',  
     	    'id' => 'required',  
     	]);
     	
     	if ($validator->fails()) { 
     	    return response()->json(['error'=>$validator->errors()], 401); 
     	    	}

        $user = Enquiries::find($request->id);
        $user->update(['dob'=>$request->dob]);

       return response()->json(['id' => $user->id], 201);

     }

             
 //---------------------------------------------------------------
      public function storeAddress(Request $request)
      {
      	$validator = Validator::make($request->all(), [ 
      	    'address' => 'required',  
      	    'id' => 'required',  
      	]);
      	
      	if ($validator->fails()) { 
      	    return response()->json(['error'=>$validator->errors()], 401);            
      	}

         $user = Enquiries::find($request->id);
         $user->update(['address'=>$request->address]);
        return response()->json(['id' => $user->id], 201);

      }
  //--------------------------------------------------------------

                      
  //---------------------------------------------------------------
       public function storeSocialSecurityNumber(Request $request)
       {
        $validator = Validator::make($request->all(), [ 
            'social_security_number' => 'required',  
            'id' => 'required',  
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

          $user = Enquiries::find($request->id);
          $user->update(['social_Security_number'=>$request->social_security_number]);
         return response()->json(['id' => $user->id], 201);

       }
    //--------------------------------------------------------------


       public function storePhoto(Request $request)
       {
        $validator = Validator::make($request->all(), [ 
            'photo' => 'required',  
            'id' => 'required',  
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        dd($request->photo);

         //  $user = Enquiries::find($request->id);
         //  $user->update(['social_Security_number'=>$request->social_security_number]);

          return response()->json(['id' => $request->id], 201);



       }

}
