<?php

namespace App\Http\Controllers\api;
// namespace Google\Cloud\Samples\Vision;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Validator;
use App\Models\Questionset;
use App\Models\Questions;
use App\Models\Answers;
use App\Models\Survey;
use App\Models\SurveyResults;
//use App\User;

class QuestionsController extends Controller{
    
     public function getQuestionsSets(Request $request){ 
         $hasQueSetActive = Questionset::where('status', '=', 'ACTIVE')->count();
         if($hasQueSetActive){
            $Qset   = Questionset::select('id','title','businessId')->where('status', 'ACTIVE')->get();
            return response()->json(['status'=>'success','message' => 'Questions sets get successfully','data'=>$Qset],200);
         }else{
            return response()->json(['status'=>'error','message' => 'Questions set not added Yet!','data'=>[]],200);
         }
     }
    
    public function getQuestionSet(Request $request){
        
            // $validator = Validator::make($request->all(), [  
            //     'name'      => 'required', 
            //     'documents' => 'required', 
            // ]);

            // if ($validator->fails()) { 
            //     return response()->json(['error'=>$validator->errors()], 401);            
            // }
            
            //  $user = Auth::user();
            //  $user_id = $user->id;
            if( isset($request->setId) && ($request->setId>0)){
              $hasQueSetActive = Questionset::where('status', '=', 'ACTIVE')->where('id','=',$request->setId)->count();
              if($hasQueSetActive){
                  $Qset   = Questionset::where('status', 'ACTIVE')->where('id','=',$request->setId)->first();
                  $hasQue = Questions::where('status', '=', 'ACTIVE')->where('setId',$request->setId)->count();
                  
                  if($hasQue){
                          $QueArr = [];
                          $Questions = Questions::where('status', '=', 'ACTIVE')->where('setId',$Qset->id)->get();
                          
                          foreach($Questions as $Que){
                              $AnsArr = [];
                              if($Que->ansType=="radio" || $Que->ansType=="checkbox"){
                                 $AnsArr = Answers::select('id as answerId','opt as option')->where('qid', '=', $Que->id)->get(); 
                              }
                              
                              $QueArr[] = ['Qid'=>$Que->id,'question'=>$Que->question,'type'=>$Que->ansType,'options'=>$AnsArr];
                          }
                          $userIcon =User::where('id', $Qset->businessId)->value('image');
                          $businessIcon=asset("assets/img/profile/".$userIcon);
                          $Data=['businessIcon' => $businessIcon,'SetTitle'=>$Qset->title,'setId'=>$Qset->id,'questionsList'=>$QueArr];
                         
                        return response()->json(['status'=>'success','message' => 'Questions set Get successfully','data'=>$Data],200);
                  }else{
                    return response()->json(['status'=>'error','message' => 'Questions not added yet for set!','data'=>[]],200);
                  }
              }else{
                  return response()->json(['status'=>'error','message' => "Selected set is Inactive" ,'data'=>[]],200); 
              }
            }else{
                  return response()->json(['status'=>'error','message' => 'Invalid set information passed.','data'=>[]],200); 
            }
    }
    
    public function saveResponse(Request $request){
        $userId = 1;
         if($userId){
            if($request->setId){
                if($request->questionSet){
                          $testData = $request->questionSet;
                          $surveyId = Survey::insertGetId(['userId' =>$userId,'formId'=>$request->setId]);
                          
                          foreach ($testData as $value) {
                                
                                $arr = [ 'surveyId'=>$surveyId, 'ansType'=>$value['ansType'],'qId'=>$value['Qid']];
                                if($value['ansType']=='radio' || $value['ansType'] == 'checkbox'){
                                    $arr['answerId']=$value['answerId'];
                                    $arr['answer']=NULL;
                                }else if($value['ansType']=='file'){
                                       $FILEDATA = $value['answer'];
                                       $extension = $FILEDATA->extension();
                                       $fileName = uniqid().'.'.$extension;
                                       $FILEDATA->move('public/assets/files/', $fileName);
                                    
                                    $arr['answerId']=NULL;
                                    $arr['answer']=$fileName;
                                    $arr['ansText']="";//$value['answer'];
                                }else{
                                     $arr['answerId']=NULL;
                                     $arr['answer']=$value['answer'];
                                }
                              SurveyResults::insertGetId($arr);
                          }
                          return response()->json(['status'=>'success','message'=>'Form submitted successfully','data'=>['survey_id'=>$surveyId]]);
                  }else{
                    return response()->json(['status'=>'error','message'=>'Question Set is missing']);
                 }
              }else{
                  return response()->json(['status'=>'error','message'=>'Please Provide Questions Set identity ']);
              }
         }else{
               return response()->json(['status'=>'error','message'=>'unknown user access']);
          }
    }
    
    //public function getQuestionSet(Request $request){
        
    //         // $validator = Validator::make($request->all(), [  
    //         //     'name'      => 'required', 
    //         //     'documents' => 'required', 
    //         // ]);

    //         // if ($validator->fails()) { 
    //         //     return response()->json(['error'=>$validator->errors()], 401);            
    //         // }
            
    //         //  $user = Auth::user();
    //         //  $user_id = $user->id;
    //         if( isset($request->businessId) && ($request->businessId>0)){
    //           $hasQueSetActive = Questionset::where('status', '=', 'ACTIVE')->where('businessId','=',$request->businessId)->count();
    //           if($hasQueSetActive){
    //               $Qset   = Questionset::where('status', 'ACTIVE')->where('businessId','=',$request->businessId)->first();
    //               $hasQue = Questions::where('status', '=', 'ACTIVE')->where('setId',$Qset->id)->count();
                  
    //               if($hasQue){
    //                       $QueArr = [];
    //                       $Questions = Questions::where('status', '=', 'ACTIVE')->where('setId',$Qset->id)->get();
                          
    //                       foreach($Questions as $Que){
    //                           $AnsArr = [];
    //                           if($Que->ansType=="radio" || $Que->ansType=="checkbox"){
    //                              $AnsArr = Answers::select('id as answerId','opt as option')->where('qid', '=', $Que->id)->get(); 
    //                           }
                              
    //                           $QueArr[] = ['Qid'=>$Que->id,'question'=>$Que->question,'type'=>$Que->ansType,'options'=>$AnsArr];
    //                       }
    //                       $userIcon =User::where('id', $request->businessId)->value('image');
    //                       $businessIcon=asset("assets/img/profile/".$userIcon);
    //                       $Data=['businessIcon' => $businessIcon,'SetTitle'=>$Qset->title,'setId'=>$Qset->id,'questionsList'=>$QueArr];
                         
    //                     return response()->json(['status'=>'success','message' => 'Questions set Get successfully','data'=>$Data],200);
    //               }else{
    //                     return response()->json(['status'=>'error','message' => 'Questions set not added Yet!','data'=>[]],200);
    //               }
    //           }else{
    //               return response()->json(['status'=>'error','message' => 'Active Question set not found.','data'=>[]],200); 
    //           }
    //         }else{
    //               return response()->json(['status'=>'error','message' => 'Business identity is required','data'=>[]],200); 
    //         }
    // }
    
    
    //public function saveResponse(Request $request){
    //     $userId = 1;
    //      if($userId){
            
    //         if($request->setId){
    //             if($request->questionSet){
    //                       $testData = $request->questionSet;
    //                       $surveyId = Survey::insertGetId(['userId' =>$userId,'formId'=>$request->setId]);
                          
    //                       foreach ($testData as $value) {
                                
    //                             $arr = [ 'surveyId'=>$surveyId, 'ansType'=>$value['ansType'],'qId'=>$value['Qid']];
    //                             if($value['ansType']=='radio' || $value['ansType'] == 'checkbox'){
    //                                 $arr['answerId']=$value['answerId'];
    //                                 $arr['answer']=NULL;
    //                             }else if($value['ansType']=='file'){
    //                                 $folderPath = getcwd()."/public/assets/files/";
    //                                 $img = $value['answer'];
    //                                 $image_parts = explode(";base64,", $img);
    //                                 $image_type_aux = explode("image/", $image_parts[0]);
    //                                 $image_type = $image_type_aux[1];
    //                                 $image_base64 = base64_decode($image_parts[1]);
    //                                 $fileName = uniqid() . '.'.$image_type;
    //                                 $file = $folderPath . $fileName;
    //                                 file_put_contents($file, $image_base64);
                                    
                                 
                                    
    //                                 $arr['answerId']=NULL;
    //                                 $arr['answer']=$fileName;
    //                                 $arr['ansText']=$value['answer'];
    //                             }else{
    //                                  $arr['answerId']=NULL;
    //                                  $arr['answer']=$value['answer'];
    //                             }
    //                           SurveyResults::insertGetId($arr);
    //                       }
    //                       return response()->json(['status'=>'success','message'=>'Form submitted successfully','data'=>['survey_id'=>$surveyId]]);
    //               }else{
    //                 return response()->json(['status'=>'error','message'=>'Question Set is missing']);
    //              }
    //           }else{
    //               return response()->json(['status'=>'error','message'=>'Please Provide Questions Set identity ']);
    //           }
    //      }else{
    //           return response()->json(['status'=>'error','message'=>'unknown user access']);
    //       }
    // }
    
    
   
    
    

    
}

