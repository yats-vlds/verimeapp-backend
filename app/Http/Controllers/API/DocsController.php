<?php

namespace App\Http\Controllers\api;
// namespace Google\Cloud\Samples\Vision;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Validator;
use App\Models\DocsModel;
use App\Models\DocumentViewsModel;
use GoogleCloudVision\GoogleCloudVision;
use GoogleCloudVision\Request\AnnotateImageRequest;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class DocsController extends Controller
{
    public function docs(Request $request){
        
        if ($request->input('type') == 'document') {
            $validator = Validator::make($request->all(), [  
                'name'      => 'required', 
                'documents' => 'required', 
            ]);

            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            if ($request->hasFile('documents')) {
                $documents_image = $request->file('documents');
                $name = time().'.'.$documents_image->getClientOriginalExtension(); 
                $destinationPath = public_path('documents');
                
                $documents_image->move($destinationPath, $name);

                $user_id = Auth::user()->id;
                $docs = new DocsModel;

                $docs->user_id = $user_id;
                $docs->text = $request->input('name');
                $docs->documents_image = $name;
                $docs->save();
                return response()->json(['message' => 'insert successfull'], 201);
            }    
        } elseif ($request->input('type') == 'police') {
            $validator = Validator::make($request->all(), [  
                'name'      => 'required', 
                'police_confirmation' => 'required', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            if ($request->hasFile('police_confirmation')) {
                $police_confirmation_documents = $request->file('police_confirmation');
                $name = time().'.'.$police_confirmation_documents->getClientOriginalExtension(); 
                $destinationPath = public_path('documents');
                
                $police_confirmation_documents->move($destinationPath, $name);
                
                $user_id = Auth::user()->id;
                $docs = new DocsModel;

                $docs->user_id = $user_id;
                $docs->text = $request->input('name');
                $docs->police_confirmation_image = $name;
                $docs->save();
                return response()->json(['message' => 'insert successfull'], 201);
            }    
        } else{
            return response()->json(['message' => 'type not valide'], 404);
        }        
    }
    
    public function get_docs()
    {
        $user = Auth::user();
        $user_id = $user->id;

        $documents = DocsModel::where('user_id', '=', $user_id)->get();

        $documentsArray = [];
        $documentsArray[] = [
            'id' => -1,
            'name' => "ID",
            'document' =>  env('APP_URL') . '/documents/' . $user->documents,
        ];
        if ($documents) {
            foreach ($documents as $value) {
                $documents_image = env('APP_URL') . '/documents/' . $value->documents_image;
                $Array = [
                    'id' => $value->id,
                    'name' => $value->text,
                    'document' => $documents_image,
                ];
                array_push($documentsArray, $Array);
            }
        }
        return response()->json($documentsArray, 200);
    }
    public function rescanDocuments(Request $request, $id)
    {
        $user_id = Auth::user()->id;

        $rescan_documents = DocsModel::where('id', $id)->where('user_id', $user_id)->first();
        if ($rescan_documents !== null) {
            $image_path = env('APP_URL').'/documents/'.$rescan_documents->documents_image;
            
            @unlink($image_path);
            
            if ($request->hasFile('documents')) {
                $documents = $request->file('documents');
                $name = time().'.'.$documents->getClientOriginalExtension(); 
                $destinationPath = public_path('documents');
                
                $documents->move($destinationPath, $name);
                $rescan_documents->text = $request->input('name');
                $rescan_documents->documents_image = $name;
                $rescan_documents->save();
                return response()->json(['message' => 'rescan successfull'], 200);
            }
        } else {
            return response()->json(['message' => 'id not found'], 404);
        }
    }

    // public function selectedDocumentsList(Request $request)
    // {

        //     $validator = Validator::make($request->all(), [  
        //         'phone'      => 'required', 
        //         // 'documents' => 'required', 
        //     ]);

        //     if ($validator->fails()) { 
        //         return response()->json(['error'=>$validator->errors()], 401);            
        //     }

        // $appUser = User::where('phone', $request->phone)->where('steps', '=', 5)->where('business', '=', NULL)->first();

        // // dd($appUser);
        // $acceptedDocumentList = DocumentViewsModel::where('user_id', $appUser->id)->where('sender_id', Auth::user()->id)->where('status', '=', 'accepted')->with('docsData')->get();

        // dd($acceptedDocumentList);
    
// }
}

