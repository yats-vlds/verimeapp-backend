<?php

namespace App\Http\Controllers\API;
// namespace Google\Cloud\Samples\Vision;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Transection;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Validator;
use App\Models\VerificationModel;
use App\Models\DocumentsModel;
use App\Models\PhoneModel;
use App\Models\InfoModel;
use App\Models\PaymentModel;
use App\Models\VideoModel;
use App\Models\ImageModel;
use App\Models\PostalModel;
use App\Models\DocumentViewsModel;
use App\Models\DocsModel;
use App\PoliceVerification;
use GuzzleHttp\Psr7\Response;
use phpDocumentor\Reflection\Types\Null_;
use GoogleCloudVision\GoogleCloudVision;
use GoogleCloudVision\Request\AnnotateImageRequest;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;
use Google\Cloud\Speech\V1\StreamingRecognitionConfig;
use Aws\Rekognition\RekognitionClient;
use Google_Client; 
use Google_Service_Drive;

class UserController extends Controller
{
    public $successStatus = 200;
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'phone' => 'required',  
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $phone = PhoneModel::where('phone', $request->input('phone'))->first();

        $fake_otp = env("FAKE_OTP", false);

        $code = "";
        if ($fake_otp) {
            $code = "1234";
        } else {
            $code = rand(1111,9999);
            \Nexmo::message()->send([
                'to'   => $request->input('phone'),
                'from' => '16105552344',
                'text' => 'Verification code: '.$code,
            ]);
        }

        if ($phone == null) {

            $input = $request->all(); 
            $input ['code'] = $code; 
            $user = User::create($input);
            
            return response()->json(['id' => $user->id], 201);
        } else {
            $phone['code'] = $code;
            $phone->save();
            
            return response()->json(['id' => $phone['id'], 'status' => 'user_exist'], 409);
        } 
    }

    public function otpVerification(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [ 
            'code' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $verification = VerificationModel::find($id);
        if (is_null($verification)) {
            return response()->json("Record not found!", 404);
        }

        if ($verification->code == $request->input('code')) {
            
            $verification->code = null;

            if ($verification->steps == null) {
                $verification->steps = '1';
            }
            $verification->save();

            $user = User::find($id);

            $data['token'] =  $user->createToken('MyApp')->accessToken;
            $data['steps'] =  $verification->steps;
            // $success['name'] =  $user->name;
            return response()->json($data, $this->successStatus);
            
        } else {
            return response()->json("Code not match", 401);
        }
    }

    public function documents(Request $request)
    {
        $user = Auth::user();

        if ($user != null) {
            $validator = Validator::make($request->all(), [ 
                'documents' => 'required', 
                'image' => 'required', 
                ]);
            // var_dump($_FILES);exit;
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 400);            
            }
            $user = DocumentsModel::find($user->id);
            
            if($request->file('documents') && $request->hasFile('documents')){
                    
                //convert image to base64
                $image = base64_encode(file_get_contents($request->file('image')));
                
                //prepare request
                $requests_documents = new AnnotateImageRequest();
                $requests_documents->setImage($image);
                $requests_documents->setFeature("DOCUMENT_TEXT_DETECTION");
                $gcvRequest = new GoogleCloudVision([$requests_documents], env('GOOGLE_CLOUD_KEY'));
                // send annotation request
                $response = $gcvRequest->annotate();
                // error_log(print_r($response, true));exit;
                if (isset($response->responses[0]->textAnnotations[0]->description)) {
                    $text_documents = $response->responses[0]->textAnnotations[0]->description;
                } else {
                    return response()->json(["message" => "text not found!"], 400);
                }
                // $text_documents = "fsdfdsf";
                // echo json_encode(["description" => $response->responses[0]->textAnnotations[0]->description]);

                
                $documents = $request->file('documents');
                $doc_name = time().'.'.$documents->getClientOriginalExtension(); 
                $destinationPath = public_path('documents');
                $documents->move($destinationPath, $doc_name);

                $image = $request->file('image');
                $img_name = time().'.'.$image->getClientOriginalExtension(); 
                $destinationPath = public_path('images');
                $image->move($destinationPath, $img_name);

                $user->documents = $doc_name;
                $user->document_image = $img_name;
                $user->steps = '2';
                $user->document_text = $text_documents;
                $user->save();

                return response()->json(['steps' => '2'], 200);
            }
        } else {
            return response()->json(['message' => "not found"], 404);
        }   
    }

    public function registration_id(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'registration_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()->first()
            ]);
        }

        $reg_id = User::where('id', Auth::user()->id)->first();
        $reg_id->registration_id = $request->registration_id;
        $reg_id->save();

        return response()->json([
            'status' => 200,
            'data' => $reg_id,
            'message' => 'Registration id saved successfully!',
        ]);

    }

    public function personal_info(Request $request)
    {
        $user = Auth::user();
        if (is_null($user)) {
            return response()->json("Record not found!", 404);
        }
        $info = InfoModel::find($user->id);
        $input   = $request->json()->all();
        \error_log(print_r($input, true));
        $info->firstname = $input['firstname'];
        $info->sure_name = $input['surname'];
        $info->id_no = '1234';
        $birth = $input['birth'];
        \Carbon\Carbon::createFromFormat('d/m/Y', $birth)->timestamp;
        $info->street_address = $input['street_address'];
        $info->postal_address = $input['postal_address'];
        $info->mobile_no = $input['mobile_no'];
        $info->tel_no = $input['tel_no'];
        $info->email = $input['email'];
        $info->steps = '5';
        $info->save();

        return response()->json([
            'data' => $info,
            'steps' => '5',
        ], 200);
    }

    public function payment_verification(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;
        
        $input = $request->json()->all();
        if (isset($input['type']) && $input['type'] == 'card') {
            $by_card = PostalModel::find($id);
            if ($by_card) {
                $by_card->first_name   = $input['first_name'];
                $by_card->last_name    = $input['last_name'];
                $by_card->card_number  = $input['card_number'];
                $by_card->steps        = '6';
                $by_card->save();
                return response()->json(['success' => 'update'], 200);
            }
        } elseif (isset($input['type']) && $input['type'] == 'postal') {
            $by_postal_code = PostalModel::find($id);
            if ($by_postal_code) {
                $by_postal_code->first_name    = $input['firstname'];
                $by_postal_code->last_name     = $input['surname'];
                $by_postal_code->address_1     = $input['address1'];
                $by_postal_code->address_op    = $input['addressOptional'];
                $by_postal_code->city          = $input['city'];
                $by_postal_code->zip_code      = $input['zipCode'];
                $by_postal_code->email         = $input['email'];
                $by_postal_code->steps         = '6';
                $by_postal_code->save();
                $user->steps = '6';
                return response()->json(['steps' => '6'], 200);
            } 
        } else {
            return response()->json(['failed' => 'type not defined'], 400);
        }
        return response()->json(['error' => 'Invalid type'], 400);
    }

    public function upload_video(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'video' => 'required',
        ]);

        if ($request->hasFile('video')) {
            putenv('GOOGLE_APPLICATION_CREDENTIALS=../auth.json');
            $video = $request->file('video');

            $audio_file = './temp/file_' . time() . rand(100,999) . ".mp3";

            // extract mp3 file
            \shell_exec('ffmpeg -i ' . $video->path() . " " . $audio_file);

            # get contents of a file into a string
            $content = file_get_contents($audio_file);

            # set string as audio content
            $audio = (new RecognitionAudio())
                ->setContent($content);

            # The audio file's encoding, sample rate and language
            $config = new RecognitionConfig([
                'encoding' => AudioEncoding::ENCODING_UNSPECIFIED,
                'sample_rate_hertz' => 32000,
                'language_code' => 'en-US'
            ]);

            # Instantiates a client
            $client = new SpeechClient();

            # Detects speech in the audio file
            $response = $client->recognize($config, $audio);
            # Print most likely transcription
            $transcript = "";
            $confidence = 0;
            foreach ($response->getResults() as $result) {
                $alternatives = $result->getAlternatives();
                $mostLikely = $alternatives[0];
                $transcript = $mostLikely->getTranscript();
                $confidence = $mostLikely->getConfidence();
                // printf('Transcript: %s' . PHP_EOL, $transcript);
                // printf('Confidence: %s' . PHP_EOL, $confidence);

            }
            $client->close();

            unlink($audio_file);

            if (strlen($transcript) <= 0) {
                return response()->json(['error' => 'Not enough words found in video.'], 400);
            }

            $txt_array_newlines = explode(PHP_EOL, $user->document_text);
            // error_log(print_r($txt_array_newlines, true)); exit;
            $txt_array = [];
            foreach($txt_array_newlines as $value) {
                $txt_array = array_merge($txt_array, explode(' ', $value));
            }
            // $txt_array = explode(' ', $user->document_text);
            foreach($txt_array as $key => $value)
            {
                $txt_array[$key] = strtolower($value);
            }
            
            
            
            $transcript_array = explode(' ', $transcript);
            foreach($transcript_array as $key => $value)
            {
                $transcript_array[$key] = strtolower($value);
            }
            
            $common_array = array_intersect($txt_array, $transcript_array);

            // $d = implode(' ', $common_array);
            if (count($common_array) < 1) {
                return response()->json(['error'=> "Could not find \"" . $transcript . "\" in text of ID Document."], 400);
            }

            $name = time().'.'.$video->getClientOriginalExtension(); 
            $destinationPath = public_path('videos');
            
            $video->move($destinationPath, $name);
        
            $user->video = $name;
            $user->transcript = $transcript;
            $user->steps = '3';
            $user->save();
            return response()->json(['steps'=> '3'], 200);
        }
    }
    public function upload_image(Request $request) {
        
        $user = Auth::user();

        if (is_null($user)) {
            return response()->json("Record not found!", 404);
        }
        $this->validate($request, [
            'img' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        if ($request->hasFile('img')) {

            $image_from_req = $request->file('img');

            $photo = public_path('images') . "/" . $user->document_image;
            $image = file_get_contents($photo);

            
            $photo2 = $image_from_req->path();
            $image2 = file_get_contents($photo2);


            $options = [
                'region'            => env('AWS_DEFAULT_REGION'),
                'version'           => 'latest',
                'credentials' => [
                    'key'    => env('AWS_ACCESS_KEY_ID'),
                    'secret' => env('AWS_SECRET_ACCESS_KEY')
                ]
            ];
        
            $rekognition = new RekognitionClient($options);
            
            try {
                $result = $rekognition->compareFaces([
                    'SourceImage' => array(
                        'Bytes' => $image,
                    ),
                    'TargetImage' => array(
                        'Bytes' => $image2,
                    ),
                ]);
                
            } catch (\Throwable $th) {
                return response()->json(['error'=> 'Image do not match with ID Document'], 400);
            }
            
            $matched = count($result->get("FaceMatches")) > 0;
            
            if (!$matched)
                return response()->json(['error'=> 'Image do not match with ID Document'], 400);
                

            $name = time().'.'.$image_from_req->getClientOriginalExtension(); 
            $destinationPath = public_path('images');
            $image_from_req->move($destinationPath, $name);
            
            $user->image = $name;
            $user->steps = '4';
            $user->save();

            return response()->json(['steps'=> '4'], 200);
        }
    }

    public function generatePaymentUrl(Request $request) {

        $user = Auth::user();

        $transection = new Transection();
        $transection->reference_no = md5(time().mt_rand(1,1000000));
        $transection->user_id = $user->id;
        $transection->status = 'pending';

        $transection->save();

        return response()->json([
            'url' => env('APP_URL') . "/transection?id=" . $transection->reference_no,
        ]);
    }

    public function get_user(Request $request)
    {
        $user = Auth::user();

        if (!$user->id) {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
        if ($user->firstname == null) {
            $first_name = "";
        } else {
            $first_name = $user->firstname;
        }
        if ($user->sure_name == null) {
            $sure_name = "";
        } else {
            $sure_name = $user->sure_name;
        }
        if ($user->email == null) {
            $email = "";
        } else {
            $email = $user->email;
        }
        if ($user->phone == null) {
            $phone = "";
        } else {
            $phone = $user->phone;
        }
        if ($user->birth == null) {
            $birth = "";
        } else {
            $birth = $user->birth;
        }
        if ($user->address == null) {
            $address = "";
        } else {
            $address = $user->address;
        }
        if ($user->street_address == null) {
            $street_address = "";
        } else {
            $street_address = $user->street_address;
        }
        if ($user->postal_adress == null) {
            $postal_adress = "";
        } else {
            $postal_adress = $user->postal_adress;
        }
        if ($user->mobile == null) {
            $mobile = "";
        } else {
            $mobile = $user->mobile;
        }
        if ($user->tel_no == null) {
            $tel_no = "";
        } else {
            $tel_no = $user->tel_no;
        }
        if ($user->image == null) {
            $image = "";
        } else {
            $image = env('APP_URL')."/images/".$user->image;
        }
        if ($user->video == null) {
            $video = "";
        } else {
            $video = env('APP_URL')."/videos/".$user->video;
        }
        if ($user->documents == null) {
            $documents = "";
        } else {
            $documents = env('APP_URL')."/documents/".$user->documents;
        }
        $user_data = [
            'id' => $user->id,
            'first_name' => $first_name,
            'sure_name' => $sure_name,
            'email' => $email,
            'phone' => $phone,
            'birth' => $birth,
            'address' => $address,
            'street_address' => $street_address,
            'postal_adress' => $postal_adress,
            'mobile' => $mobile,
            'tel_no' => $tel_no,
            'image' => $image,
            'video' => $video,
            'documents' => $documents,
            'steps' => $user->steps,
        ];
        return response()->json($user_data, 200);
    }

    // Request the document now
    public function DocumentsViewRequest($id, $document_id)
    {
        $document_exist = DocsModel::where('id', $document_id)->where('user_id', $id)->exists();
        if ($document_exist) {
            $input['user_id'] = $id;
            $input['sender_id'] = Auth::user()->id;
            $input['document_id'] = $document_id;
            $input['status'] = 'request';

            DocumentViewsModel::create($input);
            return response()->json(['message' => 'request send'], 201);
        } else {
            return response()->json(['message' => 'document not found'], 404);
        }
    }

    // Accept the document from request
    public function DocumentsViewAccept($id)
    {
        $document_get = DocumentViewsModel::where('user_id', Auth::user()->id)->where('id', $id)->first();
        if ($document_get) {
            $document_get['status'] = 'accepted';
            $document_get['star'] = 0;
            $document_get->save();
            return response()->json(['message' => 'request accepted'], 200);
        } else {
            return response()->json(['message' => 'request not found'], 404);
        }
    }

    public function DocumentsViewReject($id)
    {
        {
            $document_get = DocumentViewsModel::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if ($document_get) {
                $document_get['status'] = 'rejected';
                $document_get->save();
                return response()->json(['message' => 'Document rejected successfully!'], 200);
            } else {
                return response()->json(['message' => 'request not found'], 404);
            }
        }
    }

    // Make your document favorite
    public function DocumentsFavoriteAccept($id, $document_id)
    {
        $document_get = DocumentViewsModel::where('user_id', Auth::user()->id)->where('sender_id', $id)->where('document_id', $document_id)->first();
        if ($document_get) {
            // dd($document_get);
            $document_get['star'] = 1;
            $document_get->save();
            return response()->json(['message' => 'Favorite this document successfully!'], 200);
        } else {
            return response()->json(['message' => 'Favorite star not found'], 404);
        }
    }

    public function DocumentsView()
    {
        $document_get = DB::table('docs')
        ->join('document_views', 'document_views.document_id', 'docs.id')
        ->select('docs.documents_image', 'docs.id', 'document_views.user_id')
        ->where('document_views.status', 'accepted')
        ->where('document_views.sender_id', Auth::user()->id)
        ->get();

        $documentArray = [];
        $documentArray['document_list'] = [];
        foreach ($document_get as $value) {

            if ($value->documents_image == null) {
                $document = '';
            } else {
                $document = env('APP_URL').'/documents/'.$value->documents_image;
            }
            $document_push = [
                'id' => $value->id,
                'user_id' => $value->user_id,
                'documents' => $document,
            ];
            array_push($documentArray['document_list'], $document_push);   
        }
        return response()->json($documentArray, 200);
    }
    
    public function DocumentsRequestList()
    {
        $DocumentsRequestList = DB::table('docs')
        ->join('document_views', 'document_views.document_id', 'docs.id')
        ->select('docs.documents_image', 'docs.text', 'document_views.id', 'document_views.sender_id', 'document_views.star')
        ->where('document_views.status', 'request')
        ->where('document_views.user_id', Auth::user()->id)
        ->get();
// dd($DocumentsRequestList);
        $documentArray = [];
        foreach ($DocumentsRequestList as $value) {

            if ($value->documents_image == null) {
                $document = '';
            } else {
                $document = env('APP_URL').'/documents/'.$value->documents_image;
            }
            $sender = User::where('id', $value->sender_id)->first();
            $document_push = [
                'id' => $value->id,
                'doc_name' => $value->text,
                'phone' => $sender->phone,
                'user_id' => $value->sender_id,
                'documents' => $document,
            ];
            array_push($documentArray, $document_push);
        }   
        return response()->json($documentArray, 200); 
    }

    public function DocumentsAcceptList()
    {
        $DocumentsRequestList = DB::table('docs')
        ->join('document_views', 'document_views.document_id', 'docs.id')
        ->select('docs.documents_image', 'docs.id', 'document_views.sender_id', 'document_views.star')
        ->where('document_views.status', 'accepted')
        ->where('document_views.user_id', Auth::user()->id)
        ->get();

        $documentArray = [];
        $documentArray['requested_list'] = [];
        foreach ($DocumentsRequestList as $value) {

            if ($value->documents_image == null) {
                $document = '';
            } else {
                $document = env('APP_URL').'/documents/'.$value->documents_image;
            }
            $document_push = [
                'id' => $value->id,
                'user_id' => $value->sender_id,
                'star' => $value->star,
                'documents' => $document,
            ];
            array_push($documentArray['requested_list'], $document_push);
        }   
        return response()->json($documentArray, 200); 
    }

    public function DocumentsRejectList()
    {
        {
            $DocumentsRequestList = DB::table('docs')
            ->join('document_views', 'document_views.document_id', 'docs.id')
            ->select('docs.documents_image', 'docs.id', 'document_views.sender_id', 'document_views.star')
            ->where('document_views.status', 'rejected')
            ->where('document_views.user_id', Auth::user()->id)
            ->get();
    
            $documentArray = [];
            $documentArray['requested_list'] = [];
            foreach ($DocumentsRequestList as $value) {
    
                if ($value->documents_image == null) {
                    $document = '';
                } else {
                    $document = env('APP_URL').'/documents/'.$value->documents_image;
                }
                $document_push = [
                    'id' => $value->id,
                    'user_id' => $value->sender_id,
                    'documents' => $document,
                ];
                array_push($documentArray['requested_list'], $document_push);
            }   
            return response()->json($documentArray, 200); 
        }
    }

    public function DocumentsViewRejected($document_id)
    {
        $document_views = DB::delete('delete from document_views where id = ?',[$document_id]);
        
        if ($document_views) {
            return response()->json(['message' => 'rejected'], 200);
        } else {
            return response()->json(['message' => 'not found'], 404);
        }
    }

    public function verifyPin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()->first()
            ]);
        }

        $verify_pin = PoliceVerification::where('user_id', Auth::user()->id)->where('status', '=', 'verification requests')->where('pin', $request->pin)->get();
        $match_pin = $verify_pin->all();
        // dd($match_pin);
        // $match_pin = PoliceVerification::where('user_id', Auth::user()->id)->where('status', '=', 'verification requests')->get();
        // $match = $match_pin->all();

        // dd($match);

        // $data = [];
        // foreach ($match as $value) {
        //         $data[] = $value->pin;
        // }
        // if($match_pin == $match){
        //     echo "success";
        // }else{
        //     echo "error";
        // }

        // dd($match_pin);

        // dd($verify_pin);
        if($match_pin == NULL){

            return response()->json([
                // 'data' => $verify_pin,
                'status' => 400,
                'message' => "Pin not matched. Please try again!",
            ]);
               
        }
        return response()->json([
            'data' => $verify_pin,
            'status' => 200,
            'message' => "Pin matched successfully!",
        ]);

    }

    public function paymentVerified()
    {

        $payment_verified = User::where('id', auth::user()->id)->first();
        // dd($payment_verified);
        //  $abc = $payment_verified->all();
        //  dD($abc);

        $payment_verified->steps = '6';
        $payment_verified->payment_verified = "Payment Verified";
        
        // dd($abc);
        $payment_verified->save();

        return response()->json([
            // 'data' => $verify_pin,
            'status' => 200,
            'message' => "Payment verified successfully!",
        ]);
    }
}
    