<?php

namespace App\Http\Controllers;

use App\Sign;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignController extends Controller
{
    public function signDocument(Request $request)
    {
        // dd($request->phone);
        $user = User::where('phone', $request->phone)->get();
        // $abc = $user->all();
        

        if(count($user)>0){
            $user = $user[0];
        }
        else{
            return redirect('frontend.sendrequestpage');
        }

        // $data = [];
        // foreach ($user->phone as $value) {
        //     $data[] = $value;
        // }
        // dd($user->phone);
        
        // if(isset($user)){

            return view('frontend.signdocument', compact('user'));
        // }
        // return redirect('frontend.sendrequestpage');
        // dd($user);
    }

    public function signDocumentUpload(Request $request)
    {
        $user_id = $request->all();
        // dd($user_id);
                // $user = Auth::user();
                $user_documents = new Sign;
                $user = User::where('phone', $request->phone)->get();
                // dd($user->all());
                if (is_null($user_documents)) {
                    return response()->json("Record not found!", 404);
                        }

            if ($request->hasFile('file')) {
                $documents = $request->file('file');
                $name = time().'.'.$documents->getClientOriginalExtension(); 
                $destinationPath = public_path('documents');
                $documents->move($destinationPath, $name);
                $user_documents->documents = $name;
                $user_documents->name = $request->name;
                $user_documents->user_id = $user->user_id;
                $user_documents->sender_id = '1';
                $user_documents->save();

              return redirect('sign/document')->with('message', 'Document uploaded successfully!');

            } 
    }

    public function sendRequestPage()
    {
        return view('frontend.sendrequestpage');
    }
}
