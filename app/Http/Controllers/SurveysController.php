<?php

namespace App\Http\Controllers;

use App\Transection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use Validator;
use App\Models\Survey;
use App\Models\SurveyResults;
// use App\Models\Questionset;
use App\Models\Questions;
use App\Models\Answers;
use DB;

class SurveysController extends Controller
{ 
    
    public function AllSurveys(){
        $template['surveys'] = Survey::select('surveys.*','questionsets.title')
                                      ->leftJoin('questionsets', 'questionsets.id', '=', 'surveys.formId')
                                      ->where('questionsets.businessId', '=', Auth::user()->id)
                                      ->get();
        return View::make('surveys.list')->with($template);
    }
    
     public function surveysResponses(Request $request){
        $template['survey']= Survey::select('surveys.*','questionsets.title')
                                      ->leftJoin('questionsets', 'questionsets.id', '=', 'surveys.formId')
                                      ->where('surveys.id',$request->id)
                                      ->first();
        $template['resps'] = SurveyResults::where('surveyId',$request->id)->get();
        return View::make('surveys.responses')->with($template);
    }
    
    public function exportCsv(Request $request){
   $fileName = 'Report-'.date('Ymdhis').'.csv';
   $surveyResp = SurveyResults::where('surveyId', $request->id)->get();
   //print_r($surveyResp);
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Question','Answer Type', 'Answer');

        $callback = function() use($surveyResp, $columns) {
             $file = fopen('php://output', 'w');
             fputcsv($file, $columns);

            foreach ($surveyResp as $rsp) {
                $Que = Questions::where('id', $rsp->qId)->value('question');
                $row['Que']      = $Que;
                $row['ansType']  = $rsp->ansType;
                $ansWer="";
                if($rsp->ansType=='radio'){
                  $ansWer = Answers::where('id',$rsp->answerId)->value('opt');
                }else if($rsp->ansType=='checkbox'){
                    $ids = explode(',',$rsp->answerId);
                  $ansWer = Answers::select(DB::raw('GROUP_CONCAT(opt) as ans'))->whereIn('id', $ids)->value('ans');
                  //print_r($ansWer);
                }else{
                  $ansWer = $rsp->answer;
                }
                
                $row['answer']  = $ansWer;
          //    print_r($row);
           fputcsv($file, array($row['Que'], $row['ansType'], $row['answer']));
            }

          fclose($file);
        };

       return response()->stream($callback, 200, $headers);
    }
    

    
    
    
    
}