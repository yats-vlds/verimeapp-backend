<?php

namespace App\Http\Controllers;

use App\Transection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use View;
use Validator;

class ProfileController extends Controller
{ 
    
    public function index(){
        $template['user'] = User::find(Auth::user()->id);
        //echo Hash::make('12345678');
        return View::make('profile.Profileview')->with($template);
    }
    
    public function uploadImage(Request $request){
          if(!empty($request->file('imageUpload'))) { 
                $id =Auth::user()->id;
                 $user = User::find($id);
                 $oldfile = $user->image;
                 $fileName = uniqid(). $id.'.'.request()->imageUpload->getClientOriginalExtension();
                 $request->imageUpload->move('public/assets/img/profile/', $fileName);
                 $user->image=$fileName;
                 if($user->save()){ 
                       if($oldfile!='' && $oldfile!="avatar.png"){
                            $image_path = "public/assets/img/profile/".$oldfile;
                            if(File::exists($image_path)) { File::delete($image_path); }
                       }
                       
                       return response()->json([
                            'status' => 'success',
                            'message' => 'Profile uploaded successfully',
                        ]);
                 }else{
                     return response()->json([
                            'status' => 'error',
                            'message' => 'Error while upload.',
                        ]);
                 }
          }else{
             return response()->json([
                            'status' => 'error',
                            'message' => 'Choose image before upload.',
                        ]); 
          }
    }
    
    
    public function updateProfileInfo(Request $request){
         
          $validator = Validator::make($request->all(), [  
                'first_name'  => 'required||max:255',
                'last_name'  => 'required|string|max:255',
            ]);
            

            if ($validator->fails()) { 
                return redirect('profile')
                        ->withErrors($validator)
                        ->withInput();        
            }else{
                
                
                
                 $user = User::find(Auth::user()->id);
                 if(Auth::user()->uniqueToken==""){
                    $user->uniqueToken = strtoupper(uniqid('USER'));
                 }
                 
                 $user->first_name = $request->first_name;
                 $user->last_name = $request->last_name;
                 if($user->save()){
                    // return redirect('profile');
                      return redirect()->back()->with('success', 'Profile updated successfully');
                 }else{
                     return redirect('profile');
                    return redirect()->back()->with('error', 'Error while update profile.');
                 }
                 
            }
    }
    
     public function changePassword(Request $request){
         
          $validator = Validator::make($request->all(), [  
                'curr_pass'  => ['required', 'string', 'min:8'],
                'new_pass' => ['required', 'string', 'min:8'],
                'confirm_pass' => ['same:new_pass'],
            ]);
            

            if ($validator->fails()) { 
                return redirect('profile')
                        ->withErrors($validator)
                        ->withInput();        
            }else{
                $user = User::find(Auth::user()->id);
                if (Hash::check($request->curr_password, $user->password)) { 
                     $user->password = Hash::make($request->confirm_pass);
                     if($user->save()){
                         return redirect()->back()->with('success', 'Password changed successfully');
                     }else{
                       return redirect()->back()->with('error', 'Error while change password');
                     }
                }else{
                    return redirect()->back()->with('error', "Current Password dosen't match");
                }
                 
            }
    }
    
    

    
    
    
    
}