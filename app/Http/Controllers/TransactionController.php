<?php

namespace App\Http\Controllers;

use App\Transection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function transection(Request $request)
    {
        // $transection = new Transection();
        // $transection->reference_no = md5(time().mt_rand(1,1000000));


        // dd($request->id);
        $transection = Transection::where('reference_no', $request->id)->first();

        if (!$transection || !$transection->reference_no) {
            echo "No transaction initialted";
            return;
        }
        $user = User::find($transection->user_id);

        $user->steps = '6';
        $user->payment_verified = "Payment Verified";

        $transection->status = 'success';
        $user->save();
        $transection->save();
        
        $verifyUrl = env('APP_URL') . "/payment/verified?id=" . $transection->reference_no;

        return view('transection', compact(['transection', 'verifyUrl']));
    }
    public function paymentVerified(Request $request)
    {
        // $transection = new Transection();
        // $transection->reference_no = md5(time().mt_rand(1,1000000));


        // dd($request->id);
        $transection = Transection::where('reference_no', $request->id)->first();

        if (!$transection || !$transection->reference_no) {
            echo "No transaction initialted";
            return;
        }
        $user = User::find($transection->user_id);

        $user->steps = '6';
        $user->payment_verified = "Payment Verified";

        $transection->status = 'success';
        $user->save();
        $transection->save();
        
    }
    
    public function savepayment(Request $request)
    {

        $user = User::where('id', auth::user()->id)->first();

        // dd($users->id);
        $transection = new Transection();
        $transection->reference_no = md5(time().mt_rand(1,1000000));
        $transection->user_id = $user->id;
        $transection->status = 'pending';

        // dd($transection);
        $transection->save();

        return response()->json([
            'status' => 200,
            'data' => $transection,
            'message' => 'Registration id saved successfully!',
        ]);
    }
}
