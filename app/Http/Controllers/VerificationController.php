<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Models\PhoneModel;
use App\Models\DocsModel;
use App\Models\DocumentViewsModel;
use App\PoliceVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class VerificationController extends Controller
{

    public function saveNumber(Request $request)
    { 
        $validator = Validator::make($request->all(), [ 
            'phone' => 'required',  
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        // $users = User::where('phone', $request->input('phone'))->first();
        // // dd($users);
        // // return view('welcome', compact('users'));

        $phone_exist = PhoneModel::where('phone', $request->input('phone'))->first();

        if ($phone_exist == null) {

            $input = $request->all(); 
            // $code = rand(1111,9999);
             $input ['code'] = "1234"; 
            // \Nexmo::message()->send([
            //     'to'   => $request->input('phone'),
            //     'from' => '16105552344',
            //     'text' => 'verification code '.$code,
            // ]);
            // $input['password'] = bcrypt($input['password']); 
            $user = User::create($input);
        
            return response()->json(['id' => $user->id], 201);
            // $success['token'] =  $user->createToken('MyApp')->accessToken; 
            // $success['name'] =  $user->name;
            // return response()->json(['success'=>$success], $this->successStatus);
        } else {
            // $code = rand(1111,9999);
            $code = "1234";
            // \Nexmo::message()->send([
            //     'to'   => $request->input('phone'),
            //     'from' => '16105552344',
            //     'text' => 'verification code '.$code,
            // ]);
            $phone_exist['code'] = $code;
            $phone_exist->save();

            return response()->json(['id' => $phone_exist['id'], 'status' => 'user_exist'], 409);
        } 
    }

    public function verifyOtp(Request $request)
    {
        // dd($request->code);
        // dd($request->user_id);
        // dd($request->phone);
        $user = User::where('id', $request->user_id)->where('business', '=', 1)->first();
        if($user == NULL){
            return redirect('/');
        }

        if ($user['code'] == $request->code) {
            $user->code = null;
        } else {
            return redirect('/');
        }
        $user->save();
        return redirect('/dashboard');
        // return view('frontend.verification', compact('user'));
        
    }
    public function getOtpVerification(Request $request)
    {
        // dd('get');
        // dd($request->phone);
        $user = User::where('phone', $request->phone)->where('business', '=', 1)->first();
        if($user == NULL){
            return redirect('/');
        }

        $fake_otp = env("FAKE_OTP", false);

        $code = "";
        if ($fake_otp) {
            $code = "1234";
        } else {
            $code = rand(1111,9999);
            try {
                \Nexmo::message()->send([
                    'to'   => $request->input('phone'),
                    'from' => '16105552344',
                    'text' => 'Verification code: '.$code,
                ]);
            } catch (\Throwable $th) {
                echo "Unable to send a pin";
                return;
            }
            
        }


        $user['code'] = $code;
        $user->save();
        return view('frontend.verification', compact('user'));
        
    }
    public function postOtpVerification(Request $request)
    {
        // dd('post');
        // dd($request->method());

        // $user = Auth::user();

        // if ($user);

        if (isset($request->user_id)) {
            $user = User::find($request->user_id);

            if ($user->code == $request->code) {

                // TODO: Generate Token and save it in session

                Auth::loginUsingId($request->user_id);
                return redirect('dashboard');
            } else {
                return redirect()->route('verification', ['phone' => $user->phone]);
            }
        }
    }

    public function verificationFatchData($id)
    {
        $fatch_data = PhoneModel::where('id', $id)->first();
        // dd($fatch_data);
        return view('frontend.verification', compact('fatch_data'));
        
    }

    public function dashboardMainPage(Request $request)
    {
        
        // $users = User::where('code', '=', 1234)->first();
        // if($users == '1234'){
            $user = Auth::user();

             return view('frontend.dashboard', compact('user'));
        // }
        // return redirect('verification');
    }

    public function requestPage(Request $request)
    {
        // $user = User::where('phone', $request->phone)->get();
        // dd($user);

        $DocumentsRequestList = DB::table('docs')
        ->join('document_views', 'document_views.document_id', 'docs.id')
        ->select('docs.documents_image', 'docs.user_id', 'document_views.sender_id')
        ->where('document_views.status', 'request')
        // ->where('')
        // ->where('docs.user_id', $request->id)
        // ->where('document_views.user_id', 'docs.user_id')
        ->get();
       
        $documentsOfUser = $DocumentsRequestList->all();
        return view('frontend.request', compact('documentsOfUser'));
            // $users = new User();
            // $DocumentsRequestList = DB::table('docs')
            // ->join('document_views', 'document_views.document_id', 'docs.id')
            // ->select('docs.documents_image', 'docs.id', 'document_views.sender_id', 'document_views.star')
            // ->where('document_views.status', 'request')
            // ->where('document_views.user_id', $users->id)
            // ->get();
            // $documentArray = [];
            // $documentArray['requested_list'] = [];
            // foreach ($DocumentsRequestList as $value) {
    
            //     if ($value->documents_image == null) {
            //         $document = '';
            //     } else {
            //         $document = env('APP_URL').'/documents/'.$value->documents_image;
            //     }
            //     $document_push = [
            //         'id' => $value->id,
            //         'user_id' => $value->sender_id,
            //         'documents' => $document,
            //     ];
            //     array_push($documentArray['requested_list'], $document_push);
            // }   
            // dd($DocumentsRequestList);
    }

    public function signPage(Request $request)
    {
        return view('frontend.signpage');
    }

    public function DocumentsList(Request $request)
    {
        // $documentViewsModel = DocumentViewsModel::where('status', '=', 'request')->with(['documentViewModel.user'])->get();
        // dd($documentViewsModel);
        // $DocumentsList = DocsModel::where('');
        // dd($DocumentsList);

        // dd($request->phone);

        $appUser = User::where('phone', $request->phone)->where('steps', '=', 6)->where('business', '=', NULL)->first();
        // dd($appUser);
        // $showDocsList = DocumentViewsModel::where('user_id', Auth::user()->id)->with(['docsData'])->get();
        // dd($showDocsList);

        // dd($appUser);

        if ($appUser == null) {
            return redirect('requestpage');
        }


        $DocumentsRequestList = DB::table('docs')
        ->select('docs.text', 'docs.passport', 'docs.driving', 'docs.user_id', 'docs.id')
        ->where('docs.user_id', $appUser->id)
        ->get();
       
        $documentsOfUser = $DocumentsRequestList->all();

        // all selected item by business user
        $alreadyRequestList = DocumentViewsModel::where('user_id', $appUser->id)->where('sender_id', Auth::user()->id)->where('status', '=', 'request')->with('docsData')->get();
        $acceptedDocumentList = DocumentViewsModel::where('user_id', $appUser->id)->where('sender_id', Auth::user()->id)->where('status', '=', 'accepted')->with('docsData')->get();
        // dd($acceptedDocumentList);
        // dd(Auth::user()->id);
        // dd($acceptedDocumentList->all()[0]->docsData->text);

        $rejectedDocumentList = DocumentViewsModel::where('user_id', $appUser->id)->where('sender_id', Auth::user()->id)->where('status', '=', 'rejected')->with('docsData')->get();

        $app_url = env('APP_URL');


        return view('frontend.documentlist', compact(['app_url', 'documentsOfUser', 'appUser', 'acceptedDocumentList', 'rejectedDocumentList', 'alreadyRequestList']));
        // $numVerify = User::where([
        //     ['phone', $request->phone],
        //     ['steps', '=', 5],
        //     ['business', '=', 0],
        //     ['business', '=', 'NULL'],
        // ])->get();
        
        // $numVerify = User::where('phone', $request->phone)->where('steps', '=', 5)->where('business', '=', NULL)->get();
        // dd($numVerify->all()); 

        // $documentlist = DocsModel::where('user_id', $request->phone)->get();
        // if(isset($numVerify)){

            // $requestList = DB::table('users')
            // ->join('docs', 'docs.user_id', 'users.id')
            // ->select('users.phone', 'docs.documents_image', 'docs.user_id')
            // ->where('users.steps', 5)
            // ->where('')
            // ->get();
            // dd($requestList);
//*************************** */
        // }else{
        //     return redirect('frontend.requestpage');
        // }


        // dd($DocumentsRequestList);
// dd($DocumentsRequestList->all()[0]->documents_image);


        // foreach ($DocumentsRequestList->all() as $value) {
        //     echo $value->documents_image;
        //     echo "<br>";
        // }
        // dd($documents_image);

        // $documentArray = [];
        // $documentArray['requested_list'] = [];
        // foreach ($DocumentsRequestList as $value) {

        //     if ($value->documents_image == null) {
        //         $document = '';
        //     } else {
        //         $document = env('APP_URL').'/documents/'.$value->documents_image;
        //     }
        //     $document_push = [
        //         // 'id' => $value->id,
        //         'user_id' => $value->sender_id,
        //         'documents' => $document,
        //     ];
        //     array_push($documentArray['requested_list'], $document_push);
        // }   
        // // dd($documentArray);
        // return view('requestpage');
    }

    // Document list page redirect
    public function acceptRejectDocumentsList(Request $request)
    {
        
        $data = $request->all();
        
        $document_ids = [];
        foreach ($data as $key => $value) {
            if(is_int($key)){
                $document_ids[] = $key;
            }
        }
       
        $userId = $request->user_id;

        foreach ($document_ids as $document_id) {
            $selectedAppDocs = new DocumentViewsModel();
            $selectedAppDocs->document_id = $document_id;
            $selectedAppDocs->sender_id = Auth::user()->id;
            $selectedAppDocs->user_id = $userId;
            $selectedAppDocs->status = 'request';
            $selectedAppDocs->save();
        }

        $appUser = User::where('id', $userId)->where('steps', '=', 6)->where('business', '=', NULL)->first();

        $bodyTxt = "{$appUser->phone} requested document(s)";

        $curl = curl_init();
        $regId = $appUser->registration_id;

        curl_setopt_array($curl, [
        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"to\": \"$regId\",\n\t\"collapseKey\": \"com.verime.document.requests\",\n\t\"data\": {\n\t\t\"type\": \"documentRequest\"\n\t},\n\t\"from\": \"963616106803\",\n\t\"messageId\": \"0:1606910538930905%4d722a4b4d722a4b\",\n\t\"notification\": {\n\t\t\"android\": {},\n\t\t\"body\": \"$bodyTxt\",\n\t\t\"title\": \"Document Request received\"\n\t},\n\t\"sentTime\": 1606910538921,\n\t\"ttl\": 2419200\n}",
        CURLOPT_HTTPHEADER => [
            "Authorization: key=AAAA4Fv-6TM:APA91bHwVStMK7ksUn7pZoMSMHz-DfhnKhX7b3E5n4qXI2sK1mvFthwkICoepkIzHEYDJ7eFpkKUd4wnkQtb7A9TMy9DJeaVilQYvRPfPPX0u2JQiNunC-mRChKe1AC9H2OhSLlNfBdg",
            "Content-Type: application/json"
        ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

        // $showDocsList = DocumentViewsModel::where('id', Auth::user()->id)->get();
        // dd($showDocsList);


        return redirect()->route('documentslist', ["phone" => $appUser->phone]);
        // return redirect('frontend.documentslist');
    }

    public function showRequestedData(Request $request)
    {
        $showDocsList = DocumentViewsModel::where('id', Auth::user()->id)->get();
        // dd($showDocsList);
        return redirect('documentslist', compact('showDocsList'));

        
    }

    public function FoundDocumentsList()
    {
        return view('frontend.founddocumentslist');
    }

    public function policeVerification(Request $request)
    {

        // dd($id);
        // $appUser = User::where('phone', $request->phone)->where('steps', '=', 5)->where('business', '=', NULL)->first();
        // $DocumentsRequestList = DB::table('police_verifications')
        // ->select('police_verifications.user_id')
        // ->where('police_verifications.user_id', $appUser->id)
        // ->get();
       
        $verification_requests = PoliceVerification::where('status', '=', 'verification requests')->with('user')->get();
        $verified_list = PoliceVerification::where('status', '=', 'User Verified')->with('user')->get();
        // dd($verified_list->all()[0]->user_id);

        // foreach ($verified_list as $value) {
            
        // }
        // dd($verification_requests->all());
        // dd($verification_requests->all());
        // $verification_requests = $verification_requests->all();
        
        $data = [];
        foreach ($verification_requests->unique('user_id')->values()->all() as $value) {
            $data[] = $value->user;
        }

        $verify = [];
        foreach ($verified_list->unique('user_id')->values()->all() as $value) {
            $verify[] = $value->user;
        }

        // save pin here

        return view('frontend.policeverification', compact('data', 'verify'));
    }

    public function userDocsDetails(Request $request, $id)
    {
        // dd($id);
        return view('frontend.userdocsdetails');
    }

//     public function generatePin($id)
//     {

//         // dd('a');
//         $characters = '12345678910';
//         // generate a pin based on 2 * 7 digits + a random character
//             $pin = mt_rand(100, 999)
//             . $characters[rand(0, strlen($characters) - 1)];
//             $pin = str_shuffle($pin);
// // dd($pin);
//             return redirect('userdocslist');
//     }

    public function policeVerificationUser(Request $request)
    {
        // dd($request->id);
        $document_list = PoliceVerification::where('user_id', $request->id)->where('status', '=', 'verification requests')->get();
        $unique_pin = PoliceVerification::where('user_id', $request->id)->where('status', '=', 'verification requests')->get();

        // $unique_pin = $unique_pin->all();

         $unique_pin_data = $unique_pin->unique('pin')->values()->all();
        // foreach ($unique_pin->unique('pin')->values()->all() as $value) {
        //     $data[] = $value;
        // }

        // dd($data);
        $document_list = $document_list->all();
    
        $characters = '12345678910';
        // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(100, 999)
            . $characters[rand(0, strlen($characters) - 1)];
            $pin = str_shuffle($pin);

        foreach ($document_list as $value) {
            $value->pin = $pin;
            $value->save();
        }
        // dd($pin_list);
        // dd($document_list);
        $user_id = $request->id;

        return view('frontend.userdocslist', compact(['document_list', 'unique_pin_data', 'user_id']));
    }

    public function generatePin(Request $request)
    {
        // dd($request->user_id);
        $pin_code = PoliceVerification::where('user_id', $request->user_id)->where('status', '=', 'verification requests')->get();
        // dd($pin_code->all());
        // dd($pin_code);
        $characters = '12345678910';
        // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(100, 999)
            . $characters[rand(0, strlen($characters) - 1)];
            $pin = str_shuffle($pin);

        foreach ($pin_code as $value) {
            $value->pin = $pin;
            $value->save();
        }
        // dd($pin_code);
        return view('frontend.userdocslist', compact(['pin_code', 'user_id']));

        // return redirect('police/verification');
    }

    public function generatePinView(Request $request)
    {
        $view_pin = PoliceVerification::where('user_id', $request->user_id)->where('status', '=', 'verification requests')->get();
        dd($view_pin);
        return view('frontend.userdocslist', compact('view_pin'));
    }

}

