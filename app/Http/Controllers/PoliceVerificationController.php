<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Models\DocsModel;
use App\PoliceVerification;
use Illuminate\Http\Request;
use App\Models\DocumentViewsModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PoliceVerificationController extends Controller
{

    // public function policeDocs(Request $request)
    // {
    //     // dd('a');
    //     $validator = Validator::make($request->all(), [  
    //         'name'      => 'required', 
    //         'police_confirmation' => 'required', 
    //     ]);
    //     if ($validator->fails()) { 
    //         return response()->json(['error'=>$validator->errors()], 401);            
    //     }
    //     // dd($validator);

    //     if ($request->hasFile('police_confirmation')) {
    //         $police_confirmation_documents = $request->file('police_confirmation');
    //         $name = time().'.'.$police_confirmation_documents->getClientOriginalExtension(); 
    //         $destinationPath = public_path('documents');
            
    //         $police_confirmation_documents->move($destinationPath, $name);
            
    //         $user_id = Auth::user()->id;
    //         $docs = new PoliceVerification;
    
    //         $docs->user_id = $user_id;
    //         $docs->text = $request->input('name');
    //         $docs->police_confirmation_image = $name;
    //         // $docs->status = 'Verification requests';
    //         $docs->save();
    //         return response()->json(['message' => 'insert successfull'], 201);
    //     }  
    // }

    // public function policeDocsList()
    // {
    //     $listOfPoliceDocument = PoliceVerification::where('user_id', Auth::user()->id)->get();
    //     return response()->json([
    //         'status' => 200,
    //         'data' => $listOfPoliceDocument,
    //         'message' => 'Police document list fatched successfully!',
    //     ]);
    // }

    public function policeSelectedDocsSend(Request $request)
    {

        $validator = \Validator::make($request->all(),[

            'text' => 'required|string',
            'document_id' => 'required|integer',
    
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()->first()
            ]);
        }

        $docs = DocsModel::where('user_id', auth::user()->id)->get();
        // dd($docs);

        $data = [];
        foreach ($docs as $value) {
            $data[] = $value;
        }
        // dd($data);

        // dd($data);
        $selectDocsSend = new PoliceVerification;
        $selectDocsSend->document_id = $request->document_id;
        $selectDocsSend->text = $request->text;

        // foreach ($data as $value) {
            $selectDocsSend->user_id = auth::user()->id;
            // $selectDocsSend->document_id = $value->id;
            $selectDocsSend->status = "verification requests";
            $selectDocsSend->save();
        // }

        return response()->json([
            'data' => $selectDocsSend,
            'status' => 200,
            'message' => 'Request sent successfully!',
        ]);
    }

    public function policeSelectedDocsSendUpdate(Request $request)
    {
        // $docs = DocsModel::where('user_id', auth::user()->id)->get();

        // $data = [];
        // foreach ($docs as $value) {
        //     $data[] = $value;
        // }
        // dd($data);

        // dd($data);
        $selectDocsSend = PoliceVerification::where('user_id', auth::user()->id)->get();
            // dd($selectDocsSend);
        // foreach ($data as $value) {
            // $selectDocsSend->user_id = auth::user()->id;
            // $selectDocsSend->document_id = $value->id;
            foreach ($selectDocsSend as $value) {
                # code...
                $value->status = "User Verified";
                $value->save();
            }
        // }

        return response()->json([
            'data' => $selectDocsSend,
            'status' => 200,
            'message' => 'User verified successfully!',
        ]);

    }

}
