<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\Start;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StartController extends Controller
{
    public function startBtn(Request $request)
    {
        return view('start');
        
    }
    
    public function saveData(Request $request)
    {
        $users = new User();
        $click_btn = new Start();
        $click_btn->participant_id = 1;
        $click_btn->room_no = md5(time().mt_rand(1,1000000));
        $click_btn->save();

        // dd($click_btn);

        return redirect('start-meeting/1');
    }
}

