<?php

namespace App\Http\Controllers;

use App\Transection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use Validator;
use App\Models\Questionset;
use App\Models\Questions;
use App\Models\Answers;

class QuestionsetController extends Controller
{ 
    
    public function CreateQuestionsSet(){
        $template['questionsets'] = Questionset::where('businessId',Auth::user()->id)->get();
        return View::make('questionsSet.CreateQuestionsSet')->with($template);
    }
    
    public function saveQuestionSet(Request $request){
         
          $validator = Validator::make($request->all(), [  
                'setTitle'  => 'required|max:255|unique:questionsets,title'
            ]);
            

            if ($validator->fails()) { 
                return redirect('create/questions/set')
                        ->withErrors($validator)
                        ->withInput();        
            }else{
                 $id = Questionset::insertGetId(['title' =>$request->setTitle,'businessId'=>Auth::user()->id]);
                 return redirect('create-questions/'.$id);
            }
    }
    
    function RemoveQuestionset(Request $request){
        
        if(! $request->ajax()) {
         return response()->json([
                    'status' => 'error',
                    'message' => 'Not allowed',
                ]);
        }else{
            
            
            $quetions = Questions::where('setId',$request->setId)->get();
            foreach($quetions as $Q){
                 Answers::where('qid',$Q->id)->delete();
            }
           
            Questions::where('setId',$request->setId)->delete();
            Questionset::where('id',$request->setId)->delete();
            return response()->json([
                    'status' => 'success',
                    'message' => 'Deleted successfully',
            ]);
        }
    }
    
    function updateQuestionsetStatus(Request $request){
        
        if(! $request->ajax()) {
         return response()->json([
                    'status' => 'error',
                    'message' => 'Not allowed',
                ]);
        }else{
            
            //Questionset::where('businessId',Auth::user()->id)->update(['status'=>$request->status]);
            Questionset::where('id',$request->setId)->where('businessId',Auth::user()->id)->update(['status'=>$request->status]);
            return response()->json([
                    'status' => 'success',
                    'message' => 'Status marked as '.ucfirst(strtolower($request->status)).' successfully',
            ]);
        }
    }
    
     public function CreateQuestionsForSet(Request $request){
           $template['sets'] = Questionset::where('id',$request->id)->first();
           $template['questions'] = Questions::where('setId',$request->id)->get();
           return View::make('questionsSet.ListQuestions')->with($template);
     }
     
     public function getAnswerTemplate(Request $request){
	    if($request->ansType!=""){
	        return View::make('questionsSet.answerTemplate.input_'.$request->ansType);
	    }else{
	        echo   '<div class="col-md-12 alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                       Trying to get invalid format.
                    </div>';
	    }
	}
	
	 public function saveQuestions(Request $request){
        	  $Q['question'] = $request->question;
              $Q['ansType'] = $request->ansType;
              $Q['setId'] =  $request->setId;
              $Qid = Questions::insertGetId($Q);
              if($Qid && ($request->ansType=="checkbox" || $request->ansType=="radio")){
                  for($i=1;$i<=$request->counter;$i++){
                      Answers::insertGetId(['qid'=>$Qid,'opt'=>$_POST[$request->ansType.'-'.$i]]);
                  }
              }
              
              return response()->json([
                    'status' => 'success',
                    'message' => 'Question added successfully.',
                ]);
        //   return redirect('create-questions/'.$request->setId);
              
    }
    
    function RemoveQuestion(Request $request){
        
        if(! $request->ajax()) {
         return response()->json([
                    'status' => 'error',
                    'message' => 'Not allowed',
                ]);
        }else{
            Answers::where('qid',$request->QueId)->delete();
            Questions::where('id',$request->QueId)->delete();
             return response()->json([
                    'status' => 'success',
                    'message' => 'Deleted successfully',
                ]);
        }
    }
    
    
}