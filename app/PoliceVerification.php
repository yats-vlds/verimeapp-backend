<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoliceVerification extends Model
{
    // public $timestamps = true;
    protected $fillable = [
        'user_id',
        'sender_id',
        'text',
        'police_confirmaion_image',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
