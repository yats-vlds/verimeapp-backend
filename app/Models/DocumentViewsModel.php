<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentViewsModel extends Model
{
    protected $table = "document_views";
    public $timestamps = true;
    protected $fillable = [
        'user_id','sender_id','document_id','status', 'star',
    ];
    
    public function docsData()
    {
        return $this->belongsTo(DocsModel::class, 'document_id', 'id');
    }
}