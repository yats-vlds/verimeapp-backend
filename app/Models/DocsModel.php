<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocsModel extends Model
{
    protected $table = "docs";
    public $timestamps = true;
    protected $fillable = [
        'text', 'documents_image','police_confirmation_image',
    ];

}
