<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'phone','code',
    ];
}
