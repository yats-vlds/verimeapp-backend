<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostalModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'first_name','last_name','address_1','address_op','city','zip_code','email','card_number','steps',
    ];
}
