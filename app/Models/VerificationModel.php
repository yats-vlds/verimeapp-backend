<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerificationModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'code','steps',
    ];
}
