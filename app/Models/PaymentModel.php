<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'first_name','last_name','card_number',
    ];
}
