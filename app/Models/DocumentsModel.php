<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentsModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'documents', 'document_image', 'document_text','steps',
    ];

}
