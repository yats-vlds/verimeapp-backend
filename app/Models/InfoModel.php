<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'firstname','sure_name','id_no','birth','street_address','postal_address','mobile_no','tel_no','status','email','steps',
    ];
}
