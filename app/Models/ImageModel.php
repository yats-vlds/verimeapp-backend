<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'image','steps',
    ];
}
