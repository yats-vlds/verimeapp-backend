<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoModel extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = [
        'video',
        'steps',
        'transcript',
    ];
}
