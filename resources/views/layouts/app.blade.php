<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('assets/js/confirm/css/jquery-confirm.css') }}" rel="stylesheet">
   <link href="{{ asset('assets/js/toastr/toastr.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    <div class="full-page login-page" data-color="" data-image="assets/img/background/background-admin.svg">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm" style="background: #48AC98;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('assets/img/logo.png')}}" style="width: auto; height: 45px;"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item" style="margin-left: 10px;">
                                <a class="nav-link text-white" style="background: #45A592" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item" style="margin-left: 10px;">
                                    <a class="nav-link text-white" style="background: #45A592" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <li class="nav-item" style="margin-left: 10px;">
                                <a class="nav-link text-white" style="background: #45A592" href="{{ url('/create/questions/set') }}">Question Set</a>
                            </li>
                            <li class="nav-item" style="margin-left: 10px;">
                                <a class="nav-link text-white" style="background: #45A592" href="{{ url('/surveys') }}">Form Responses</a>
                            </li>
                            <li class="nav-item" style="margin-left: 10px;">
                                <a class="nav-link text-white" style="background: #45A592" href="{{ url('/profile') }}">{{Auth::user()->first_name}}</a>
                            </li>
                         <li class="nav-item" style="margin-left: 10px;">
                                     <a class="nav-link text-white" style="background: #45A592" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        </div>
    </div>

	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
	<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/jquery.validate.min.js')}}" type="text/javascript"></script>
	<!--<script src="{{asset('assets/js/jquery-ui.min.js')}}" type="text/javascript"></script>-->
	<script src="{{asset('assets/js/confirm/js/jquery-confirm.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/toastr/toastr.min.js')}}" type="text/javascript"></script>
	<!--  Forms Validations Plugin -->


	<script>
	    var base_url = "{{url('/')}}";
	</script>
	<script>
	    $(document).ready(function() {
        'use strict';
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': _token }
            });
	    });
	</script>
    <script src="{{asset('js/questions.js')}}"></script>
     <script src="{{asset('js/profile.js')}}"></script>
</body>
</html>
