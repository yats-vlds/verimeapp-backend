<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
           
                </div>
            <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Do You Love</th>
      <th scope="col">Address</th>
      <th scope="col">Choose Two Option</th>
      <th scope="col">DOB</th>
      <th scope="col">Social Security Number</th>
      <th scope="col">Created at</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $key=>$val)
    <tr>
      <th scope="row">{{++$key}}</th>
      <td>{{$val->do_you_love}}</td>
      <td>{{$val->address}}</td>
      <td>{{$val->choose_two_option}}</td>
      <td>{{$val->dob}}</td>
      <td>{{$val->social_Security_number}}</td>
      <td>{{$val->created_at}}</td>
    </tr>
@endforeach
  </tbody>
</table>
        </div>
    </div>
</div>
</div>

