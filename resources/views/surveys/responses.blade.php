
@extends('layouts.app')

@section('content')
<style>
   .has-error .form-control{
        border-color:#dc3545 !important;
    }
    .error-text{
       font-size: 12px;
    }
    .error{
        color:#dc3545 ;
    }
</style>
<div class="container">
    <div class="row justify-content-center" style="margin-top:12px;">
        <div class="col-md-10">

            <div class="card">
                <div class="card-header" style="font-weight: bold;">Question resposes of <b>{{$survey->title}}</b>

                    <a style="float:right;margin-left: 16px; color: #48AC98;" href="{{url('surveys/export-sheet/'.$survey->id)}}">Export</a>&nbsp;
                    <a style="float:right; color: #48AC98;" href="{{url('/surveys')}}">Back</a>

                </div>

                <div class="card-body">
                       <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Question</th>
                              <th scope="col">Answer Type</th>
                              <th scope="col">Options</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $cnt = 1; ?>
                              @foreach($resps as $resp)
                              <?php  $hasQ = DB::table('questions')->where('id',$resp->qId)->count();
                                      $opt ="";
                                      if($hasQ){
                                          $Q = DB::table('questions')->where('id',$resp->qId)->first();
                                          $que = "<span>".$Q->question."</span>";

                                             if($resp->ansType=="radio"){
                                                $ansss = DB::table('answers')->where('id',$resp->answerId)->first();
                                                $opt .="<li style='display:inline-block;margin-right:5px;'>".$ansss->opt."</li>";

                                             }else if($resp->ansType=="checkbox"){
                                                $a=1;
                                                $ids = explode(',',$resp->answerId);
                                                foreach($ids as $aid){
                                                      $ansss = DB::table('answers')->where('id',$aid)->first();
                                                      $opt .="<li style='display:inline-block;margin-right:5px;'><b>".$a.".</b>".$ansss->opt."</li>"; $a++;
                                                    }
                                             }else if($resp->ansType=='text' || $resp->ansType=='textarea' || $resp->ansType=='date'){
                                               $opt=  $resp->answer;
                                             }else{
                                                 $as = asset('assets/files/'.$resp->answer);
                                                 $opt= '<img style="width: 15px;" src="'.$as.'"/>';
                                             }
                                      }else{
                                          $que = "<span class='text-danger'>Question Deleted.</span>";
                                      }?>
                                <tr id="tr-{{$resp->id}}">
                                  <th scope="row">{{$cnt}}</th>
                                  <td>{!!$que!!}</td>
                                  <td>{{ucwords(strtolower($resp->ansType))}}</td>
                                  <td><ul style="list-style:none;margin-bottom: 0px;">{!!$opt !!}</ul></td>
                                </tr>
                                <?php $cnt++; ?>
                             @endforeach
                          </tbody>
                        </table>
                </div>



        </div>
    </div>
</div>
</div>
@endsection

