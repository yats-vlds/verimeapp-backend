
@extends('layouts.app')

@section('content')
<style>
   .has-error .form-control{
        border-color:#dc3545 !important;
    }
    .error-text{
       font-size: 12px;
    }
    .error{
        color:#dc3545 ;
    }
</style>
<div class="container">
    <div class="row justify-content-center" style="margin-top:12px;">
        <div class="col-md-10">

            <div class="card">
                <div class="card-header">List of <b>Submitted</b> resposes</div>

                <div class="card-body">
                      <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">User Name</th>
                              <th scope="col">Question Set</th>
                              <th scope="col">Created At</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $cnt = 1; ?>
                              @foreach($surveys as $sur)

                                <tr id="tr-{{$sur->id}}">
                                  <th scope="row">{{$cnt}}</th>
                                  <td>user name</td>
                                  <td>{{$sur->title}}</td>
                                  <td>{{$sur->created_at}}</td>
                                 <td>
                                   <a class="btn btn-sm btn-primary" href="{{url('/surveys-responses/'.$sur->id)}}">View</a>
                                   <a class="btn btn-sm btn-success" href="{{url('/surveys/export-sheet/'.$sur->id)}}">Export</a>
                                 </td>
                                </tr>
                                <?php $cnt++; ?>
                             @endforeach
                          </tbody>
                        </table>
                </div>



        </div>
    </div>
</div>
  <div style="position: absolute;
                         bottom: 0;
                         color: white;
                         text-align: center;
                         left:0;
                         right:0;
                         "><p> &copy; <script>
                                                          document.write(new Date().getFullYear())

                                                      </script>, made with <i class="fa fa-heart heart" style="color: #CB7B7A;"></i> by <a
                                                          href="https://tectsoft.com" class="text-white" style="text-decoration: none;">TectSoft</a>
                                                      <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=someone@gmail.com"target="_blank">
                                                          {{-- <button type="submit" class="btn btn-fill btn-wd "
                                                              style="background-color: #63AA98; border-color: #63AA98; border-radius: 0px; float: right;">Contact
                                                              Us</button> --}}</p></div>
</div>
@endsection

