<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Police verification</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js')}}">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.bundle.min.js">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"></script>

</head>
<body>
    <h1 style="margin: 20px 0 0 480px;">Document List</h1>
    <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="" action="">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$user_id}}">
                        <div style="margin-left: 500px">
                            
                            
                            @foreach ($document_list as $key => $value) 
                                <div  style="float: left; margin-right:5px"> {{ ++$key }}</div>
                                 {{$value->text}}
                            
                                @endforeach
                        </div>
                    <button style="margin: 20px 0 0 460px;" type="submit" class="btn btn-primary"><a style="color:white">Generate New Pin</a></button><br>


                    @foreach ($unique_pin_data as $value)
                <div style="margin-left: 510px"> Pin: {{$value->pin}}</div>
                    @endforeach

                            </form>
                        </div>
                    </div>
                </div>
            </div>
</body>

</html>