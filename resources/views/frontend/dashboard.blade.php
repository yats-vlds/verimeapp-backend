<!--

  =========================================================
  * Paper Dashboard PRO - V1.3.1
  =========================================================

  * Product Page: https://www.creative-tim.com/product/paper-dashboard-pro
  * Available with purchase of license from https://www.creative-tim.com/product/paper-dashboard-pro
  * Copyright 2017 Creative Tim (https://www.creative-tim.com)
  * License Creative Tim (https://www.creative-tim.com/license)

  =========================================================

-->

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>VeriMe</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


     <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
</head>
<style>
    .ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      width: 133px;;
      background-color: #f1f1f1;
    }
    
    li a {
      display: block;
      color: #000;
      padding: 8px 16px;
      text-decoration: none;
    }
    
    /* Change the link color on hover */
    li a:hover {
      background-color: #63AA98;
      color: white;
    }
    </style>
<body>
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('dashboard') }}"><img src="assets/img/logo.png" alt="Verime" style="width:auto;height:50px;"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{route('policeverification')}}">
                             Police Verification
                         </a>
                     </li>

                    <li>
                        <a href="{{route('verification')}}">
                             Logout
                         </a>
                     </li>
                    {{-- <li>
                        <a href="">
                             Back
                         </a>
                     </li> --}}
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="" data-image="assets/img/background/background-2.jpg">
        <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="#" action="#">
                                <div class="card" data-background="color" data-color="blue">
                                    <div class="card-header">
                                        <img src="assets/img/user.jpg" alt="Girl in a jacket" style="width:120px;height:120px; margin-left: 100px; margin-top: 50px;">
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="btn-group" style="margin-top: 25px;">
                                        <button type="button" id="request" class="btn btn-default" style="border-radius: 0px; background-color: #63aa98; color: white;"><a href="{{route('requestpage')}}" style="color:white;">Request Documents</a></button>
                                        {{-- <button type="button" id="send" class="btn btn-default" style="border-radius: 0px;">  <a href="{{route('sendrequestpage')}}">send</a> </button> --}}
                                         </div>
                                    </div>
                                    {{-- <div class="add">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="radio" style="margin-left: 80px;">
                                                    <input type="radio" name="radio1" id="radio1" value="option1">
                                                    <label for="radio1">Document</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <input type="radio" name="radio1" id="radio1" value="option1">
                                                <label for="radio1"><a href="{{route('startmeeting')}}">Video Call</a></label>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> --}}
                                    <div class="docu">

                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        	<footer class="footer footer-transparent">
                <div class="container">
                    <div class="copyright">
                        &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="https://tectsoft.com">TectSoft</a>
                        <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=info@verime.com" target="_blank"><button type="submit" class="btn btn-fill btn-wd " style="background-color: #63AA98; border-color: #63AA98; border-radius: 0px; float: right;">Contact Us</button></a>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

	<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Forms Validations Plugin -->
	<script src="assets/js/jquery.validate.min.js"></script>

	<!-- Promise Library for SweetAlert2 working on IE -->
	<script src="assets/js/es6-promise-auto.min.js"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="assets/js/moment.min.js"></script>

	<!--  Date Time Picker Plugin is included in this js file -->
	<script src="assets/js/bootstrap-datetimepicker.js"></script>

	<!--  Select Picker Plugin -->
	<script src="assets/js/bootstrap-selectpicker.js"></script>

	<!--  Switch and Tags Input Plugins -->
	<script src="assets/js/bootstrap-switch-tags.js"></script>

	<!-- Circle Percentage-chart -->
	<script src="assets/js/jquery.easypiechart.min.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

	<!--  Notifications Plugin    -->
	<script src="assets/js/bootstrap-notify.js"></script>

	<!-- Sweet Alert 2 plugin -->
	<script src="assets/js/sweetalert2.js"></script>

	<!-- Vector Map plugin -->
	<script src="assets/js/jquery-jvectormap.js"></script>

	<!--  Google Maps Plugin    -->
	<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

	<!-- Wizard Plugin    -->
	<script src="assets/js/jquery.bootstrap.wizard.min.js"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="assets/js/bootstrap-table.js"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="assets/js/jquery.datatables.js"></script>

	<!--  Full Calendar Plugin    -->
	<script src="assets/js/fullcalendar.min.js"></script>

	<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

	<script type="text/javascript">
        $().ready(function(){
            demo.checkFullPageBackgroundImage();

            setTimeout(function(){
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
        $(document).ready(function(){
            $("#request").click(function(){
                $('.add').html('<div class="row"><div class="col-md-6"><div class="radio" style="margin-left: 80px;"><input type="radio" name="radio1" id="radio1" value="option1"><label for="radio1">Document</label></div></div><div class="col-md-6"><div class="radio"><input type="radio" name="radio1" id="radio1" value="option1"><label for="radio1">Video Call</label></div></div></div>');
            });
            $("#send").click(function(){
                $('.add').html('<div class="card-content"><div class="btn btn-default btn-fill btn-wd" style="border-radius: 0px; margin-left: 94px;">Choose file<input type="file"  multiple style="display: none;"></div></div><div class="card-footer text-center"><button type="submit" class="btn btn-fill btn-wd " style="border-radius: 0px;">Send File</button></div>');
            });
            $("#radio1").click(function(){
                $('.docu').html('<div class="row"><div class="col-md-4"><ul class="ul" style="margin-left: 112px;;"><li><a href="">ID</a></li><li><a href="">Passport</a></li><li><a href="">Birth</a></li></ul></div></div>');
            });
        });
	</script>

</html>
