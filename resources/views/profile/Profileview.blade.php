
@extends('layouts.app')

@section('content')
<style>
   .has-error .form-control{
        border-color:#dc3545 !important;
    }
    .error-text{
       font-size: 12px;
    }

        .avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 50px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
/*.avatar-upload .avatar-edit input + label:after {*/
  /*content: "\f040";*/
  /*font-family: 'FontAwesome';*/
/*  color: #757575;*/
/*  position: absolute;*/
/*  top: 10px;*/
/*  left: 0;*/
/*  right: 0;*/
/*  text-align: center;*/
/*  margin: auto;*/
/*}*/
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

.fileActionElem-hidden{
    display:none;
}
</style>
<div class="container">
    <div class="row justify-content-center" style="margin-top:12px;">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                	<button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                	<button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
                @endif

        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header" style="font-weight: bold;">Profile Image</div>
                <div class="card-body">
                     <?php  $img = asset('assets/img/profile/'.$user->image);?>
                    <form id="avtarUploadForm" method="post">
                        @csrf
                         <div class="avatar-upload">
                             <input type="hidden" name="oldImage"  id="oldImage" value="{{$img}}"/>
                             <div class="avatar-edit">
                                <input type='file' name="imageUpload" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload">
                                    <i class="fa fa-camera" style="position: absolute;top: 10px;left: 0;right: 0; text-align: center;"></i>
                                </label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url({{$img}});"></div>
                                <div id="fileActionElem" class=" fileActionElem-hidden" style="margin-top: 12px;">
                                    <button class="btn btn-sm btn-success pull-left" type="button" id="uploadProfile">Change Profile</button>
                                    <button class="btn btn-sm btn-danger pull-right" type="button" id="uploadCancel">Cancel</button>
                                </div>

                            </div>
                        </div>
                   </form>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <form method="post" action="{{url('profile/update-info/')}}">
                <div class="card">
                    <div class="card-header" style="font-weight: bold;">Update Profile Info
                    <span style="border: 1px solid #ccc;padding: 1px 5px;font-weight: 500;background: #8b8b8b;color: #fff;" class="pull-right">{{$user->uniqueToken}}</span></div>

                    <div class="card-body">

                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                  <label for="first_name">First Name </label>
                                  <input class="form-control" type="text" name="first_name" id="first_name" value="{{$user->first_name}}"/>
                                  <span class="error-text text-danger">{{ $errors->first('first_name') }}</span>
                              </div>
                              <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                  <label for="first_name">Last Name </label>
                                  <input class="form-control" type="text" name="last_name" id="last_name" value="{{$user->last_name}}"/>
                                  <span class="error-text text-danger">{{ $errors->first('last_name') }}</span>
                              </div>
                               <div class="form-group ">
                                  <label for="first_name">Mobile Number</label>
                                  <input class="form-control" type="text" name="phone" id="phone" value="{{$user->phone}}" readonly/>
                              </div>
                              <div class="form-group ">
                                  <label for="first_name">Email</label>
                                  <input class="form-control" type="text" name="email" id="email" value="{{$user->email}}" readonly/>
                              </div>


                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" name="updateProfileInfo" id="updateProfileInfo" class="btn btn-success" style="background-color:#48AC98;">Update</button>
                    </div>
                </div>
         </form>
        </div>
         <div class="col-md-4">
              <form method="post" action="{{url('profile/change-password/')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card">
                <div class="card-header" style="font-weight: bold;">Change Password</div>

                <div class="card-body">



                          <div class="form-group {{ $errors->has('curr_pass') ? 'has-error' : '' }}">
                              <label for="first_name">Current Password</label>
                              <input class="form-control" type="password" name="curr_pass" id="curr_pass" />
                              <span class="error-text text-danger">{{ $errors->first('curr_pass') }}</span>
                          </div>
                          <div class="form-group {{ $errors->has('new_pass') ? 'has-error' : '' }}">
                              <label for="new_pass">New Password</label>
                              <input class="form-control" type="password" name="new_pass" id="new_pass" />
                              <span class="error-text text-danger">{{ $errors->first('new_pass') }}</span>
                          </div>
                          <div class="form-group {{ $errors->has('confirm_pass') ? 'has-error' : '' }}">
                              <label for="confirm_pass">Confirm Password</label>
                              <input class="form-control" type="password" name="confirm_pass" id="confirm_pass"/>
                              <span class="error-text text-danger">{{ $errors->first('confirm_pass') }}</span>
                          </div>



                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn text-white" style="background-color:#48AC98;" >Change Password</button>
                </div>
            </form>


        </div>
       </div>
    </div>
     <div style="position: absolute;
                         bottom: 0;
                         color: white;
                         text-align: center;
                         left:0;
                         right:0;
                         "><p> &copy; <script>
                                                          document.write(new Date().getFullYear())

                                                      </script>, made with <i class="fa fa-heart heart" style="color: #CB7B7A;"></i> by <a
                                                          href="https://tectsoft.com" class="text-white" style="text-decoration: none;">TectSoft</a>
                                                      <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=someone@gmail.com"target="_blank">
                                                          {{-- <button type="submit" class="btn btn-fill btn-wd "
                                                              style="background-color: #63AA98; border-color: #63AA98; border-radius: 0px; float: right;">Contact
                                                              Us</button> --}}</p></div>
</div>
@endsection

