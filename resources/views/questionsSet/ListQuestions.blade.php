
@extends('layouts.app')

@section('content')
<style>
   .has-error .form-control{
        border-color:#dc3545 !important;
    }
    .error-text{
       font-size: 12px;
    }
    .error{
        color:#dc3545 ;
    }
</style>
<div class="container">
    <div class="row justify-content-center" style="margin-top:12px;">
        <div class="col-md-10">

            <div class="card">
                <div class="card-header" style="font-weight: bold;">List Questions of <b>{{$sets->title}}</b>  <a style="float:right;color:#48AC98; " href="{{url('/create/questions/set')}}">Back</a></div>

                <div class="card-body">
                      <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Question</th>
                              <th scope="col">Answer Type</th>
                              <th scope="col">Options</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $cnt = 1; ?>
                              @foreach($questions as $que)
                              <?php  $opt ="";
                                     if($que->ansType=="checkbox" || $que->ansType=="radio"){
                                        $ansss = DB::table('answers')->where('qid',$que->id)->get();
                                        $a=1;
                                        foreach($ansss as $ans){
                                          $opt .="<li style='display:inline-block;margin-right:5px;'><b>".$a.".</b>".$ans->opt."</li>"; $a++;
                                        }
                                     }?>
                                <tr id="tr-{{$que->id}}">
                                  <th scope="row">{{$cnt}}</th>
                                  <td>{{$que->question}}</td>
                                  <td>{{ucwords(strtolower($que->ansType))}}</td>
                                  <td><ul style="list-style:none;margin-bottom: 0px;">{!!$opt !!}</ul></td>
                                  <td style="text-align:center;"><a  data-id="{{$que->id}}" class="delete-Question" href="#"><img style="width: 15px;" src="{{asset('assets/img/trash-icon.png')}}"/></a></td>
                                </tr>
                                <?php $cnt++; ?>
                             @endforeach
                          </tbody>
                        </table>
                </div>



        </div>
        </br>
         <form method="post" id="formQuestionSet" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" id="counter" name="counter" value="1">
              <input type="hidden" id="setId" name="setId" value="{{$sets->id}}">
            <div class="card">
                <div class="card-header" style="font-weight: bold;">Create New Question for  <b>{{$sets->title}}</b></div>

                <div class="card-body">


                          <div class="row">
                              <div class="col-md-10">
                                   <div class="form-group">
                                      <label for="setTitle">Enter Question : </label>
                                      <input
                                      class="form-control"
                                       type="text"
                                        name="question"
                                         id="question"
                                          value=""
                                          style="border-left: 0; border-right: 0; border-top: 0; border-bottom: 2px solid #48AC98;"
                                          />
                                  </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">
                                      <label for="ansType">Answer Type</label>
                                      <select class="form-control" name="ansType" id="ansType">
                                           <option value="">Select</option>
                                          <option value="text">Text</option>
                                          <option value="textarea">Textarea</option>
                                          <option value="checkbox">checkbox</option>
                                          <option value="radio">Radio</option>
                                          <option value="date">Date</option>
                                          <option value="file">File</option>
                                      </select>
                                  </div>
                              </div>
                          </div>

                           <div class="card card-primary">
                          <div class="card-header" style="font-weight: bold;"">Answers </div>
                          <div class="card-body answerTempBoxCardBody" >
                              <div class="row" id="answerTempBox">

                              </div>
                          </div>

                     </div>


                </div>

                <div class="card-footer">
                  <button type="submit" class="btn text-white" style="background-color: #48AC98;">Submit</button>
                </div>

        </div>
          </form>

    </div>
</div>
  <div style="position: absolute;
                     bottom: 0;
                     color: white;
                     text-align: center;
                     left:0;
                     right:0;
                     "><p> &copy; <script>
                                                      document.write(new Date().getFullYear())

                                                  </script>, made with <i class="fa fa-heart heart" style="color: #CB7B7A;"></i> by <a
                                                      href="https://tectsoft.com" class="text-white" style="text-decoration: none;">TectSoft</a>
                                                  <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=someone@gmail.com"target="_blank">
                                                      {{-- <button type="submit" class="btn btn-fill btn-wd "
                                                          style="background-color: #63AA98; border-color: #63AA98; border-radius: 0px; float: right;">Contact
                                                          Us</button> --}}</p></div>
</div>
@endsection

