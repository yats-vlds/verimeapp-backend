
@extends('layouts.app')

@section('content')
<style>
   .has-error .form-control{
        border-color:#dc3545 !important;
    }
    .error-text{
       font-size: 12px;
    }
</style>
<div class="container">
    <div class="row justify-content-center" style="margin-top:12px;">
        <div class="col-md-10">
        <h1 class="text-white" style="font-weight: bold;">Forms</h6>
            <div class="card mb-5">
                <div class="card-header" style="font-weight: bold;">Create New Questions Set</div>

                <div class="card-body">
                      <form method="post" action="{{url('save/questions/set/')}}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="row">
                              <div class="col-md-10">
                                   <div class="form-group {{ $errors->has('setTitle') ? 'has-error' : '' }}">
                                      <label for="setTitle">Enter Question Set Title : </label>
                                      <input class="form-control" type="text" name="setTitle" id="setTitle" value="{{ old('setTitle') }}" style="border-left: 0; border-right: 0; border-top: 0; border-bottom: 2px solid #48AC98;" />
                                      <span class="error-text text-danger">{{ $errors->first('setTitle') }}</span>
                                  </div>
                              </div>
                              <div class="col-md-2">
                                  <button type="submit" class="btn text-white" style="border-radius: 10%;margin-top: 30px; background-color: #48AC98;">
                                    Create
                                </button>
                              </div>
                          </div>

                      </form>
                </div>



        </div>

        <div class="card">
                <div class="card-header" style="font-weight: bold;">List Questions Set</div>

                <div class="card-body">
                      <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Title</th>
                              <th scope="col">Status</th>
                              <th scope="col">View</th>
                              <th scope="col">Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $cnt = 1; ?>
                              @foreach($questionsets as $sets)
                                <tr id="tr-{{$sets->id}}">
                                  <th scope="row">{{$cnt}}</th>
                                  <td>{{$sets->title}}</td>
                                  <td><a class="Qset-status" style="color: #48AC98;" href="#" data-id="{{$sets->id}}"><span id="span-status-{{$sets->id}}" data-status="{{$sets->status}}">{{ucwords(strtolower($sets->status))}}</span></a></td>
                                  <td><a style="color: #48AC98;" href="{{url('/create-questions/'.$sets->id)}}">view & add Questions</a></td>
                                  <td style="text-align:center;"><a data-status="{{$sets->status}}"  data-id="{{$sets->id}}" class="delete-QuestionSet" href="#"><img style="width: 15px;" src="{{asset('assets/img/trash-icon.png')}}"/></a></td>
                                </tr>
                                <?php $cnt++; ?>
                             @endforeach
                          </tbody>
                        </table>
                </div>
        </div>
    </div>
</div>
     <div style="position: absolute;
                     bottom: 0;
                     color: white;
                     text-align: center;
                     left:0;
                     right:0;
                     "><p> &copy; <script>
                                                      document.write(new Date().getFullYear())

                                                  </script>, made with <i class="fa fa-heart heart" style="color: #CB7B7A;"></i> by <a
                                                      href="https://tectsoft.com" class="text-white" style="text-decoration: none;">TectSoft</a>
                                                  <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=someone@gmail.com"target="_blank">
                                                      {{-- <button type="submit" class="btn btn-fill btn-wd "
                                                          style="background-color: #63AA98; border-color: #63AA98; border-radius: 0px; float: right;">Contact
                                                          Us</button> --}}</p></div>
</div>
@endsection

