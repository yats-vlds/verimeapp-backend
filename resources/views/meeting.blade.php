<!doctype html>
<html lang="en">
    <head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>VeriMe</title>
</head>

<body>

    {{-- <@php
        echo uniqid();
    @endphp --}}
    {{$roomNumUrl}}

    <div id="meet"></div>

    <script src='https://meet.jit.si/external_api.js'></script>
    
    <script>
        const domain = 'meet.jit.si';
        const options = {
       
        roomName: '{{$roomNumUrl}}',
        width: 1200,
        height: 500,
        parentNode: document.querySelector('#meet'),
    
        userInfo: {
            email: 'email@jitsiexamplemail.com',
            displayName: 'Abdullah'
        }
        };
    const api = new JitsiMeetExternalAPI(domain, options);
    </script>
</body>
</html>