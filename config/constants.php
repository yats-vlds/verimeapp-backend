<?php
return [
    'star' => [
        '0' => 'not favorite',
        '1' => 'favorite',
    ], 
    
    'status' => [
        '0' => 'pending',
        '1' => 'approved',
        '2' => 'rejected',
    ], 
];
